
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/sdl)
set_target_properties(SDL2main PROPERTIES FOLDER "libs")
set_target_properties(SDL2-static PROPERTIES FOLDER "libs")
set_target_properties(uninstall PROPERTIES FOLDER "libs")