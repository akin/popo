
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/volk)

# unfortunately VOLK does not include the header. :( 
target_include_directories(
    volk 
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/volk
)
set_target_properties(volk PROPERTIES FOLDER "libs")