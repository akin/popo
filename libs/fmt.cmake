
set(FMT_TEST OFF)
set(FMT_INSTALL OFF)
set(FMT_DOC OFF)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/fmt)
set_target_properties(fmt PROPERTIES FOLDER "libs")