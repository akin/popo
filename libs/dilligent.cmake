
option(ENGINE_DILLIGENT "Add Dilligent engine" ON)
if(ENGINE_DILLIGENT)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/DiligentCore)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/DiligentTools)
endif()
