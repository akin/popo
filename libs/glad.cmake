cmake_minimum_required(VERSION 3.15)
project(glad)

add_library(
	${PROJECT_NAME} 
		${CMAKE_CURRENT_LIST_DIR}/glad/include/glad/glad.h 
		${CMAKE_CURRENT_LIST_DIR}/glad/src/glad.c
	)
target_include_directories(
	${PROJECT_NAME} 
		PUBLIC 
		${CMAKE_CURRENT_LIST_DIR}/glad/include/
	)
