
include(${CMAKE_CURRENT_LIST_DIR}/dilligent.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/stb.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/glm.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/fmt.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/json.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/tinygltf.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/volk.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/sdl.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/spdlog.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/lua.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/sol2.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/flatbuffers.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/glfw.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/glad.cmake)