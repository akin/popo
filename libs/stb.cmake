if(TARGET stb)
else()
    add_library(stb STATIC "${CMAKE_CURRENT_LIST_DIR}/empty.c")
    target_include_directories(stb INTERFACE ${CMAKE_CURRENT_LIST_DIR}/stb)
endif()

set_target_properties(stb PROPERTIES FOLDER "libs")