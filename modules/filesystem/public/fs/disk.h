#ifndef FILESYSTEM_DISK_H_
#define FILESYSTEM_DISK_H_

#include "package.h"

namespace filesystem
{
	class Disk final : public Package
	{
	public:
		~Disk() final;

		std::unique_ptr<Handle> getHandle(const std::string& path) final;
	};
} // ns filesystem

#endif // FILESYSTEM_DISK_H_