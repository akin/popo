#ifndef FILESYSTEM_PACKAGE_H_
#define FILESYSTEM_PACKAGE_H_

#include <string>
#include <memory>

namespace filesystem
{
	class Handle;
	class Package
	{
	public:
		virtual ~Package() = default;

		virtual std::unique_ptr<Handle> getHandle(const std::string& path) = 0;
	};
} // ns filesystem

#endif // FILESYSTEM_PACKAGE_H_