#ifndef FILESYSTEM_HANDLE_H_
#define FILESYSTEM_HANDLE_H_

#include <string>
#include <memory>
#include <vector>

namespace filesystem
{
	class Handle
	{
	public:
		virtual ~Handle() = default;

		const std::string& getPath() const;

		virtual bool isFolder() const = 0;
		virtual bool isFile() const = 0;

		virtual std::unique_ptr<Handle> getParentHandle() = 0;

		// Folder:
		virtual std::vector<std::string> listFiles() const = 0;
		virtual std::unique_ptr<Handle> getHandle(const std::string& path) const = 0;

		// File
		virtual size_t getBytesize() const = 0;

		virtual void* map() = 0;
		virtual void unmap() = 0;

		size_t read(void* buffer);
		virtual size_t read(void* buffer, size_t offset, size_t bytesize) = 0;
		virtual std::string readToString() = 0;
	protected:
		Handle(const std::string& path);

		std::string m_path;
	};
} // ns filesystem

#endif // FILESYSTEM_HANDLE_H_