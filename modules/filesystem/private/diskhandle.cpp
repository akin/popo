#include "diskhandle.h"
#include <fs/disk.h>

#include <filesystem>
#include <fstream>

#include <spdlog/spdlog.h>
#include <assert.h>

namespace filesystem
{
	DiskHandle::DiskHandle(Disk& package, const std::string& path)
		: Handle(path)
		, m_fspath(path)
		, m_package(package)
	{
	}

	DiskHandle::~DiskHandle()
	{
	}

	bool DiskHandle::isFolder() const
	{
		return std::filesystem::is_directory(m_fspath);
	}

	bool DiskHandle::isFile() const
	{
		return std::filesystem::is_regular_file(m_fspath);
	}

	std::unique_ptr<Handle> DiskHandle::getParentHandle()
	{
		std::string target = m_fspath.parent_path().string();
		return m_package.getHandle(target);
	}

	std::vector<std::string> DiskHandle::listFiles() const
	{
		if (!isFolder())
		{
			return {};
		}
		std::vector<std::string> listing;
		for (auto& p : std::filesystem::directory_iterator(m_fspath))
		{
			std::filesystem::path current{ p };
			listing.push_back(current.filename().string());
		}
		return std::move(listing);
	}

	std::unique_ptr<Handle> DiskHandle::getHandle(const std::string& path) const
	{
		if (!isFolder())
		{
			return {};
		}
		std::filesystem::path tmp = m_fspath;
		tmp /= path; // this operator is just retarded, must be result of some disease dream.
		std::string target = tmp.string();
		return m_package.getHandle(target);
	}

	size_t DiskHandle::getBytesize() const
	{
		return std::filesystem::file_size(m_fspath);
	}

	void* DiskHandle::map()
	{
		if (isFolder())
		{
			return nullptr;
		}
		size_t size = getBytesize();
		m_map.reserve(size);
		std::ifstream in(m_fspath, std::ios_base::in | std::ios_base::binary);
		m_map = std::vector<char>(std::istreambuf_iterator<char>(in), {});
		return m_map.data();
	}

	void DiskHandle::unmap()
	{
		m_map = std::move(std::vector<char>{});
	}

	size_t DiskHandle::read(void* buffer, size_t offset, size_t bytesize)
	{
		if (isFolder())
		{
			return 0;
		}
		size_t size = getBytesize();
		m_map.reserve(size);
		std::ifstream in(m_fspath, std::ios_base::in | std::ios_base::binary);
		in.seekg(offset);

		in.read(static_cast<char*>(buffer), bytesize);
		return in.gcount();
	}

	std::string DiskHandle::readToString()
	{
		size_t size = getBytesize();
		std::ifstream in(m_fspath, std::ios_base::in | std::ios_base::binary);

		std::string str(size, '\0'); // construct string to stream size
		if (!in.read(&str[0], size))
		{
			return {};
		}
		return str;
	}
} // ns filesystem
