#include <fs/disk.h>
#include "diskhandle.h"
#include <filesystem>
#include <spdlog/spdlog.h>
#include <assert.h>

namespace filesystem
{
	Disk::~Disk()
	{
	}

	std::unique_ptr<Handle> Disk::getHandle(const std::string& path)
	{
		std::filesystem::path fspath{ path };

		if (!std::filesystem::exists(fspath))
		{
			return {};
		}

		return std::make_unique<DiskHandle>(*this, path);
	}
} // ns filesystem
