#include <fs/handle.h>

#include <spdlog/spdlog.h>
#include <assert.h>

namespace filesystem
{
	Handle::Handle(const std::string& path)
		: m_path(path)
	{
	}

	const std::string& Handle::getPath() const
	{
		return m_path;
	}

	size_t Handle::read(void* buffer)
	{
		return read(buffer, 0, -1);
	}
} // ns filesystem
