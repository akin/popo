#ifndef FILESYSTEM_DISKHANDLE_H_
#define FILESYSTEM_DISKHANDLE_H_

#include <string>
#include <memory>
#include <vector>
#include <filesystem>
#include <fs/handle.h>

namespace filesystem
{
	class Disk;
	class DiskHandle final : public Handle
	{
	public:
		DiskHandle(Disk& package, const std::string& path);
		~DiskHandle() final;

		bool isFolder() const final;
		bool isFile() const final;

		std::unique_ptr<Handle> getParentHandle() final;

		// Folder:
		std::vector<std::string> listFiles() const final;
		std::unique_ptr<Handle> getHandle(const std::string& path) const final;

		// File
		size_t getBytesize() const final;

		void* map() final;
		void unmap() final;

		size_t read(void* buffer, size_t offset, size_t bytesize) final;
		std::string readToString() final;
	private:
		Disk& m_package;
		std::filesystem::path m_fspath;

		std::vector<char> m_map;
	};
} // ns filesystem

#endif // FILESYSTEM_DISKHANDLE_H_