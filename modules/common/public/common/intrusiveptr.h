#ifndef BONA_COMMON_INTRUSIVEPTR_H_
#define BONA_COMMON_INTRUSIVEPTR_H_

namespace bncommon 
{

/**
 * This class is a documentation of what a reference counted class should
 * look like.
 */
class ReferenceCountedObject
{
public:
    virtual ~ReferenceCountedObject() = default;
    virtual void incrementReferenceCount() noexcept = 0;
    virtual void decrementReferenceCount() noexcept = 0;
};

/**
 * This class is implementation of Intrusive Reference Counted Pointer.
 * I guess it takes some blackbox inspiration from boos intrusive_ptr.
 * Functions and class should follow standard std ptr types closely,
 * so, some inspiration from shared_ptr and unique_ptr etc.
 * 
 * I hope Ill make this thread safe one day, but it hasnt been priority.
 */
template <class T>
class IntrusivePtr
{
public:
    IntrusivePtr() = default;
    ~IntrusivePtr() noexcept;

    IntrusivePtr(T* value) noexcept;
    IntrusivePtr(const IntrusivePtr<T>& value) noexcept;

    const IntrusivePtr<T>& operator=(const IntrusivePtr<T>& r ) noexcept;

    void reset() noexcept;

    T* get() const noexcept;
    T& operator*() const noexcept;
    T* operator->() const noexcept;
    
    explicit operator bool() const noexcept;
private:
    T *m_value = nullptr;

    void incrementRef() noexcept;
    void decrementRef() noexcept;
};

///Implementation
template <class T>
IntrusivePtr<T>::~IntrusivePtr() noexcept
{
    decrementRef();
    m_value = nullptr;
}

template <class T>
IntrusivePtr<T>::IntrusivePtr(T* value) noexcept
: m_value(value)
{
    incrementRef();
}

template <class T>
IntrusivePtr<T>::IntrusivePtr(const IntrusivePtr<T>& value) noexcept
: m_value(value.get())
{
    incrementRef();
}

template <class T>
const IntrusivePtr<T>& IntrusivePtr<T>::operator=( const IntrusivePtr<T>& r ) noexcept
{
    decrementRef();
    m_value = r.get();
    incrementRef();
    return *this;
}

template <class T>
void IntrusivePtr<T>::reset() noexcept
{
    decrementRef();
    m_value = nullptr;
}

template <class T>
T* IntrusivePtr<T>::get() const noexcept
{
    return m_value;
}

template <class T>
T& IntrusivePtr<T>::operator*() const noexcept
{
    return *m_value;
}

template <class T>
T* IntrusivePtr<T>::operator->() const noexcept
{
    return m_value;
}

template <class T>
IntrusivePtr<T>::operator bool() const noexcept
{
    return m_value != nullptr;
}

template <class T>
void IntrusivePtr<T>::incrementRef() noexcept
{
    if(m_value != nullptr)
    {
        m_value->incrementReferenceCount();
    }
}

template <class T>
void IntrusivePtr<T>::decrementRef() noexcept
{
    if(m_value != nullptr)
    {
        m_value->decrementReferenceCount();
    }
}
} // ns bncommon

#endif // BONA_COMMON_INTRUSIVEPTR_H_
