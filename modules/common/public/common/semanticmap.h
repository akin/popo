#ifndef COMMON_SEMANTIC_ID_MAP_H_INCLUDED
#define COMMON_SEMANTIC_ID_MAP_H_INCLUDED

#include <string>
#include <unordered_map>

namespace common
{
    using SemanticID = uint32_t;
    class SemanticMap final
    {
    public:
        void set(const std::string& key, SemanticID id);

        SemanticID get(const std::string& key) const;
        SemanticID getOrCreate(const std::string& key);
        std::string getString(SemanticID id);

        SemanticID getMaximum() const;
    private:
        std::unordered_map<std::string, SemanticID> m_mapping;
        std::unordered_map<SemanticID, std::string> m_mappingToStr;
        SemanticID m_current = 4;
    };
} // ns common

#endif // COMMON_SEMANTIC_ID_MAP_H_INCLUDED