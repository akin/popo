
#ifndef COMMON_PLATFORM_H_INCLUDED_POPO
#define COMMON_PLATFORM_H_INCLUDED_POPO

#ifndef NOMINMAX
# define NOMINMAX
#endif
#ifndef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

namespace common
{
#ifdef WIN32
	struct Win32WindowHandle
	{
		HWND hwnd;
		HINSTANCE hinstance = nullptr;
	};
	using WindowHandle = Win32WindowHandle;
#elif XLIB
	struct XLibWindowHandle
	{
		Display* dpy = nullptr;
		Window window;
	};
	using WindowHandle = XLibWindowHandle;
#elif XCB
	struct XCBWindowHandle
	{
		xcb_connection_t* connection = nullptr;
		xcb_window_t window;
	};
	using WindowHandle = XCBWindowHandle;
#endif
} // ns common

#endif // COMMON_PLATFORM_H_INCLUDED_POPO
