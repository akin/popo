
#ifndef BONA_COMMON_COMMON_H_
#define BONA_COMMON_COMMON_H_

#include <vector>
#include <memory>
#include <chrono>
#include <string>

namespace bncommon 
{
    using Float = float;
    using Time = float;

	// Times in nanoseconds
    using Times = std::vector<size_t>;
    using Clock = std::chrono::high_resolution_clock;
	using TimeUnit = std::chrono::nanoseconds;

	std::string getTimeUnit();
} // ns bncommon

#endif // BONA_COMMON_COMMON_H_
