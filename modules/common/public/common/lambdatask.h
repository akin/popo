
#ifndef BONA_COMMON_LAMBDATASK_H_
#define BONA_COMMON_LAMBDATASK_H_

#include <bninterface/task.h>
#include <functional>

namespace bncommon {

class LambdaTask final : public bninterface::Task
{
private:
    const std::function<void()> m_func;
public:
    LambdaTask(const std::function<void()>& func);
    
    ~LambdaTask() final;

    void run() final;
};

} // ns bncommon

#endif // BONA_COMMON_LAMBDATASK_H_
