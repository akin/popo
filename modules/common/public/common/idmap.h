
#ifndef BONA_COMMON_IDMAP_H_
#define BONA_COMMON_IDMAP_H_

#include <bninterface/task.h>
#include <functional>
#include <unordered_map>

namespace bncommon {

template <typename CType, typename VType = uint32_t, VType startvalue = 1>
class IDMap
{
public:
    VType getOrCreateID(const CType& key)
    {
        auto iter = m_map.find(key);
        if(iter == m_map.end())
        {
            VType val = m_current++;
            m_map[key] = val;
            return val;
        }
        return iter->second;
    }

    void set(const CType& key, VType value)
    {
        m_map[key] = value;
    }

    VType get(const CType& key) const
    {
        auto iter = m_map.find(key);
        if (iter == m_map.end())
        {
            return {};
        }
        return iter->second;
    }
private:
    VType m_current = startvalue;
    std::unordered_map<CType, VType> m_map;
};

} // ns bncommon

#endif // BONA_COMMON_IDMAP_H_
