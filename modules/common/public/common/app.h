#ifndef COMMON_APP_H_INCLUDED_POPO
#define COMMON_APP_H_INCLUDED_POPO

namespace common
{
	class App
	{
	public:
		virtual ~App() = default;

		virtual bool initialize() = 0;

		virtual bool update() = 0;

		virtual void render() = 0;
	};

} // common
#endif // COMMON_APP_H_INCLUDED_POPO