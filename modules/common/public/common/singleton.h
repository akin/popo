
#ifndef BONA_COMMON_SINGLETON_H_
#define BONA_COMMON_SINGLETON_H_

namespace bncommon {

template <class CType>
CType& getSingleton()
{
    static CType instance;
    return instance;
}

} // ns bncommon

#endif // BONA_COMMON_SINGLETON_H_
