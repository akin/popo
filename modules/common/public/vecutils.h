#ifndef VECUTILS_H_
#define VECUTILS_H_

#include <vector>

template <typename T>
void swapBack(std::vector<T>& vec, size_t index)
{
   std::swap(vec[index], vec.back());
}

#endif