
#include <common/semanticmap.h>
#include <algorithm>

namespace common
{
	namespace
	{
		std::string toLowerCase(const std::string& str)
		{
            std::string lkey = str;
			std::for_each(lkey.begin(), lkey.end(), [](char& c) {
				c = ::tolower(c);
            });
            return std::move(lkey);
		}
	}

	void SemanticMap::set(const std::string& key, SemanticID id)
	{
		std::string lkey = toLowerCase(key);

		m_mapping[lkey] = id;
		m_mappingToStr[id] = lkey;

		if (m_current < id)
		{
			m_current = id;
		}
	}

	SemanticID SemanticMap::get(const std::string& key) const
	{
		if (key.empty())
		{
			return 0;
		}
		std::string lkey = toLowerCase(key);
		auto iter = m_mapping.find(lkey);
		if (iter == m_mapping.end())
		{
			return 0;
		}
		return iter->second;
	}

	SemanticID SemanticMap::getOrCreate(const std::string& key)
	{
		if (key.empty())
		{
			return 0;
		}
		std::string lkey = toLowerCase(key);
		auto iter = m_mapping.find(lkey);
		if (iter == m_mapping.end())
		{
			SemanticID newID = ++m_current;
			set(key, newID);
			return newID;
		}
		return iter->second;
	}

	std::string SemanticMap::getString(SemanticID id)
	{
		auto iter = m_mappingToStr.find(id);
		if (iter == m_mappingToStr.end())
		{
			return {};
		}
		return iter->second;
	}

	SemanticID SemanticMap::getMaximum() const
	{
		return m_current;
	}
} // ns common
