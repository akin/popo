#include <iostream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <graphics_common.h>
#include <glm/glm.hpp>

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

const std::string g_vertexShaderSource{
R"SHADER(
attribute vec3 position;
void main()
{
	gl_Position = vec4(position, 1.0);
}
)SHADER"
};

const std::string g_fragmentShaderSource{
R"SHADER(
void main()
{
	gl_FragColor = vec4(0.8, 0.8, 1.0, 1.0);
}
)SHADER"
};

class Effect
{
public:
	Effect(const std::string& vertexSource, const std::string& fragmentSource)
	{
		program = glCreateProgram();
		{
			vertexShader = glCreateShader(GL_VERTEX_SHADER);
			const GLchar* src = static_cast<const GLchar*>(vertexSource.data());
			const GLint size = static_cast<GLint>(vertexSource.size());
			glShaderSource(vertexShader, 1, &src, &size);
			glCompileShader(vertexShader);
			glAttachShader(program, vertexShader);
		}
		{
			fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
			const GLchar* src = static_cast<const GLchar*>(fragmentSource.data());
			const GLint size = static_cast<GLint>(fragmentSource.size());
			glShaderSource(fragmentShader, 1, &src, &size);
			glCompileShader(fragmentShader);
			glAttachShader(program, fragmentShader);
		}
		glLinkProgram(program);
	}

	~Effect()
	{
		glDeleteProgram(program);
		glDeleteShader(fragmentShader);
		glDeleteShader(vertexShader);
	}

	void use()
	{
		glUseProgram(program);
	}

	uint32_t getAttributeLocation(const std::string& attribute)
	{
		return glGetAttribLocation(program, attribute.c_str());
	}

	uint32_t program = 0;
	uint32_t vertexShader = 0;
	uint32_t fragmentShader = 0;
};

struct Buffer
{
public:
	Buffer(size_t bytesize)
	{
		GLuint bufferID;
		glGenBuffers(1, &bufferID);
		id = bufferID;
		size = bytesize;
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(size), nullptr, GL_STATIC_DRAW);
	}

	Buffer(size_t bytesize, const void* data)
		: Buffer(bytesize)
	{
		set(data);
	}

	~Buffer()
	{
		GLuint bufferID = id;
		glDeleteBuffers(1, &bufferID);
	}

	void set(const void* data)
	{
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(size), data, GL_STATIC_DRAW);
	}

	uint32_t id = 0;
	uint32_t attribute = 0;
	size_t size = 0;
};

int main(int argc, char* argv[])
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		spdlog::error("Failed to initialize GLAD");
		return -1;
	}

	Effect effect{ g_vertexShaderSource, g_fragmentShaderSource };

	std::vector<glm::vec3> vertexData;
	vertexData.push_back({ 0.0f, 0.5f, 0.0 });
	vertexData.push_back({ 0.5f,-0.5f, 0.0 });
	vertexData.push_back({ -0.5f,-0.5f, 0.0 });

	Buffer buffer{ vertexData.size() * sizeof(glm::vec3), vertexData.data() };
	buffer.attribute = effect.getAttributeLocation("position");

	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);

		effect.use();

		glBindBuffer(GL_ARRAY_BUFFER, buffer.id);
		glEnableVertexAttribArray(buffer.attribute);
		glVertexAttribPointer(buffer.attribute, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexData.size());

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
