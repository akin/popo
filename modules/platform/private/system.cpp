#include <platform/system.h>
#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <stdexcept>
#include <SDL_keyboard.h>

namespace platform
{
	namespace 
	{
		void handleAppEvents(const SDL_Event& e, input::Emitter<input::ApplicationEvent>& emitter)
		{
			input::ApplicationEvent out;
			out.state = input::ApplicationState::none;
			out.timestamp = e.common.timestamp;
            switch(e.type)
            {
                case SDL_QUIT:
					out.state = input::ApplicationState::quitRequested;
					break;
                case SDL_APP_TERMINATING:
					out.state = input::ApplicationState::terminate;
					break;
                case SDL_APP_LOWMEMORY:
					out.state = input::ApplicationState::lowMemory;
					break;
                case SDL_APP_WILLENTERBACKGROUND:
					out.state = input::ApplicationState::soonBackground;
					break;
                case SDL_APP_DIDENTERBACKGROUND:
					out.state = input::ApplicationState::background;
					break;
                case SDL_APP_WILLENTERFOREGROUND:
					out.state = input::ApplicationState::soonForeground;
					break;
                case SDL_APP_DIDENTERFOREGROUND:
					out.state = input::ApplicationState::foregeround;
					break;
                case SDL_LOCALECHANGED:
				default: 
					break;
			}
			if(out.state != input::ApplicationState::none)
			{
				emitter.emit(out);
			}
		}
		
		void handleKeyEvents(const SDL_Event& e, const std::unordered_map<SDL_Keycode, input::SemanticID>& keysemantic, input::Emitter<input::KeyEvent>& emitter)
		{
			input::KeyEvent event;
			event.timestamp = e.common.timestamp;
            
			switch(e.type)
            {
                case SDL_KEYDOWN:
                case SDL_KEYUP:
				{
					const SDL_Keysym& keysym = e.key.keysym;
					auto  iter = keysemantic.find(keysym.sym);
					if(iter != keysemantic.end())
					{
						event.state = e.type == SDL_KEYDOWN ? input::KeyState::down : input::KeyState::up;
						event.id = iter->second;
					}
					break;
				}
                case SDL_TEXTEDITING:
                case SDL_TEXTINPUT:
                case SDL_KEYMAPCHANGED:
				default:
					break;
			}
			if(event.state != input::KeyState::none)
			{
				emitter.emit(event);
			}
		}

		void handleMouseEvents(const SDL_Event& e, const std::unordered_map<uint8_t, input::SemanticID>& buttonSemantic, input::Emitter<input::MouseMoveEvent>& mouse, input::Emitter<input::KeyEvent>& key)
		{
			switch(e.type)
            {
                case SDL_MOUSEMOTION:
				{
					const SDL_MouseMotionEvent& motion = e.motion;
					input::MouseMoveEvent mouseEvent;
					mouseEvent.timestamp = e.common.timestamp;

					mouseEvent.position.x = motion.x;
					mouseEvent.position.y = motion.y;
					mouseEvent.delta.x = motion.xrel;
					mouseEvent.delta.y = motion.yrel;
					mouse.emit(mouseEvent);

					break;
				}
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
				{
					input::KeyEvent keyEvent;
					keyEvent.timestamp = e.common.timestamp;
					const SDL_MouseButtonEvent& button = e.button;
					auto  iter = buttonSemantic.find(button.button);
					if(iter != buttonSemantic.end())
					{
						keyEvent.state = e.type == SDL_MOUSEBUTTONDOWN ? input::KeyState::down : input::KeyState::up;
						keyEvent.id = iter->second;
					}
					key.emit(keyEvent);
					break;
				}
                case SDL_MOUSEWHEEL:
                default:
					// unhandled..
					break;
			}
		}

		void handleJoystickEvents(const SDL_Event& e)
		{
		}
	}
	System::System()
	{
		//Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0)
		{
			std::string error = fmt::format("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
			spdlog::error(error);
			throw std::runtime_error(error);
		}
	}

	System::~System()
	{
	}

	std::unique_ptr<Window> System::createWindow()
	{
		return std::make_unique<Window>();
	}

	void System::update()
	{
        SDL_Event e;
        while( SDL_PollEvent( &e ) != 0 )
        {
            switch(e.type)
            {
                case SDL_QUIT:
                case SDL_APP_TERMINATING:
                case SDL_APP_LOWMEMORY:
                case SDL_APP_WILLENTERBACKGROUND:
                case SDL_APP_DIDENTERBACKGROUND:
                case SDL_APP_WILLENTERFOREGROUND:
                case SDL_APP_DIDENTERFOREGROUND:
                case SDL_LOCALECHANGED:
					handleAppEvents(e, application);
                    break;
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                case SDL_TEXTEDITING:
                case SDL_TEXTINPUT:
                case SDL_KEYMAPCHANGED:
					handleKeyEvents(e, m_keySemanticMap, key);
                    break;
                case SDL_MOUSEMOTION:
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                case SDL_MOUSEWHEEL:
					handleMouseEvents(e, m_mouseSemanticMap, mouse, key);
                    break;
                default:
					// unhandled..
					break;
            }
        }
	}
	
	void System::setKeyMap(const input::KeyMap& keymap)
	{
		m_keymap.clear();
		m_keySemanticMap.clear();
		m_mouseSemanticMap.clear();

		std::unordered_map<std::string, SDL_Keycode> strToKey {
			{"space", SDLK_SPACE},
			{"escape", SDLK_ESCAPE},
		};

		for(auto iter : keymap)
		{
			// Standard keys, filtered by SDL (hopefully handles unicode :)
			const std::string& key = iter.first;
			const input::SemanticID semantic = iter.second;
			SDL_Keycode code = SDL_GetKeyFromName(key.c_str());
			if(code != SDLK_UNKNOWN)
			{
				switch(code)
				{
				case SDLK_SPACE:
					m_keymap["space"] = semantic;
					break;
				default:
					m_keymap[key] = semantic;
					break;
				}
				m_keySemanticMap[code] = semantic;
				continue;
			}
			
			// Find custom keys
			auto iter = strToKey.find(key);
			if(iter != strToKey.end())
			{
				m_keySemanticMap[iter->second] = semantic;
				m_keymap[key] = semantic;
				continue;
			}

			// Find mouse keys
			static std::string mouse{"mouse"};
			if(key.substr(0, mouse.size()) == mouse)
			{
				uint8_t id = 0;
				if(key.size() > mouse.size())
				{
					std::string end = key.substr(mouse.size());
					id = static_cast<uint8_t>(std::stoi(end));
				}
				m_mouseSemanticMap[id] = semantic;
				m_keymap[key] = semantic;
				continue;
			}
		}
	}

	const input::KeyMap& System::getKeyMap() const
	{
		return m_keymap;
	}
} // ns platform