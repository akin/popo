#include <platform/window.h>
#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <stdexcept>
#include <SDL_syswm.h>

namespace platform
{
	Window::Window()
	{
	}

	Window::~Window()
	{
	}

	void Window::init(glm::ivec2 size, glm::ivec2 pos, const std::string& title)
	{
		//Create window
		// SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED
		m_window = SDL_CreateWindow(title.c_str(), pos.x, pos.y, size.x, size.y, SDL_WINDOW_SHOWN);
		if (m_window == nullptr)
		{
			std::string error = fmt::format("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			spdlog::error(error);
			throw std::runtime_error(error);
		}
	}

	void Window::setTitle(const std::string& title)
	{
		m_title = title;
	}

	const std::string& Window::getTitle() const
	{
		return m_title;
	}

	void Window::setLockFocus(bool state)
	{
		SDL_SetRelativeMouseMode(state ? SDL_TRUE : SDL_FALSE);
	}

	uint32_t Window::getWidth() const
	{
		int w, h;
		SDL_GetWindowSize(m_window, &w, &h);
		return static_cast<uint32_t>(w);
	}

	uint32_t Window::getHeight() const
	{
		int w, h;
		SDL_GetWindowSize(m_window, &w, &h);
		return static_cast<uint32_t>(h);
	}

	common::WindowHandle Window::getHandle() const
	{
		SDL_SysWMinfo info;
		SDL_VERSION(&info.version);
		SDL_GetWindowWMInfo(m_window, &info);

		common::WindowHandle handle;
		handle.hinstance = GetModuleHandle(0);
		handle.hwnd = info.info.win.window;
		return handle;
	}
} // ns platform
