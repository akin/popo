#ifndef PLATFORM_SYSTEN_H_INCLUDED_POPO
#define PLATFORM_SYSTEN_H_INCLUDED_POPO

#include <string>
#include "Window.h"
#include <SDL.h>
#include <input/action/application.h>
#include <input/action/key.h>
#include <input/action/mouse.h>
#include <input/keymap.h>
#include <input/emitter.h>
#include <memory>

namespace platform
{
	class System final
	{
	public:
		System();
		~System();

		std::unique_ptr<Window> createWindow();
		
		void update();

		void setKeyMap(const input::KeyMap& keymap);
		const input::KeyMap& getKeyMap() const;

		input::Emitter<input::ApplicationEvent> application;
		input::Emitter<input::KeyEvent> key;
		input::Emitter<input::MouseMoveEvent> mouse;
	private:
		input::KeyMap m_keymap;
		std::unordered_map<SDL_Keycode, input::SemanticID> m_keySemanticMap;
		std::unordered_map<uint8_t, input::SemanticID> m_mouseSemanticMap;
	};
} // ns platform

#endif // PLATFORM_SYSTEN_H_INCLUDED_POPO