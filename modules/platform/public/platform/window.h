#ifndef PLATFORM_WINDOW_H_INCLUDED_POPO
#define PLATFORM_WINDOW_H_INCLUDED_POPO

#include <string>
#include <SDL.h>
#include <glm/glm.hpp>
#include <common/platform.h>

namespace platform
{
	class Window
	{
	public:
		explicit Window();
		~Window();

		void init(glm::ivec2 size, glm::ivec2 pos, const std::string& title);

		void setTitle(const std::string& title);
		const std::string& getTitle() const;

		void setLockFocus(bool state);

		uint32_t getWidth() const;
		uint32_t getHeight() const;

		common::WindowHandle getHandle() const;
	private:
		std::string m_title;

		SDL_Window* m_window = nullptr;
	};
} // ns platform

#endif // PLATFORM_WINDOW_H_INCLUDED_POPO