#ifndef INPUT_LISTENER_H_INCLUDED
#define INPUT_LISTENER_H_INCLUDED

#include <string>
#include <functional>

namespace input
{
    template <class CType>
    class Listener
    {
    public:
        virtual ~Listener() = default;

        virtual void handle(const CType& event) = 0;
    };
} // ns input

#endif // INPUT_LISTENER_H_INCLUDED