#ifndef INPUT_MOUSE_H_INCLUDED
#define INPUT_MOUSE_H_INCLUDED

#include <input/action.h>
#include <input/action/event.h>
#include <functional>
#include <glm/glm.hpp>

namespace input
{
	struct MouseState
	{
		glm::ivec2 position;
		glm::ivec2 delta;
	};

	struct MouseMoveEvent : public Event, MouseState
	{
	};

	CreateInputActionType(MouseMoveAction, MouseMoveEvent, uint32_t);

} // input 

#endif // INPUT_MOUSE_H_INCLUDED