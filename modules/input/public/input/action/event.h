#ifndef INPUT_EVENT_H_INCLUDED
#define INPUT_EVENT_H_INCLUDED

#include <stdint.h>

namespace input
{
	struct Event
	{
		uint32_t timestamp;
    protected:
        ~Event() = default;
	};
} // ns input

#endif // INPUT_EVENT_H_INCLUDED