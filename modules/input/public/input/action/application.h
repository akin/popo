#ifndef INPUT_APPLICATION_H_INCLUDED
#define INPUT_APPLICATION_H_INCLUDED

#include <stdint.h>
#include <string>
#include <input/action.h>
#include <input/action/event.h>
#include <functional>

namespace input
{
    enum class ApplicationState : uint32_t
    {
        none = 0,
        quitRequested,
        terminate,
        lowMemory,
        soonBackground,
        background,
        soonForeground,
        foregeround,
    };
    struct ApplicationEvent : public Event
    {
        ApplicationState state;
    };

	CreateInputActionType(ApplicationAction, ApplicationEvent, uint32_t);
} // ns input

#endif // INPUT_APPLICATION_H_INCLUDED