#ifndef INPUT_KEY_H_INCLUDED
#define INPUT_KEY_H_INCLUDED

#include <string>
#include <input/action.h>
#include <input/semantic.h>
#include <input/action/event.h>
#include <functional>
#include <stdint.h>

namespace input
{
	enum class KeyState : uint8_t
	{
		none = 0,
		up,
		down,
	};

	struct KeyEvent : public Event
	{
		SemanticID id = 0;
		KeyState state = KeyState::none;
	};

	CreateInputActionType(KeyAction, KeyEvent, SemanticID);
} // ns input

#endif // INPUT_KEY_H_INCLUDED