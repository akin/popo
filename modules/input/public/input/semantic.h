#ifndef INPUT_SEMANTIC_H_INCLUDED
#define INPUT_SEMANTIC_H_INCLUDED

#include <stdint.h>
#include <common/semanticmap.h>

namespace input
{
	using SemanticMap = common::SemanticMap;
	using SemanticID = common::SemanticID;

#define SEMANTIC_TYPE(NAME) NAME
	enum class Semantic : SemanticID
	{
		SEMANTIC_TYPE(none) = 0,

		SEMANTIC_TYPE(forward),
		SEMANTIC_TYPE(backward),
		SEMANTIC_TYPE(left),
		SEMANTIC_TYPE(left_strafe),
		SEMANTIC_TYPE(right),
		SEMANTIC_TYPE(right_strafe),
		SEMANTIC_TYPE(up),
		SEMANTIC_TYPE(down),
		SEMANTIC_TYPE(jump),
		SEMANTIC_TYPE(reset),
		SEMANTIC_TYPE(action),
		SEMANTIC_TYPE(alternative),
		SEMANTIC_TYPE(mouse1),
		SEMANTIC_TYPE(mouse2),
		SEMANTIC_TYPE(mouse3),
		SEMANTIC_TYPE(mouse4),
		SEMANTIC_TYPE(mouse5),
		SEMANTIC_TYPE(quit),

		maximum_predefined
	};
#undef SEMANTIC_TYPE

	void loadSemanticDefaults(SemanticMap& target);
} // ns input

#endif // INPUT_SEMANTIC_H_INCLUDED