#ifndef INPUT_KEYMAP_H_INCLUDED
#define INPUT_KEYMAP_H_INCLUDED

#include <string>
#include <unordered_map>
#include <input/semantic.h>

namespace input
{
	using KeyMap = std::unordered_map<std::string, SemanticID>;

	void loadKeymapDefaults(KeyMap& keymap, SemanticMap& semantics);
} // ns input

#endif // INPUT_KEYMAP_H_INCLUDED