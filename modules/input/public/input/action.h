#ifndef INPUT_ACTION_H_INCLUDED
#define INPUT_ACTION_H_INCLUDED

#include <string>

namespace input
{
    class Context;
	class Action
	{
	public:
		Action(Context& context, const std::string& name);

		virtual ~Action();
	protected:
        Context& m_context;
        std::string m_name;
	};

	#define CreateInputActionType(NAME, EVENT_STRUCT, IDENTIFIER) \
    using NAME##Call = std::function<bool(const EVENT_STRUCT&)>;\
    class Context;\
    class NAME final : public Action\
    {\
    public:\
        NAME(Context& context, IDENTIFIER identifier = {}, const std::string& name = "",const NAME##Call& func = {});\
        ~NAME() final;\
        const IDENTIFIER& getIdentifier() const;\
        void setAction(const NAME##Call& func);\
        bool update(const EVENT_STRUCT& event);\
    private:\
        IDENTIFIER m_identifier;\
        NAME##Call m_function;\
    }

#define CreateInputActionImpl(NAME, EVENT_STRUCT, IDENTIFIER) \
    NAME::NAME(Context& context, IDENTIFIER identifier, const std::string& name, const NAME##Call& func)\
    : Action(context, name)\
    , m_identifier(identifier)\
    , m_function(func)\
    {\
        m_context.add(*this);\
    }\
    NAME::~NAME()\
    {\
        m_context.remove(*this);\
    }\
    const IDENTIFIER& NAME::getIdentifier() const\
    {\
        return m_identifier;\
    }\
    void NAME::setAction(const NAME##Call& func)\
    {\
        m_function = func;\
    }\
    bool NAME::update(const EVENT_STRUCT& event)\
    {\
        if(m_function)\
        {\
            return m_function(event);\
        }\
        return false;\
    }
} // ns input

#endif // INPUT_ACTION_H_INCLUDED