#ifndef INPUT_EMITTER_H_INCLUDED
#define INPUT_EMITTER_H_INCLUDED

#include <string>
#include <functional>
#include <input/listener.h>

namespace input
{
    template <class CType>
	class Emitter
	{
	public:
		Emitter() = default;
		~Emitter() = default;

        void addListener(Listener<CType>* item)
        {
            m_listeners.push_back(item);
        }

        void removeListener(Listener<CType>* item)
        {
            // TODO! Solve removal :D
            //m_listeners.delete(func);
        }

        void emit(const CType& event)
        {
            for(auto& listener : m_listeners)
            {
                listener->handle(event);
            }
        }
	private:
        std::vector<Listener<CType>*> m_listeners;
	};
} // ns input

#endif // INPUT_EMITTER_H_INCLUDED