#ifndef INPUT_CONTEXT_H_INCLUDED
#define INPUT_CONTEXT_H_INCLUDED

namespace input
{
	class KeyAction;
	class MouseMoveAction;
	class ApplicationAction;
	class Context
	{
	public:
		Context();

		virtual ~Context() = default;

		virtual void add(KeyAction& action) = 0;
		virtual void remove(KeyAction& action) = 0;

		virtual void add(MouseMoveAction& action) = 0;
		virtual void remove(MouseMoveAction& action) = 0;

		virtual void add(ApplicationAction& action) = 0;
		virtual void remove(ApplicationAction& action) = 0;
	private:
	};
} // ns input

#endif // INPUT_CONTEXT_H_INCLUDED