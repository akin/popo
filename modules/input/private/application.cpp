#include <input/action/application.h>
#include <input/context.h>

namespace input
{
	CreateInputActionImpl(ApplicationAction, ApplicationEvent, uint32_t);
} // ns input