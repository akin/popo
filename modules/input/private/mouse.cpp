#include <input/action/mouse.h>
#include <input/context.h>

namespace input
{
	CreateInputActionImpl(MouseMoveAction, MouseMoveEvent, uint32_t);
} // ns input