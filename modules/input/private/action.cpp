#include <input/action.h>
#include <input/context.h>

namespace input
{
    Action::Action(Context& context, const std::string& name)
    : m_context(context)
    , m_name(name)
    {
    }

    Action::~Action()
    {
    }
} // ns input