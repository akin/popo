#include <input/keymap.h>

namespace input
{
	void loadKeymapDefaults(KeyMap& keymap, SemanticMap& semantics)
	{
		keymap["w"] = static_cast<SemanticID>(Semantic::forward);
		keymap["a"] = static_cast<SemanticID>(Semantic::left_strafe);
		keymap["d"] = static_cast<SemanticID>(Semantic::right_strafe);
		keymap["s"] = static_cast<SemanticID>(Semantic::backward);
		keymap["e"] = static_cast<SemanticID>(Semantic::down);
		keymap["q"] = static_cast<SemanticID>(Semantic::up);
		keymap["t"] = static_cast<SemanticID>(Semantic::action);
		keymap["space"] = static_cast<SemanticID>(Semantic::jump);
		keymap["mouse1"] = static_cast<SemanticID>(Semantic::mouse1);
		keymap["mouse2"] = static_cast<SemanticID>(Semantic::mouse2);
		keymap["mouse3"] = static_cast<SemanticID>(Semantic::mouse3);
		keymap["mouse4"] = static_cast<SemanticID>(Semantic::mouse4);
		keymap["mouse5"] = static_cast<SemanticID>(Semantic::mouse5);
		keymap["escape"] = static_cast<SemanticID>(Semantic::quit);
	}
} // ns input
