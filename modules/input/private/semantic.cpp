#include <input/semantic.h>

namespace input
{
	void loadSemanticDefaults(SemanticMap& target)
	{
#define SEMANTIC_TYPE(NAME)  {#NAME, Semantic::NAME}
		std::unordered_map<std::string, Semantic> map = {
			SEMANTIC_TYPE(forward),
			SEMANTIC_TYPE(backward),
			SEMANTIC_TYPE(left),
			SEMANTIC_TYPE(left_strafe),
			SEMANTIC_TYPE(right),
			SEMANTIC_TYPE(right_strafe),
			SEMANTIC_TYPE(up),
			SEMANTIC_TYPE(down),
			SEMANTIC_TYPE(jump),
			SEMANTIC_TYPE(reset),
			SEMANTIC_TYPE(action),
			SEMANTIC_TYPE(alternative),
			SEMANTIC_TYPE(mouse1),
			SEMANTIC_TYPE(mouse2),
			SEMANTIC_TYPE(mouse3),
			SEMANTIC_TYPE(mouse4),
			SEMANTIC_TYPE(mouse5),
			SEMANTIC_TYPE(quit),
		};
#undef SEMANTIC_TYPE

		for (auto& iter : map)
		{
			target.set(iter.first, static_cast<SemanticID>(iter.second));
		}
	}
} // ns input