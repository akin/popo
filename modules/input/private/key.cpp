#include <input/action/key.h>
#include <input/context.h>

namespace input
{
	CreateInputActionImpl(KeyAction, KeyEvent, SemanticID);
} // ns input