/**
 * @brief main entry point for the application
 * @file main.cpp
 * @author Mikael Korpela
 * @date 2.9.2021
 */

#include <iostream>
#include <unordered_map>

using SimpleArgs = std::unordered_map<std::string, std::string>;

/**
 * @brief command line arguments parsing function.
 * @param count number of arguments.
 * @param args c string array of arguments.
 * @param offset indicating offset of where the arguments start in c string array. On android this should be 0, in windows, the first argument is usually the executable name thus offset should be 1.
 * @return parsed SimpleArgs object with arguments in it.
 */
SimpleArgs parseArgs(int count, char** args, size_t offset = 1);

/**
 * @brief main entry point for application
 * @param count number of arguments
 * @param args c-string array
 * @return EXIT_SUCCESS (0) on success, EXIT_FAILURE (1) on failure.
 */
int main(int count, char** args)
{
	auto arguments = parseArgs(count, args);
	std::cout << "Hello World" << std::endl;

	std::string in;
	std::string out;
	SimpleArgs::iterator iter;
	if ((iter = arguments.find("in")) != arguments.end())
	{
		in = iter->second;
		std::cout << "Found in! " << in << std::endl;
	}
	if ((iter = arguments.find("out")) != arguments.end())
	{
		out = iter->second;
		std::cout << "Found out! " << out << std::endl;
	}

	return EXIT_SUCCESS;
}

SimpleArgs parseArgs(int count, char** args, size_t offset)
{
	SimpleArgs arguments;
	for (size_t i = offset + 1; i < count; i += 2)
	{
		arguments[args[i - 1]] = args[i];
	}
	return arguments;
}