#include <iostream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <graphics_common.h>
#include <glm/glm.hpp>

#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#define JSON_NOEXCEPTION
#include <tiny_gltf.h>

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

const std::string g_vertexShaderSource{
R"SHADER(
attribute vec3 position;
attribute vec2 uv;

uniform mat4 matrix;

varying vec2 texcoord;

void main()
{
	gl_Position = matrix * vec4(position, 1.0);
	texcoord = uv;
}
)SHADER"
};

const std::string g_fragmentShaderSource{
R"SHADER(
uniform sampler2D texture;

varying vec2 texcoord;

void main()
{
	gl_FragColor = texture2D(texture, texcoord);
}
)SHADER"
};

class Effect
{
public:
	Effect(const std::string& vertexSource, const std::string& fragmentSource)
	{
		program = glCreateProgram();
		{
			vertexShader = glCreateShader(GL_VERTEX_SHADER);
			const GLchar* src = static_cast<const GLchar*>(vertexSource.data());
			const GLint size = static_cast<GLint>(vertexSource.size());
			glShaderSource(vertexShader, 1, &src, &size);
			if (!compileShader(vertexShader))
			{
				glDeleteShader(vertexShader);
				assert(false);
				std::abort();
			}
			glAttachShader(program, vertexShader);
		}
		{
			fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
			const GLchar* src = static_cast<const GLchar*>(fragmentSource.data());
			const GLint size = static_cast<GLint>(fragmentSource.size());
			glShaderSource(fragmentShader, 1, &src, &size);
			if (!compileShader(fragmentShader))
			{
				glDeleteShader(fragmentShader);
				assert(false);
				std::abort();
			}
			glAttachShader(program, fragmentShader);
		}
		glLinkProgram(program);
	}

	~Effect()
	{
		glDeleteProgram(program);
		glDeleteShader(fragmentShader);
		glDeleteShader(vertexShader);
	}

	void use()
	{
		glUseProgram(program);
	}

	bool compileShader(uint32_t shader)
	{
		glCompileShader(shader);

		GLint isCompiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

			spdlog::error("Failed to compile shader {0}", errorLog.data());

			return false;
		}
		return true;
	}

	uint32_t program = 0;
	uint32_t vertexShader = 0;
	uint32_t fragmentShader = 0;
};

class Buffer
{
public:
	Buffer(size_t bytesize)
	{
		GLuint bufferID;
		glGenBuffers(1, &bufferID);
		id = bufferID;
		size = bytesize;
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(size), nullptr, GL_STATIC_DRAW);
	}

	Buffer(size_t bytesize, const void* data)
		: Buffer(bytesize)
	{
		set(data);
	}

	~Buffer()
	{
		GLuint bufferID = id;
		glDeleteBuffers(1, &bufferID);
	}

	void set(const void* data)
	{
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(size), data, GL_STATIC_DRAW);
	}

	uint32_t id = 0;
	size_t size = 0;
};

class Attribute
{
public:
	Attribute(const Effect& effect, const std::string& name_)
		: name(name_)
	{
		id = glGetAttribLocation(effect.program, name.c_str());
		assert(id >= 0);
	}

	std::string name;
	GLint id = 0;
};

class Uniform
{
public:
	Uniform(const Effect& effect, const std::string& name_)
		: name(name_)
	{
		id = glGetUniformLocation(effect.program, name.c_str());
		assert(id >= 0);
	}

	std::string name;
	GLint id = 0;
};

struct Vertex
{
	float x, y, z;
	float u, v;
};

bool loadModel(tinygltf::Model& model, const std::string& filename)
{
	tinygltf::TinyGLTF loader;
	std::string err;
	std::string warn;
	bool res = loader.LoadBinaryFromFile(&model, &err, &warn, filename);
	if (!warn.empty())
	{
		spdlog::warn(warn);
	}
	if (!err.empty())
	{
		spdlog::error(err);
	}
	return res;
}

int main(int argc, char* argv[])
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		spdlog::error("Failed to initialize GLAD");
		return -1;
	}

	Effect effect{ g_vertexShaderSource, g_fragmentShaderSource };

	tinygltf::Model model;
	std::string filepath{ "resources/resources/fox.glb" };
	if (!loadModel(model, filepath))
	{
		spdlog::error("Failed to load glTF: {0}", filepath);
		return EXIT_FAILURE;
	}

	std::vector<Vertex> vertexData;
	vertexData.push_back({ 0.0f, 0.5f, 0.0, 0.0f, 0.0 });
	vertexData.push_back({ 0.5f,-0.5f, 0.0, 0.0f, 0.0 });
	vertexData.push_back({ -0.5f,-0.5f, 0.0, 0.0f, 0.0 });

	Buffer buffer{ vertexData.size() * sizeof(glm::vec3), vertexData.data() };

	Attribute position{ effect, "position" };
	Attribute uv{ effect, "uv" };
	Uniform mvp{ effect, "matrix" };
	Uniform texture{ effect, "texture" };

	float matrix[16];
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);

		effect.use();

		glUniformMatrix4fv(mvp.id, 1, GL_FALSE, matrix);

		glBindBuffer(GL_ARRAY_BUFFER, buffer.id);
		glEnableVertexAttribArray(position.id);
		glVertexAttribPointer(position.id, 3, GL_FLOAT, GL_FALSE, 5, nullptr);
		glVertexAttribPointer(uv.id, 3, GL_FLOAT, GL_FALSE, 5, nullptr);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexData.size());

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
