#ifndef GRAPHICS_COMMON_H_
#define GRAPHICS_COMMON_H_

// https://www.glfw.org/docs/3.3/build_guide.html#build_link_cmake_source
#include <glm/glm.hpp>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>

#endif // GRAPHICS_COMMON_H_
