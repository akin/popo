cmake_minimum_required(VERSION 3.15)
project(OpenglGltfAPP C CXX)

##############################################
# Configure
list(APPEND CMAKE_MODULE_PATH 
	${CMAKE_CURRENT_SOURCE_DIR}/cmake
)

##############################################
# Declare dependencies
set(GLM_TEST_ENABLE OFF CACHE BOOL "" FORCE)
set(GLM_INSTALL_ENABLE OFF CACHE BOOL "" FORCE)
find_package(glm CONFIG REQUIRED)

find_package(OpenGL REQUIRED)

##############################################
# Create target and set properties

# Project stuffs
add_executable(${PROJECT_NAME})

set(CMAKE_MODULE_PATH 
	${CMAKE_MODULE_PATH} 
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake"
)

file(GLOB CURRENT_PUBLICS
	${CMAKE_CURRENT_LIST_DIR}/public/*.h
)

file(GLOB CURRENT_PRIVATES 
	${CMAKE_CURRENT_LIST_DIR}/private/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/private/*.c
	${CMAKE_CURRENT_LIST_DIR}/private/*.inl
	${CMAKE_CURRENT_LIST_DIR}/private/*.h
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_PUBLICS}
		${CURRENT_PRIVATES}
	)

target_include_directories(
	${PROJECT_NAME} 
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/private"
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/public"
)

target_link_libraries(
    ${PROJECT_NAME} 
	PRIVATE
		glm
		glfw
		glad
		fmt::fmt-header-only
		spdlog::spdlog_header_only
		nlohmann_json::nlohmann_json
)

## resources
add_custom_command(
	TARGET ${PROJECT_NAME} 
	POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_CURRENT_LIST_DIR}/resources" "${PROJECT_BINARY_DIR}/resources")

## features
target_compile_features(
	${PROJECT_NAME} 
	PRIVATE cxx_std_17
)

## Add system information
cmake_host_system_information(RESULT build_hostname QUERY HOSTNAME)
target_compile_definitions(
	${PROJECT_NAME} 
	PRIVATE 
		BUILD_SYSTEM_HOSTNAME=\"${build_hostname}\"
		BUILD_SYSTEM_VERSION=\"${CMAKE_SYSTEM_VERSION}\"
		BUILD_SYSTEM_NAME=\"${CMAKE_SYSTEM_NAME}\"
		BUILD_PROJECT_NAME=\"${PROJECT_NAME}\"
)

## Add GIT information
find_package(Git)
if(GIT_FOUND)
	execute_process(
		COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
		OUTPUT_VARIABLE GIT_HASH
			OUTPUT_STRIP_TRAILING_WHITESPACE
		)
	target_compile_definitions(${PROJECT_NAME} PRIVATE GIT_HASH=\"${GIT_HASH}\")
else()
	target_compile_definitions(${PROJECT_NAME} PRIVATE GIT_HASH=\"ERROR\")
	message(STATUS "GIT not found")
endif()
