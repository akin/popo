#ifndef TOY_ENTITY_H_
#define TOY_ENTITY_H_

#include <memory>
#include <unordered_map>
#include <typeinfo>
#include <typeindex>

namespace toy
{

using EntityID = uint32_t;
EntityID NO_ENTITY = 0;

class Component
{
public:
    Component(EntityID id)
    : m_id(id) {}

    EntityID getId() const
    {
        return m_id;
    }
private:
    EntityID m_id = NO_ENTITY;
};

class System
{
public:
    virtual ~System() {}
    
    virtual void attach(EntityID id) = 0;
    virtual void detach(EntityID id) = 0;

    virtual void update() = 0;

    virtual void clear() = 0;
};

class Manager
{
public:
    EntityID createEntity()
    {
        return ++m_currentEntityID;
    }

    void destroyEntity(EntityID id)
    {
        for(auto& iter : m_systems)
        {
            iter.second->detach(id);
        }
    }

    template <class CType, typename... Args>
    bool createSystem(Args... args)
    {
        std::type_index index = std::type_index(typeid(CType));
        auto iter = m_systems.find(index);
        if(iter != m_systems.end())
        {
            return false;
        }
        m_systems[index] = std::unique_ptr<System>(new CType(args...));
        return true;
    }

    template <class CType>
    CType* getSystem()
    {
        std::type_index index = std::type_index(typeid(CType));
        auto iter = m_systems.find(index);
        if(iter != m_systems.end())
        {
            return static_cast<CType*>(iter->second.get());
        }
        return nullptr;
    }

    void update()
    {
        for(auto& iter : m_systems)
        {
            iter.second->update();
        }
    }
private:
    EntityID m_currentEntityID = 10;
    std::unordered_map<std::type_index, std::unique_ptr<System>> m_systems;
};

class TransformSystem final : public System 
{
public:
    virtual ~TransformSystem() override 
    {}
    
    void attach(EntityID id) override 
    {}

    void detach(EntityID id) override 
    {}

    void update() override 
    {}

    void clear() override 
    {}
};

class Transform : public Component
{
public:
    Transform(Manager& mgr, EntityID id)
    : Component(id)
    , m_system(mgr.getSystem<TransformSystem>())
	{
        m_system->attach(id);
	}
private:
    TransformSystem* m_system = nullptr;
};

void tinyTest()
{
    Manager mgr;
    mgr.createSystem<TransformSystem>();

    EntityID myEntity = mgr.createEntity();

    Transform trans{mgr, myEntity};
}

} // ns toy

#endif // TOY_ENTITY_H_