#ifndef TOY_TILE_H_
#define TOY_TILE_H_

#include <cstdint>
#include <glm/glm.hpp>

namespace toy
{
	class Tile
	{
	public:
		Tile() = default;

		Tile(uint32_t type)
			: m_type(type)
		{
		}

		Tile(uint32_t type, const glm::vec4& color)
			: m_type(type)
			, m_color(color)
		{
		}

		Tile(const Tile& other)
			: m_type(other.m_type)
			, m_color(other.m_color)
		{
		}

		uint32_t getType() const
		{
			return m_type;
		}

		void setType(uint32_t type)
		{
			m_type = type;
		}

		void setColor(const glm::vec4& color)
		{
			m_color = color;
		}

		const glm::vec4& getColor()
		{
			return m_color;
		}
	private:
		uint32_t m_type = 0;
		glm::vec4 m_color{ 0.0f, 0.0f, 0.0f, 0.0f };
	};
} // ns toy

#endif // TOY_TILE_H_