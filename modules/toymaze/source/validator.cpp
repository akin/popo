#include "validator.h"

namespace toy 
{
bool checkConnection(const std::vector<Edge>& edges, const std::vector<glm::vec2>& points, size_t start, size_t end, std::vector<bool>& done)
{
    // indices of edged, that touch the vertex.
    std::vector<std::vector<size_t>> vertexEdges;
    vertexEdges.resize(points.size());
    for (size_t i = 0; i < edges.size(); ++i)
    {
        const Edge& edge = edges[i];
        vertexEdges[edge.first].push_back(i);
        vertexEdges[edge.second].push_back(i);
    }

    done.resize(points.size());
    std::fill(done.begin(), done.end(), false);

    std::queue<size_t> que;
    que.push(start);
    auto addPoint = [&que, &done](size_t vertexIndex) {
        if (done[vertexIndex]) {
            return;
        }
        done[vertexIndex] = true;
        que.push(vertexIndex);
    };
    addPoint(start);
    while (!que.empty())
    {
        size_t vertexIndex = que.front();
        que.pop();
        if (vertexIndex == end)
        {
            return true;
        }
        for (auto edgeIndex : vertexEdges[vertexIndex])
        {
            const Edge& edge = edges[edgeIndex];
            if (edge.first != vertexIndex)
            {
                addPoint(edge.first);
            }
            else
            {
                addPoint(edge.second);
            }
        }
    }
    return false;
}

bool checkConnection(const std::vector<Edge>& edges, const std::vector<glm::vec2>& points, size_t start, size_t end)
{
    std::vector<bool> done;
    return checkConnection(edges, points, start, end, done);
}

std::vector<size_t> getShortestPath(const std::vector<Edge>& edges, size_t start, size_t end, const std::vector<glm::vec2>& points, std::vector<float>& vertexDistances, std::vector<size_t>& vertexParents)
{
    // "a*"
    // indices of edged, that touch the vertex.
    std::vector<std::vector<size_t>> vertexEdges;
    vertexEdges.resize(points.size());

    for (size_t i = 0; i < edges.size(); ++i)
    {
        const Edge& edge = edges[i];
        vertexEdges[edge.first].push_back(i);
        vertexEdges[edge.second].push_back(i);
    }

    vertexDistances.resize(points.size());
    std::fill(vertexDistances.begin(), vertexDistances.end(), std::numeric_limits<float>::max());
    vertexParents.resize(points.size());
    std::fill(vertexParents.begin(), vertexParents.end(), std::numeric_limits<size_t>::max());

    {
        const glm::vec2& endPoint = points[end];
        // vertex index, vertex f value
        using PriorityIndex = std::pair<size_t, float>;
        auto cmp = [](const PriorityIndex& left, const PriorityIndex& right) { return left.second > right.second; };
        std::priority_queue<PriorityIndex, std::vector<PriorityIndex>, decltype(cmp)> process(cmp);

        auto addVertex = [&points, &vertexDistances, &vertexParents, &endPoint, &process](const size_t parent, const size_t vertexIndex, float distance)
        {
            if (vertexDistances[vertexIndex] != std::numeric_limits<float>::max())
            {
                return;
            }

            vertexParents[vertexIndex] = parent;
            vertexDistances[vertexIndex] = distance;
            float fValue = distance + glm::distance(points[vertexIndex], endPoint);
            process.push({ vertexIndex, fValue });
        };

        bool found = false;
        addVertex(start, start, 0.0f);
        while (!process.empty())
        {
            PriorityIndex value = process.top();
            process.pop();
            float vertexIndex = value.first;
            if (vertexIndex == end)
            {
                found = true;
                break;
            }

            float vertexDistance = vertexDistances[vertexIndex];

            // Add all links
            for (size_t edgeIndex : vertexEdges[vertexIndex])
            {
                const Edge& edge = edges[edgeIndex];
                float edgeDistance = glm::distance(points[edge.first], points[edge.second]);
                if (edge.first != vertexIndex)
                {
                    addVertex(vertexIndex, edge.first, vertexDistance + edgeDistance);
                }
                else
                {
                    addVertex(vertexIndex, edge.second, vertexDistance + edgeDistance);
                }
            }
        }

        if (!found)
        {
            return {};
        }
    }

    size_t current = end;
    auto findEdgeIndex = [&edges, &vertexEdges](size_t source, size_t target) -> size_t {
        for (size_t edgeIndex : vertexEdges[source])
        {
            const Edge& edge = edges[edgeIndex];
            if ((edge.first == source && edge.second == target) || (edge.first == target && edge.second == source))
            {
                return edgeIndex;
            }
        }
        assert(false && "WAT?");
        return std::numeric_limits<size_t>::max();
    };

    std::vector<size_t> result;
    while (current != start)
    {
        size_t parent = vertexParents[current];
        if (parent == current)
        {
            return {};
        }

        size_t edgeIndex = findEdgeIndex(current, parent);
        result.push_back(edgeIndex);
        current = parent;
    }
    return result;
}

std::vector<size_t> getShortestPath(const std::vector<Edge>& edges, size_t start, size_t end, const std::vector<glm::vec2>& points)
{
    std::vector<float> vertexDistances;
    std::vector<size_t> vertexParents;

    return getShortestPath(edges, start, end, points, vertexDistances, vertexParents);
}

} // ns toy