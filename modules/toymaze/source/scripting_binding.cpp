
#include "scripting_binding.h"
#include <string>
#include <glm/glm.hpp>
#include "scripting_state.h"

#include "tile.h"
#include "maze.h"
#include "mesh.h"
#include "generator.h"
#include "exporter.h"

namespace scripting
{
	void setupMath(State& state)
	{
		auto vec2 = state.bind<glm::vec2, glm::vec2(float, float)>("Vec2");
		vec2["x"] = &glm::vec2::x;
		vec2["y"] = &glm::vec2::y;
		auto ivec2 = state.bind<glm::ivec2, glm::ivec2(int, int)>("IVec2");
		ivec2["x"] = &glm::ivec2::x;
		ivec2["y"] = &glm::ivec2::y;

		auto u8vec4 = state.bind<glm::u8vec4, glm::u8vec4(uint8_t, uint8_t, uint8_t, uint8_t)>("U8Vec4");
		u8vec4["x"] = &glm::u8vec4::x;
		u8vec4["y"] = &glm::u8vec4::y;
		u8vec4["z"] = &glm::u8vec4::z;
		u8vec4["w"] = &glm::u8vec4::w;

		auto vec4 = state.bind<glm::vec4, glm::vec4(float, float, float, float)>("Vec4");
		vec4["x"] = &glm::vec4::x;
		vec4["y"] = &glm::vec4::y;
		vec4["z"] = &glm::vec4::z;
		vec4["w"] = &glm::vec4::w;
	}

	void setupMaze(State& state)
	{
		auto tile = state.bind<toy::Tile, toy::Tile(), toy::Tile(uint32_t), toy::Tile(uint32_t, const glm::vec4&)>("Tile");
		tile["getType"] = &toy::Tile::getType;
		tile["setType"] = &toy::Tile::setType;
		tile["setColor"] = &toy::Tile::setColor;
		tile["getColor"] = &toy::Tile::getColor;

		auto mazeConfig = state.bind<toy::MazeConfig, toy::MazeConfig()>("MazeConfig");
		mazeConfig["size"] = &toy::MazeConfig::size;
		mazeConfig["tiles"] = &toy::MazeConfig::tiles;
		mazeConfig["points"] = &toy::MazeConfig::points;

		auto maze = state.bind<toy::Maze, toy::Maze(glm::ivec2)>("Maze");
		maze["clear"] = &toy::Maze::clear;
		maze["getSize"] = &toy::Maze::getSize;
		maze["getTile"] = &toy::Maze::getTile;
		maze["setTile"] = &toy::Maze::setTile;
		maze["setTiles"] = &toy::Maze::setTiles;
		maze["isValid"] = &toy::Maze::isValid;
		maze["apply"] = &toy::Maze::apply;
		maze["getShortestPath"] = &toy::Maze::getShortestPath;
	}

	void setupMesh(State& state)
	{
		auto item = state.bind<toy::Mesh, toy::Mesh()>("Mesh");
	}

	void setupGenerator(State& state)
	{
		auto& global = state.getState();
		global["createMazeRandom"] = toy::createMaze;
		global["createMazeTriangles"] = toy::createTriangleMaze;
		global["createMazeVoronoi"] = toy::createVoronoiMaze;
	}

	void setupExporter(State& state)
	{
		auto& global = state.getState();
		global["saveMazeToImage"] = toy::saveMazeToImage;
		global["createUnitCube"] = toy::createUnitCube;
		global["saveMesh"] = toy::saveMesh;
	}

	void setup(State& state)
	{
		setupMath(state);
		setupMesh(state);
		setupMaze(state);
		setupExporter(state);
		setupGenerator(state);
	}
} // scripting
