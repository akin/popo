#ifndef TOY_SCRIPTING_STATE_H_
#define TOY_SCRIPTING_STATE_H_

#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <stack>
#include <regex>
#include <fmt/format.h>

//#define SOL_NO_EXCEPTIONS
#include <sol/sol.hpp>

namespace scripting
{
	class State
	{
	public:
		State()
		{
			m_state.open_libraries(sol::lib::base);

			// https://blog.rubenwardy.com/2020/07/26/sol3-script-sandbox/
			m_state.set_function("require", [this](const std::string& path) -> sol::object {
				return executeFile(resolveRequirePath(path));
				});
		}

		bool run(const std::string& script)
		{
			m_state.script(script);
			return true;
		}

		bool runFile(const std::string& path)
		{
			std::filesystem::path item(path);
			PathRAII tmp(m_paths, item.parent_path());
			m_state.script_file(path);

			return true;
		}

		template <class CType, typename... Args>
		sol::usertype<CType> bind(const std::string& className)
		{
			return m_state.new_usertype<CType>(className, sol::constructors<Args...>());
		}

		sol::state& getState()
		{
			return m_state;
		}
	private:
		class PathRAII
		{
		public:
			PathRAII(std::stack<std::filesystem::path>& paths, std::filesystem::path path)
				: m_paths(paths)
			{
				m_paths.push(path);
			}
			~PathRAII()
			{
				m_paths.pop();
			}
			std::stack<std::filesystem::path>& m_paths;
		};

		sol::state m_state;
		std::stack<std::filesystem::path> m_paths;

		std::string resolveRequirePath(const std::string& path)
		{
			std::regex rgx("[.]");
			std::sregex_token_iterator iter(path.begin(),
				path.end(),
				rgx,
				-1);

			std::filesystem::path item;
			std::sregex_token_iterator end;
			for (; iter != end; ++iter)
			{
				item.append(iter->str());
			}
			item.replace_extension("lua");
			return item.string();
		}

		sol::object loadString(const std::string& str, const std::string& chunkname, std::string& error)
		{
			if (!str.empty() && str[0] == LUA_SIGNATURE[0])
			{
				error = fmt::format("Bytecode prohibited by Lua sandbox, {0}", chunkname);
				return sol::nil;
			}

			sol::load_result result = m_state.load(str, chunkname, sol::load_mode::text);
			if (result.valid())
			{
				return result;
			}

			error = fmt::format("Error executing chunk [{0}] {1}", sol::to_string(result.status()), chunkname);
			return sol::nil;
		}

		sol::object executeFile(const std::string& path)
		{
			std::filesystem::path item = m_paths.top();
			item.append(path);
			PathRAII tmp(m_paths, item.parent_path());

			std::ifstream input(item);
			std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

			std::string error;
			sol::object ret = loadString(str, "@" + item.string(), error);
			if (ret == sol::nil)
			{
				throw sol::error(error);
			}

			sol::unsafe_function func = ret;
			return func();
		}
	};

} // ns scripting

#endif // TOY_SCRIPTING_STATE_H_