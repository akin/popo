#ifndef TOY_VALIDATOR_H_
#define TOY_VALIDATOR_H_

#include <functional>
#include <queue>
#include <limits>
#include "common.h"
#include "maze.h"
#include "tile.h"
#include "map2d.h"

namespace toy
{
	template <class CType>
	bool checkConnection(const Map2D<CType>& map2d, const glm::ivec2& start, const glm::ivec2& end, const std::function<bool(const CType&)>& cellCheck, Map2D<bool>& done)
	{
		// "flood fill" 
		auto size = map2d.getSize();
		done.resize(size, false);
		done.clear(false);

		std::queue<glm::ivec2> process;
		auto processCell = [&map2d, &done, &process, &cellCheck](const glm::ivec2& position) {
			if (!map2d.isValid(position) || done.get(position) || !cellCheck(map2d.get(position)))
			{
				return;
			}
			done.set(position, true);
			process.push(position);
		};

		// bootstrap, handle first tile.
		processCell(start);
		while (!process.empty())
		{
			glm::ivec2 point = process.front();
			process.pop();

			if (point == end)
			{
				return true;
			}

			processCell({ point.x, point.y - 1 });
			processCell({ point.x, point.y + 1 });
			processCell({ point.x + 1, point.y });
			processCell({ point.x - 1, point.y });
		}
		return false;
	}

	template <class CType>
	bool checkConnection(const Map2D<CType>& map2d, const glm::ivec2& start, const glm::ivec2& end, const std::function<bool(const CType&)>& cellCheck)
	{
		Map2D<bool> done;
		return checkConnection(map2d, start, end, cellCheck, done);
	}

	template <class CType>
	int32_t checkDistance(const Map2D<CType>& map2d, const glm::ivec2& start, const glm::ivec2& end, const std::function<bool(const CType&)>& cellCheck, Map2D<uint32_t>& done)
	{
		// "Dijkstras" 
		auto size = map2d.getSize();
		done.resize(size, std::numeric_limits<uint32_t>::max());
		done.clear(std::numeric_limits<uint32_t>::max());

		using PriorityIndex = std::pair<glm::ivec2, int32_t>;

		auto cmp = [](const PriorityIndex& left, const PriorityIndex& right) { return left.second > right.second; };
		std::priority_queue<PriorityIndex, std::vector<PriorityIndex>, decltype(cmp)> process(cmp);

		auto processCell = [&map2d, &done, &process, &cellCheck](const glm::ivec2& position, int32_t value) {
			if (!map2d.isValid(position) || done[position] != std::numeric_limits<uint32_t>::max() || !cellCheck(map2d.get(position)))
			{
				return;
			}
			done[position] = value;
			process.push({ position, value });
		};

		// bootstrap, handle first tile.
		processCell(start, 0);
		while (!process.empty())
		{
			PriorityIndex value = process.top();
			process.pop();
			glm::ivec2 point = value.first;
			int32_t total = value.second + 1;

			if (point == end)
			{
				return value.second;
			}

			processCell({ point.x, point.y - 1 }, total);
			processCell({ point.x, point.y + 1 }, total);
			processCell({ point.x + 1, point.y }, total);
			processCell({ point.x - 1, point.y }, total);
		}
		return -1;
	}

	template <class CType>
	int32_t checkDistance(const Map2D<CType>& map2d, const glm::ivec2& start, const glm::ivec2& end, const std::function<bool(const CType&)>& cellCheck)
	{
		Map2D<uint32_t> done;
		return checkDistance(map2d, start, end, cellCheck, done);
	}

	template <class CType>
	bool getShortestPath(const Map2D<CType>& map2d, const glm::ivec2& start, const glm::ivec2& end, const std::function<bool(const CType&)>& cellCheck, std::vector<glm::ivec2>& route, Map2D<uint32_t>& done)
	{
		// "Dijkstras" 
		auto size = map2d.getSize();
		done.resize(size, std::numeric_limits<uint32_t>::max());
		done.clear(std::numeric_limits<uint32_t>::max());

		using PriorityIndex = std::pair<glm::ivec2, int32_t>;

		auto cmp = [](const PriorityIndex& left, const PriorityIndex& right) { return left.second > right.second; };
		std::priority_queue<PriorityIndex, std::vector<PriorityIndex>, decltype(cmp)> process(cmp);

		{
			auto processCell = [&map2d, &done, &process, &cellCheck](const glm::ivec2& position, int32_t value) {
				if (!map2d.isValid(position) || done[position] != std::numeric_limits<uint32_t>::max() || !cellCheck(map2d.get(position)))
				{
					return;
				}
				done[position] = value;
				process.push({ position, value });
			};

			// bootstrap, handle first tile.
			processCell(start, 0);
			while (!process.empty())
			{
				PriorityIndex value = process.top();
				process.pop();
				glm::ivec2 point = value.first;
				int32_t total = value.second + 1;

				if (point == end)
				{
					break;
				}

				processCell({ point.x, point.y - 1 }, total);
				processCell({ point.x, point.y + 1 }, total);
				processCell({ point.x + 1, point.y }, total);
				processCell({ point.x - 1, point.y }, total);
			}
		}
		if (done.get(end) == std::numeric_limits<uint32_t>::max())
		{
			return false;
		}

		while (!process.empty())
		{
			process.pop();
		}
		{
			auto processCell = [&map2d, &done, &process](const glm::ivec2& position) {
				if (!map2d.isValid(position))
				{
					return;
				}
				auto value = done[position];
				if (value == std::numeric_limits<uint32_t>::max())
				{
					return;
				}
				process.push({ position, value });
			};

			processCell(end);
			while (!process.empty())
			{
				PriorityIndex value = process.top();
				process.pop();
				glm::ivec2 point = value.first;

				route.push_back(point);

				if (point == start)
				{
					return true;
				}

				processCell({ point.x, point.y - 1 });
				processCell({ point.x, point.y + 1 });
				processCell({ point.x + 1, point.y });
				processCell({ point.x - 1, point.y });
			}
		}

		return false;
	}

	template <class CType>
	bool getShortestPath(const Map2D<CType>& map2d, const glm::ivec2& start, const glm::ivec2& end, const std::function<bool(const CType&)>& cellCheck, std::vector<glm::ivec2>& route)
	{
		Map2D<uint32_t> done;
		return getShortestPath(map2d, start, end, cellCheck, route, done);
	}

	bool checkConnection(const std::vector<Edge>& edges, const std::vector<glm::vec2>& points, size_t start, size_t end, std::vector<bool>& done);
	bool checkConnection(const std::vector<Edge>& edges, const std::vector<glm::vec2>& points, size_t start, size_t end);

	std::vector<size_t> getShortestPath(const std::vector<Edge>& edges, size_t start, size_t end, const std::vector<glm::vec2>& points, std::vector<float>& vertexDistances, std::vector<size_t>& vertexParents);
	std::vector<size_t> getShortestPath(const std::vector<Edge>& edges, size_t start, size_t end, const std::vector<glm::vec2>& points);
} // ns toy

#endif // TOY_VALIDATOR_H_