#ifndef TOY_MAZE_H_
#define TOY_MAZE_H_

#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include "tile.h"
#include "map2d.h"
#include "mesh.h"

namespace toy
{
class MazeConfig
{
public:
	glm::ivec2 size{ 0, 0 };
	std::map<std::string, Tile> tiles;
	std::map<std::string, glm::ivec2> points;
};

class Maze
{
public:
	Maze(glm::ivec2 size);
	void setPoint(const std::string& key, glm::ivec2 point);
	bool getPoint(const std::string& key, glm::ivec2& point);
	Tile& getTile(const glm::ivec2& pos);
	const Tile& get(const glm::ivec2& pos) const;
	const glm::ivec2& getSize() const;
	void clear(const Tile& tile);
	void setTile(const glm::ivec2& pos, const Tile& tile);
	void setTiles(const std::vector<glm::ivec2>& positions, const Tile& tile);
	bool isValid(const glm::ivec2& point) const;
	const Map2D<Tile>& getTileMap2D();
	void apply(const Mesh& mesh, const Tile& tile);
	std::vector<glm::ivec2> getShortestPath(glm::ivec2 start, glm::ivec2 end, const Tile& compareTile);
private:
	Map2D<Tile> m_tiles;

	std::unordered_map<std::string, glm::ivec2> m_points;
};
} // ns toy

#endif // TOY_MAZE_H_