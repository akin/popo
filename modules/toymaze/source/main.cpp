
#include "scripting_state.h"
#include "scripting_binding.h"

int main(int count, char** args)
{
	scripting::State state;
	scripting::setup(state);

	if (count > 1)
	{
		for (size_t i = 1; i < count; ++i)
		{
			state.runFile(args[i]);
		}
	}
	else
	{
		state.runFile("resources/main.lua");
	}

	return 0;
}
