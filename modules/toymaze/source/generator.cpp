#include "generator.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <random>
#include "common.h"
#include "tile.h"
#include "validator.h"
#include "fuzzy.h"

#include <delaunator.hpp>

namespace toy
{
	Maze createMaze(const MazeConfig& config, size_t pathComplexity, size_t falsePaths, size_t falsePathComplexity, size_t seed)
	{
		Fuzzy fuzzy(seed);
		Maze maze{ config.size };

		glm::vec2 maxSize{ config.size };

		glm::vec2 start;
		glm::vec2 end;
		{
			auto iter = config.points.find("start");
			if(iter != config.points.end())
			{
				start = iter->second;
			}
			iter = config.points.find("end");
			if(iter != config.points.end())
			{
				end = iter->second;
			}
		}
		Tile empty;
		Tile wall;
		{
			auto iter = config.tiles.find("empty");
			if(iter != config.tiles.end())
			{
				empty = iter->second;
			}
			iter = config.tiles.find("wall");
			if(iter != config.tiles.end())
			{
				wall = iter->second;
			}
		}

		maze.setPoint("start", start);
		maze.setPoint("end", end);

		maze.clear(wall);

		std::vector<glm::vec2> points;
		std::vector<Edge> edges;

		points.push_back(start);
		fuzzy.getMultiple(pathComplexity, maxSize, points);
		points.push_back(end);

		for (size_t i = 1; i < points.size(); ++i)
		{
			edges.push_back({
				i - 1, i
				});
		}

		// Create false paths
		size_t extraPaths = fuzzy.get<size_t>(1, falsePaths);
		for (size_t i = 0; i < extraPaths; ++i)
		{
			size_t complexity = fuzzy.get(falsePathComplexity);
			const Edge& edge = edges[fuzzy.get(edges.size() - 1)];

			const auto& start = points[edge.first];
			const auto& end = points[edge.second];

			auto newPointStart = glm::normalize(start - end) * fuzzy.get(glm::distance(start, end));

			size_t startPoint = points.size();
			points.push_back(newPointStart);
			fuzzy.getMultiple(complexity, maxSize, points);

			for (size_t i = startPoint + 1; i < points.size(); ++i)
			{
				edges.push_back({
					i - 1, i
					});
			}
		}

		Mesh mesh;
		mesh.setTopology(Topology::none);
		mesh.setAttribute(Attribute::position, points);
		mesh.setEdges(edges);

		// draw edges..
		maze.apply(mesh, empty);

		return std::move(maze);
	}

	Maze createTriangleMaze(const MazeConfig& config, size_t pointCount, size_t seed)
	{
		Fuzzy fuzzy(seed);
		Maze maze{ config.size };
		glm::vec2 maxSize{ config.size };

		glm::vec2 start;
		glm::vec2 end;
		{
			auto iter = config.points.find("start");
			if(iter != config.points.end())
			{
				start = iter->second;
			}
			iter = config.points.find("end");
			if(iter != config.points.end())
			{
				end = iter->second;
			}
		}
		Tile empty;
		Tile wall;
		{
			auto iter = config.tiles.find("empty");
			if(iter != config.tiles.end())
			{
				empty = iter->second;
			}
			iter = config.tiles.find("wall");
			if(iter != config.tiles.end())
			{
				wall = iter->second;
			}
		}

		maze.setPoint("start", start);
		maze.setPoint("end", end);

		maze.clear(wall);

		std::vector<glm::vec2> points;
		points.push_back(start);
		points.push_back(end);

		fuzzy.getMultiple(pointCount, maxSize, points);

		std::vector<std::size_t> triangles;
		{
			std::vector<double> coords;
			for (const glm::ivec2& point : points)
			{
				coords.push_back(point.x);
				coords.push_back(point.y);
			}
			delaunator::Delaunator d(coords);
			triangles = d.triangles;
		}

		const size_t triangleCount = triangles.size() / 3;
		std::vector<Edge> edges;
		edges.reserve(triangleCount * 3);
		for (size_t triangle = 0; triangle < triangleCount; ++triangle)
		{
			const size_t offset = triangle * 3;
			edges.push_back({
				triangles[offset + 0],
				triangles[offset + 1],
				});
			edges.push_back({
				triangles[offset + 1],
				triangles[offset + 2],
				});
			edges.push_back({
				triangles[offset + 2],
				triangles[offset + 0],
				});
		}

		std::vector<float> vertexDistances;
		std::vector<size_t> vertexParents;

		if (!getShortestPath(edges, 0, 1, points, vertexDistances, vertexParents).empty())
		{
			Edge edge;
			while (!getShortestPath(edges, 0, 1, points, vertexDistances, vertexParents).empty())
			{
				size_t index = fuzzy.get<size_t>(edges.size() - 1);
				edge = edges[index];

				std::swap(edges[index], edges.back());
				edges.pop_back();
			}
			edges.push_back(edge);
		}
		else
		{
			assert(false && "no path!");
		}

		Mesh mesh;
		mesh.setTopology(Topology::none);
		mesh.setAttribute(Attribute::position, points);
		mesh.setEdges(edges);

		// draw edges..
		maze.apply(mesh, empty);

		return std::move(maze);
	}

	Maze createVoronoiMaze(const MazeConfig& config, size_t pointCount, size_t seed)
	{
		Fuzzy fuzzy(seed);
		Maze maze{ config.size };
		glm::vec2 maxSize{ config.size };

		glm::vec2 start;
		glm::vec2 end;
		{
			auto iter = config.points.find("start");
			if(iter != config.points.end())
			{
				start = iter->second;
			}
			iter = config.points.find("end");
			if(iter != config.points.end())
			{
				end = iter->second;
			}
		}
		Tile empty;
		Tile wall;
		{
			auto iter = config.tiles.find("empty");
			if(iter != config.tiles.end())
			{
				empty = iter->second;
			}
			iter = config.tiles.find("wall");
			if(iter != config.tiles.end())
			{
				wall = iter->second;
			}
		}

		maze.setPoint("start", start);
		maze.setPoint("end", end);

		maze.clear(wall);
		std::vector<Edge> edges;
		std::vector<glm::vec2> points;
		{
			std::vector<glm::vec2> trianglePoints;
			trianglePoints.push_back(start);
			trianglePoints.push_back(end);

			fuzzy.getMultiple(pointCount, maxSize, trianglePoints);

			std::vector<size_t> triangles;
			{
				std::vector<double> coords;
				for (const auto& point : trianglePoints)
				{
					coords.push_back(point.x);
					coords.push_back(point.y);
				}
				delaunator::Delaunator d(coords);
				triangles = d.triangles;
			}

			std::vector<glm::vec2> circleMidPoints;
			struct TriangleEdge {
				int32_t p1 = -1;
				int32_t p2 = -1;

				int32_t t1 = -1;
				int32_t t2 = -1;
			};

			std::vector<TriangleEdge> triangleEdges;
			{
				const size_t triangleCount = triangles.size() / 3;
				triangleEdges.reserve(triangleCount * 3);
				circleMidPoints.reserve(triangleCount);

				auto findEdge = [&triangleEdges](size_t p1, size_t p2, size_t& index) -> bool {
					if (p2 < p1)
					{
						std::swap(p1, p2);
					}
					for (size_t i = 0; i < triangleEdges.size(); ++i)
					{
						const auto& edge = triangleEdges[i];
						if (edge.p1 == p1 && edge.p2 == p2)
						{
							index = i;
							return true;
						}
					}
					return false;
				};
				auto addTriangleToEdges = [&triangleEdges, &findEdge](size_t p1, size_t p2, size_t circlePointIndex)
				{
					if (p2 < p1)
					{
						std::swap(p1, p2);
					}
					size_t index;

					if (!findEdge(p1, p2, index))
					{
						index = triangleEdges.size();
						TriangleEdge edge;
						edge.p1 = p1;
						edge.p2 = p2;
						triangleEdges.push_back(std::move(edge));
					}
					auto& edge = triangleEdges[index];
					if (edge.t1 == -1)
					{
						edge.t1 = circlePointIndex;
					}
					else if (edge.t2 == -1)
					{
						edge.t2 = circlePointIndex;
					}
					else
					{
						assert(false && "edges cant have 2 triangles.");
					}
				};
				for (size_t ti = 0; ti < triangleCount; ++ti)
				{
					const size_t p1 = triangles[ti * 3 + 0];
					const size_t p2 = triangles[ti * 3 + 1];
					const size_t p3 = triangles[ti * 3 + 2];

					glm::vec2 cm = getCircleMidPoint(trianglePoints[p1], trianglePoints[p2], trianglePoints[p3]);

					if (!maze.isValid(cm))
					{
						// Skip points outsize of maze
						continue;
					}

					size_t circlePointIndex = circleMidPoints.size();
					circleMidPoints.push_back({ (int)cm.x, (int)cm.y });

					addTriangleToEdges(p1, p2, circlePointIndex);
					addTriangleToEdges(p1, p3, circlePointIndex);
					addTriangleToEdges(p2, p3, circlePointIndex);
				}
			}

			edges.reserve(triangleEdges.size());
			for (const auto& value : triangleEdges)
			{
				if (value.t1 == -1 || value.t2 == -1)
				{
					continue;
				}
				edges.push_back({ value.t1, value.t2 });
			}
			points = circleMidPoints;
		}

		size_t startIndex;
		size_t endIndex;
		{
			size_t startP = getClosestPointIndex(start, points);
			size_t endP = getClosestPointIndex(end, points);

			// Connect start and end
			size_t offset = points.size();

			points.push_back(start);
			points.push_back(end);

			edges.push_back({ startP, offset });
			edges.push_back({ endP, offset + 1 });

			startIndex = offset;
			endIndex = offset + 1;
		}

		std::vector<float> vertexDistances;
		std::vector<size_t> vertexParents;
		if (!getShortestPath(edges, startIndex, endIndex, points, vertexDistances, vertexParents).empty())
		{
			std::vector<Edge> undo;
			while (true)
			{
				size_t index = fuzzy.get<size_t>(edges.size() - 1);
				undo.push_back(edges[index]);

				std::swap(edges[index], edges.back());
				edges.pop_back();

				if (getShortestPath(edges, startIndex, endIndex, points, vertexDistances, vertexParents).empty())
				{
					edges.push_back(undo.back());
					break;
				}
			}
		}
		/*
		else
		{
			assert(false && "no path!");
		}
		/**/
		Mesh mesh;
		mesh.setTopology(Topology::none);
		mesh.setAttribute(Attribute::position, points);
		mesh.setEdges(edges);

		// draw edges..
		maze.apply(mesh, empty);

		return std::move(maze);
	}
} // ns toy
