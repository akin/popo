#ifndef TOY_FUZZY_H_
#define TOY_FUZZY_H_

#include <cstdint>
#include <cstddef>
#include <random>
#include <glm/glm.hpp>

namespace toy
{
	class Fuzzy
	{
	public:
		Fuzzy(size_t seed = 0)
		{
			setSeed(seed);
		}

		void setSeed(size_t seed)
		{
			m_seed = seed;
			m_generator.seed(m_seed);
		}

		size_t getSeed() const
		{
			return m_seed;
		}

		template <class CType>
		CType get(const CType& max)
		{
			CType min{};
			return get<CType>(min, max);
		}

		template <class CType>
		CType get(const CType& min, const CType& max)
		{
			assert(false && "No implementation.");
			return {};
		}

		template <class CType>
		void getMultiple(size_t count, const CType& max, std::vector<CType>& out)
		{
			CType min{};
			return getMultiple(count, min, max, out);
		}

		template <class CType>
		void getMultiple(size_t count, const CType& min, const CType& max, std::vector<CType>& out)
		{
			assert(false && "No implementation.");
			return {};
		}

        template <class CType>
		CType getCircle(float radius)
		{
			assert(false && "No implementation.");
			return {};
		}
	private:
		size_t m_seed = 0;
		std::default_random_engine m_generator;
	};

	/// size_t
	template <>
	size_t Fuzzy::get<size_t>(const size_t& min, const size_t& max)
	{
		std::uniform_int_distribution<size_t> dist{ min, max };
		return dist(m_generator);
	}

	template <>
	void Fuzzy::getMultiple<size_t>(size_t count, const size_t& min, const size_t& max, std::vector<size_t>& out)
	{
		std::uniform_int_distribution<size_t> dist{ min, max };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ dist(m_generator) });
		}
	}

	/// float
	template <>
	float Fuzzy::get<float>(const float& min, const float& max)
	{
		std::uniform_real_distribution<float> dist{ min, max };
		return dist(m_generator);
	}

	template <>
	void Fuzzy::getMultiple<float>(size_t count, const float& min, const float& max, std::vector<float>& out)
	{
		std::uniform_real_distribution<float> dist{ min, max };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ dist(m_generator) });
		}
	}

	/// double
	template <>
	double Fuzzy::get<double>(const double& min, const double& max)
	{
		std::uniform_real_distribution<double> dist{ min, max };
		return dist(m_generator);
	}

	template <>
	void Fuzzy::getMultiple<double>(size_t count, const double& min, const double& max, std::vector<double>& out)
	{
		std::uniform_real_distribution<double> dist{ min, max };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ dist(m_generator) });
		}
	}

	/// ivec2
	template <>
	glm::ivec2 Fuzzy::get<glm::ivec2>(const glm::ivec2& min, const glm::ivec2& max)
	{
		std::uniform_int_distribution<int> xDist{ min.x, max.x };
		std::uniform_int_distribution<int> yDist{ min.y, max.y };
		return { xDist(m_generator), yDist(m_generator) };
	}

	template <>
	void Fuzzy::getMultiple<glm::ivec2>(size_t count, const glm::ivec2& min, const glm::ivec2& max, std::vector<glm::ivec2>& out)
	{
		std::uniform_int_distribution<int> xDist{ min.x, max.x };
		std::uniform_int_distribution<int> yDist{ min.y, max.y };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ xDist(m_generator), yDist(m_generator) });
		}
	}

	/// vec2
	template <>
	glm::vec2 Fuzzy::get<glm::vec2>(const glm::vec2& min, const glm::vec2& max)
	{
		std::uniform_real_distribution<float> xDist{ min.x, max.x };
		std::uniform_real_distribution<float> yDist{ min.y, max.y };
		return { xDist(m_generator), yDist(m_generator) };
	}

	template <>
	void Fuzzy::getMultiple<glm::vec2>(size_t count, const glm::vec2& min, const glm::vec2& max, std::vector<glm::vec2>& out)
	{
		std::uniform_real_distribution<float> xDist{ min.x, max.x };
		std::uniform_real_distribution<float> yDist{ min.y, max.y };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ xDist(m_generator), yDist(m_generator) });
		}
	}

	template <>
    glm::vec2 Fuzzy::getCircle<glm::vec2>(float radius)
    {
		std::uniform_real_distribution<float> dist{ -1.0f, 1.0f };
		std::uniform_real_distribution<float> power{ 0.0f, 1.0f };
        return glm::sqrt(power(m_generator)) * glm::normalize(glm::vec2{ dist(m_generator), dist(m_generator) }) * radius;
    }

	/// ivec3
	template <>
	glm::ivec3 Fuzzy::get<glm::ivec3>(const glm::ivec3& min, const glm::ivec3& max)
	{
		std::uniform_int_distribution<int> xDist{ min.x, max.x };
		std::uniform_int_distribution<int> yDist{ min.y, max.y };
		std::uniform_int_distribution<int> zDist{ min.z, max.z };
		return { xDist(m_generator), yDist(m_generator), zDist(m_generator) };
	}

	template <>
	void Fuzzy::getMultiple<glm::ivec3>(size_t count, const glm::ivec3& min, const glm::ivec3& max, std::vector<glm::ivec3>& out)
	{
		std::uniform_int_distribution<int> xDist{ min.x, max.x };
		std::uniform_int_distribution<int> yDist{ min.y, max.y };
		std::uniform_int_distribution<int> zDist{ min.z, max.z };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ xDist(m_generator), yDist(m_generator), zDist(m_generator) });
		}
	}

	/// vec3
	template <>
	glm::vec3 Fuzzy::get<glm::vec3>(const glm::vec3& min, const glm::vec3& max)
	{
		std::uniform_real_distribution<float> xDist{ min.x, max.x };
		std::uniform_real_distribution<float> yDist{ min.y, max.y };
		std::uniform_real_distribution<float> zDist{ min.z, max.z };
		return { xDist(m_generator), yDist(m_generator), zDist(m_generator) };
	}

	template <>
	void Fuzzy::getMultiple<glm::vec3>(size_t count, const glm::vec3& min, const glm::vec3& max, std::vector<glm::vec3>& out)
	{
		std::uniform_real_distribution<float> xDist{ min.x, max.x };
		std::uniform_real_distribution<float> yDist{ min.y, max.y };
		std::uniform_real_distribution<float> zDist{ min.z, max.z };
		out.reserve(out.size() + count);

		for (size_t i = 0; i < count; ++i)
		{
			out.push_back({ xDist(m_generator), yDist(m_generator), zDist(m_generator) });
		}
	}

	template <>
    glm::vec3 Fuzzy::getCircle<glm::vec3>(float radius)
    {
		std::uniform_real_distribution<float> dist{ -1.0f, 1.0f };
		std::uniform_real_distribution<float> power{ 0.0f, 1.0f };
        return glm::sqrt(power(m_generator)) * glm::normalize(glm::vec3{ dist(m_generator), dist(m_generator), dist(m_generator) }) * radius;
    }

} // ns toy

#endif // TOY_FUZZY_H_