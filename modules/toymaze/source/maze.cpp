#include "maze.h"
#include "common.h"
#include "validator.h"

namespace toy
{
Maze::Maze(glm::ivec2 size)
    : m_tiles(size)
{
}

void Maze::setPoint(const std::string& key, glm::ivec2 point)
{
    m_points[key] = point;
}

bool Maze::getPoint(const std::string& key, glm::ivec2& point)
{
    auto iter = m_points.find(key);
    if (iter != m_points.end())
    {
        point = iter->second;
        return true;
    }
    return false;
}

Tile& Maze::getTile(const glm::ivec2& pos)
{
    return m_tiles[pos];
}

const Tile& Maze::get(const glm::ivec2& pos) const
{
    return m_tiles[pos];
}

const glm::ivec2& Maze::getSize() const
{
    return m_tiles.getSize();
}

void Maze::clear(const Tile& tile)
{
    m_tiles.clear(tile);
}

void Maze::setTile(const glm::ivec2& pos, const Tile& tile)
{
    m_tiles[pos] = tile;
}

void Maze::setTiles(const std::vector<glm::ivec2>& positions, const Tile& tile)
{
    m_tiles.set(positions, tile);
}

bool Maze::isValid(const glm::ivec2& point) const
{
    return point.x >= 0 && point.x < getSize().x&& point.y >= 0 && point.y < getSize().y;
}

const Map2D<Tile>& Maze::getTileMap2D()
{
    return m_tiles;
}

void Maze::apply(const Mesh& mesh, const Tile& tile)
{
    auto pointsPtr = mesh.getAttribute<glm::vec2>(Attribute::position);
    if (pointsPtr == nullptr)
    {
        assert(false && "No positions!");
        return;
    }
    auto* edges = mesh.getEdges();
    if (edges == nullptr)
    {
        assert(false && "No edges!");
        return;
    }

    auto& points = *pointsPtr;
    for (const Edge& edge : *edges)
    {
        std::vector<glm::ivec2> pathPoints;
        getLinePoints(points[edge.first], points[edge.second], pathPoints);
        setTiles(pathPoints, tile);
    }
}

std::vector<glm::ivec2> Maze::getShortestPath(glm::ivec2 start, glm::ivec2 end, const Tile& compareTile)
{
    std::vector<glm::ivec2> points;
    if (!toy::getShortestPath<Tile>(getTileMap2D(), start, end, [&compareTile](const Tile& tile) -> bool { return tile.getType() == compareTile.getType(); }, points))
    {
        return {};
    }
    return points;
}
} // ns toy
