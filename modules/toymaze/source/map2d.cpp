#include "map2d.h"

namespace toy
{
template <>
bool& Map2D<bool>::operator [](const glm::ivec2& position)
{
    assert(false && "you shouldnt use this function with bool.");
    std::exit(-1);
    static bool garbage = false;
    return garbage;
}

template <>
const bool& Map2D<bool>::operator [](const glm::ivec2& position) const
{
    assert(false && "you shouldnt use this function with bool.");
    std::exit(-1);
    static bool garbage = false;
    return garbage;
}
} // ns toy