﻿#ifndef TOY_GENERATOR_H_
#define TOY_GENERATOR_H_

#include <cstddef>
#include "maze.h"

namespace toy
{
	Maze createMaze(const MazeConfig& config, size_t pathComplexity, size_t falsePaths, size_t falsePathComplexity, size_t seed);
	Maze createTriangleMaze(const MazeConfig& config, size_t pointCount, size_t seed);
	Maze createVoronoiMaze(const MazeConfig& config, size_t pointCount, size_t seed);
} // ns toy

#endif // TOY_MAZE_H_