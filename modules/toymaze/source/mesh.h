#ifndef TOY_MESH_H_
#define TOY_MESH_H_

#include <cstdint>
#include <variant>
#include <vector>
#include <unordered_map>
#include <glm/glm.hpp>
#include "common.h"

namespace toy
{
	class Mesh
	{
	public:
		Mesh() = default;

		Topology getTopology() const
		{
			return m_topology;
		}

		void setTopology(Topology topology)
		{
			m_topology = topology;
		}

		void setEdges(const std::vector<Edge>& edges)
		{
			m_edges = edges;
		}

		const std::vector<Edge>* getEdges() const
		{
			return &m_edges;
		}

		template <class CType>
		void setAttribute(Attribute attribute, const std::vector<CType>& data)
		{
			m_attributes[attribute] = data;
		}

		template <class CType>
		const std::vector<CType>* getAttribute(Attribute attribute) const
		{
			auto iter = m_attributes.find(attribute);
			if (iter == m_attributes.end())
			{
				return nullptr;
			}
			return std::get_if<std::vector<CType>>(&(iter->second));
		}

		template <class CType>
		void setIndices(const std::vector<CType>& data)
		{
			m_indices = data;
		}

		template <class CType>
		const std::vector<CType>* getIndices() const
		{
			return std::get_if<std::vector<CType>>(m_indices);
		}
	private:
		std::vector<Edge> m_edges;

		std::unordered_map<Attribute, std::variant<std::vector<glm::vec2>, std::vector<glm::vec3>, std::vector<uint16_t>>> m_attributes;
		std::variant<std::vector<uint16_t>, std::vector<uint32_t>> m_indices;
		Topology m_topology = Topology::none;
	};

} // ns toy

#endif // TOY_MESH_H_