#include "common.h"

#include <cmath>
#include <limits>

namespace toy
{
	void getLinePoints(glm::ivec2 start, glm::ivec2 end, std::vector<glm::ivec2>& points)
	{
		/// Generalized integer version
		/// for float, you need to take into account the remainder offsets.. 
		/// so that tMax doesnt start from 0.0, but at appopriate T distance.

		// http://www.cse.yorku.ca/~amana/research/grid.pdf
		glm::vec2 diff{ end.x - start.x , end.y - start.y };
		glm::ivec2 step;

		double x2 = std::abs(diff.x);
		double y2 = std::abs(diff.y);
		double t = std::sqrt(y2 * y2 + x2 * x2);

		// how much we need to move, to cross grid
		double tDeltaX = t / x2;
		double tDeltaY = t / y2;

		// where are we?
		float tMaxX = 0.0f;
		float tMaxY = 0.0f;

		if (diff.x < 0)
		{
			step.x = -1;
		}
		else if (diff.x > 0)
		{
			step.x = 1;
		}
		else
		{
			step.x = 0;
			// need to set t X to insane as basically it will go near infinite
			tMaxX = std::numeric_limits<float>::max();
		}

		if (diff.y < 0)
		{
			step.y = -1;
		}
		else if (diff.y > 0)
		{
			step.y = 1;
		}
		else
		{
			step.y = 0;
			// need to set t Y to insane as basically it will go near infinite
			tMaxY = std::numeric_limits<float>::max();
		}

		glm::ivec2 current = start;
		points.push_back(current);
		while (current != end)
		{
			if (tMaxX < tMaxY)
			{
				tMaxX += tDeltaX;
				current.x += step.x;
				assert(current.x != (end.x + step.x) && "Over the target.");
			}
			else
			{
				tMaxY += tDeltaY;
				current.y += step.y;
				assert(current.y != (end.y + step.y) && "Over the target.");
			}
			points.push_back(current);
		}
	}

	size_t getClosestPointIndex(glm::vec2 start, const std::vector<glm::vec2>& points)
	{
		if (points.empty())
		{
			assert(false);
			return 0;
		}

		float distance = glm::distance(start, points[0]);
		size_t index = 0;
		for (size_t i = 1; i < points.size(); ++i)
		{
			float tmp = glm::distance(start, points[i]);
			if (tmp < distance)
			{
				distance = tmp;
				index = i;
			}
		}
		return index;
	}

	void getPathPoints(const Path& path, std::vector<glm::ivec2>& points)
	{
		if (path.empty())
		{
			return;
		}
		if (path.size() == 1)
		{
			points.push_back(path.front());
			return;
		}
		for (size_t i = 1; i < path.size(); ++i)
		{
			getLinePoints(path[i - 1], path[i], points);
		}
	}

	glm::vec2 getCircleMidPoint(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c)
	{
		glm::vec2 ab{ a.x - b.x, a.y - b.y };
		glm::vec2 ac{ a.x - c.x, a.y - c.y };

		glm::vec2 sqac{ std::pow(a.x, 2) - std::pow(c.x, 2), std::pow(a.y, 2) - std::pow(c.y, 2) };
		glm::vec2 sqba{ std::pow(b.x, 2) - std::pow(a.x, 2), std::pow(b.y, 2) - std::pow(a.y, 2) };

		return {
			-((sqac.x * ab.y + sqac.y * ab.y + sqba.x * ac.y + sqba.y * ac.y) / (2 * ((c.x - a.x) * ab.y - (b.x - a.x) * ac.y))),
			-((sqac.x * ab.x + sqac.y * ab.x + sqba.x * ac.x + sqba.y * ac.x) / (2 * ((c.y - a.y) * ab.x - (b.y - a.y) * ac.x)))
		};
	}
} // ns toy
