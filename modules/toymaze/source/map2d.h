#ifndef TOY_MAP2D_H_
#define TOY_MAP2D_H_

#include <vector>
#include <assert.h>
#include <glm/glm.hpp>

namespace toy
{
template <class CType>
class Map2D
{
public:
	Map2D()
	{
	}

	Map2D(glm::ivec2 size)
	{
		resize(size);
	}

	void resize(const glm::ivec2& size)
	{
		m_size = size;
		m_data.resize(m_size.x * m_size.y);
	}

	void resize(const glm::ivec2& size, const CType& value)
	{
		m_size = size;
		m_data.resize(m_size.x * m_size.y, value);
	}

	CType& operator [](const glm::ivec2& position);
	const CType& operator [](const glm::ivec2& position) const;

	const glm::ivec2& getSize() const
	{
		return m_size;
	}

	void clear(const CType& value)
	{
		std::fill(m_data.begin(), m_data.end(), value);
	}

	void set(const glm::ivec2& position, const CType& value)
	{
		assert(position.x < m_size.x&& position.x >= 0 && position.y < m_size.y&& position.y >= 0 && "position out of bounds");
		m_data[position.y * m_size.x + position.x] = value;
	}

	CType get(const glm::ivec2& position) const
	{
		assert(position.x < m_size.x&& position.x >= 0 && position.y < m_size.y&& position.y >= 0 && "position out of bounds");
		return m_data[position.y * m_size.x + position.x];
	}

	void set(const std::vector<glm::ivec2>& positions, const CType& value)
	{
		for (const glm::ivec2& position : positions)
		{
			if (isValid(position))
			{
				operator [](position) = value;
			}
		}
	}

	bool isValid(const glm::ivec2& point) const
	{
		return point.x >= 0 && point.x < getSize().x&& point.y >= 0 && point.y < getSize().y;
	}
private:
	glm::ivec2 m_size;
	std::vector<CType> m_data;
};

template <class CType>
CType& Map2D<CType>::operator [](const glm::ivec2& position)
{
	assert(position.x < m_size.x&& position.x >= 0 && position.y < m_size.y&& position.y >= 0 && "position out of bounds");
	return m_data[position.y * m_size.x + position.x];
}

template <class CType>
const CType& Map2D<CType>::operator [](const glm::ivec2& position) const
{
	assert(position.x < m_size.x&& position.x >= 0 && position.y < m_size.y&& position.y >= 0 && "position out of bounds");
	return m_data[position.y * m_size.x + position.x];
}
} // ns toy

#endif // TOY_MAP2D_H_