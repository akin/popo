#include "exporter.h"

#include <functional>
#include "tile.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#undef STB_IMAGE_IMPLEMENTATION
#undef STB_IMAGE_WRITE_IMPLEMENTATION

#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#include "tiny_gltf.h"

namespace toy
{
	bool saveMazeToImage(Maze& maze, const std::string& path)
	{
		const auto size = maze.getSize();
		std::vector<glm::u8vec4> data;
		data.resize(size.y * size.x);

		size_t index = 0;
		for (size_t y = 0; y < size.y; ++y)
		{
			for (size_t x = 0; x < size.x; ++x)
			{
				auto color = maze.getTile({ x,y }).getColor();
				data[y * size.x + x] = {
					(uint8_t)(color.x * 0xFF),
					(uint8_t)(color.y * 0xFF),
					(uint8_t)(color.z * 0xFF),
					(uint8_t)(color.w * 0xFF)
				};
			}
		}

		size_t bpp = 4;
		int ret = stbi_write_png(path.c_str(), size.x, size.y, 4, data.data(), bpp * size.x);

		return ret == 1;
	}

	Mesh createUnitCube()
	{
		std::vector<glm::vec3> positions;
		std::vector<uint32_t> indices;

		// Front
		positions.push_back({ -1.0f, 1.0f, -1.0f }); // 0 
		positions.push_back({ 1.0f, 1.0f, -1.0f });  // 1
		positions.push_back({ 1.0f,-1.0f, -1.0f });  // 2
		positions.push_back({ -1.0f,-1.0f, -1.0f }); // 3

		// Back
		positions.push_back({ -1.0f, 1.0f,  1.0f }); // 4
		positions.push_back({ 1.0f, 1.0f,  1.0f });  // 5
		positions.push_back({ 1.0f,-1.0f,  1.0f });  // 6
		positions.push_back({ -1.0f,-1.0f,  1.0f }); // 7

		auto addIndices = [&indices](const std::vector<uint32_t>& addIndices) {
			std::transform(addIndices.cbegin(), addIndices.cend(), std::back_inserter(indices), [](const uint32_t value) -> uint32_t {
				return value;
				});
		};

		addIndices({ 0,1,3, 1,2,3 }); // front
		addIndices({ 5,4,7, 7,6,5 }); // back

		addIndices({ 0,1,5, 5,4,1 }); // top
		addIndices({ 3,7,6, 6,2,3 }); // bottom

		addIndices({ 0,3,7, 7,4,0 }); // left
		addIndices({ 2,1,5, 5,6,2 }); // righ

		Mesh mesh;
		mesh.setTopology(Topology::triangles);
		mesh.setAttribute(Attribute::position, positions);
		mesh.setIndices(indices);
		return std::move(mesh);
	}

	bool saveMesh(const Mesh& mesh, const std::string& path)
	{
		using namespace tinygltf;

		Model model;
		TinyGLTF loader;


		{
			/*
			std::string name;
			std::vector<unsigned char> data;
			std::string
				uri;  // considered as required here but not in the spec (need to clarify)
					  // uri is not decoded(e.g. whitespace may be represented as %20)
			Value extras;
			ExtensionMap extensions;

			// Filled when SetStoreOriginalJSONForExtrasAndExtensions is enabled.
			std::string extras_json_string;
			std::string extensions_json_string;

			Buffer() = default;
			DEFAULT_METHODS(Buffer)
				bool operator==(const Buffer&) const;
			*/
			Buffer buffer;
			buffer.name = "Vertexes";
			model.buffers.push_back(std::move(buffer));
		}

		return loader.WriteGltfSceneToFile(&model, path, true, true);
	}
} // ns toy
