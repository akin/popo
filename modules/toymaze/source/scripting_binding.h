#ifndef TOY_SCRIPTING_BINDING_H_
#define TOY_SCRIPTING_BINDING_H_

#include "scripting_state.h"

namespace scripting
{
	void setup(State& state);
} // scripting

#endif // TOY_SCRIPTING_BINDING_H_