#ifndef TOY_EXPORTER_H_
#define TOY_EXPORTER_H_

#include "maze.h"
#include "mesh.h"
#include <string>

namespace toy
{
	bool saveMazeToImage(Maze& maze, const std::string& path);
	Mesh createUnitCube();
	bool saveMesh(const Mesh& mesh, const std::string& path);
} // ns toy

#endif // TOY_EXPORTER_H_