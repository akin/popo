#ifndef TOY_COMMON_H_
#define TOY_COMMON_H_

#include <cstddef>
#include <vector>
#include <glm/glm.hpp>
#include <utility>

namespace toy
{
	using Path = std::vector<glm::vec2>;
	using Edge = std::pair<size_t, size_t>;

	enum class Topology : uint32_t
	{
		none = 0,
		points = 1,
		lines = 2,
		linesStrip = 3,
		triangles = 4,
		triangleStrip = 5,
		triangleFan = 6
	};

	enum class Attribute : uint32_t
	{
		position = 0,
		texturecoordinate = 1,
		normal = 2,
		tangent = 3,
		bone = 4,
	};

	void getLinePoints(glm::ivec2 start, glm::ivec2 end, std::vector<glm::ivec2>& points);

	size_t getClosestPointIndex(glm::vec2 start, const std::vector<glm::vec2>& points);

	void getPathPoints(const Path& path, std::vector<glm::ivec2>& points);

	glm::vec2 getCircleMidPoint(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c);
} // ns toy

#endif // TOY_COMMON_H_