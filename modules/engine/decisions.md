### Engine
Engine is the "dirty" implementation of the engine, tying together different modules and providing generic "meat".

## Decisiongs
 * Not everything needs to know about ECS system
   * ECS system handles frame variance, in goes delta time, ECS generates fixed timestep simulation.
   * Input system does not need to be in ecs, but I think coupling it with ECS might give interesting options for higher quality input on the system (inputs for fixed time steps, maybe animation like interpolations. Buttons could be other than "step").
   * the smallest unit is 1 frame, if the action also stops durning that frame, that actions stays active full frame
