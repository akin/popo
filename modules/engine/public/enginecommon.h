#ifndef XP_COMMON_H_
#define XP_COMMON_H_

#include <graphics/common.h>

namespace engine
{
	struct ConstantBuffer
	{
		glm::mat4 MVP;
	};

	constexpr glm::vec3 UP{ 0.0f, 1.0f, 0.0f };
	constexpr glm::vec3 RIGHT{ 1.0f, 0.0f, 0.0f };
	constexpr glm::vec3 FORWARD{ 0.0f, 0.0f, -1.0f };
} // xp
#endif // XP_COMMON_H_
