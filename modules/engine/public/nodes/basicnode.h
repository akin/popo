#ifndef ENGINE_BASIC_NODE_H_
#define ENGINE_BASIC_NODE_H_

#include <memory>
#include "effectnode.h"
#include <graphics/renderobject.h>
#include <enginecommon.h>

#include <DeviceContext.h>
#include <RefCntAutoPtr.hpp>

#include <MapHelper.hpp>

namespace engine
{
	class BasicNode final : public EffectNode
	{
	public:
		BasicNode(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device, Diligent::RefCntAutoPtr<Diligent::IDeviceContext> context)
			: EffectNode(device, context)
		{
		}

		~BasicNode() final
		{
			m_context->Flush();
		}

		bool initialize() final
		{
			if (!EffectNode::initialize())
			{
				return false;
			}

			Diligent::BufferDesc desc;
			desc.Name = "VS constants CB";
			desc.uiSizeInBytes = sizeof(engine::ConstantBuffer);
			desc.Usage = Diligent::USAGE_DYNAMIC;
			desc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
			desc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;
			m_device->CreateBuffer(desc, nullptr, &m_constantBuffer);

			m_pipelinestate->GetStaticVariableByName(Diligent::SHADER_TYPE_VERTEX, "Constants")->Set(m_constantBuffer);

			return true;
		}

		void attach(graphics::RenderObject& object) final
		{
			// TODO! this is hacky way just to get HELLO WORLD on screen
			// In reality there should be a map or something that connects the objects and ShaderResourceBindings
			// these are per object UNIFORMS etc.
			m_pipelinestate->CreateShaderResourceBinding(&object.resourceBinding, true);
		}

		void render(graphics::RenderData& renderData) final
		{
			assert(m_effect);
			assert(m_output);
			m_output->bindAsOuput();

			// Set the pipeline state
			m_context->SetPipelineState(m_pipelinestate);

			for (size_t viewIndex = 0; viewIndex < renderData.views.size(); ++viewIndex)
			{
				const graphics::RenderView& view = renderData.views[viewIndex];
				graphics::RenderSet& set = renderData.sets[viewIndex];

				for (graphics::RenderObject& object : set)
				{
					{
						glm::mat4 mvp = view.projection * view.view * object.matrix;

						// Map the buffer and write current world-view-projection matrix
						Diligent::MapHelper<engine::ConstantBuffer> constants(m_context, m_constantBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
						constants->MVP = glm::transpose(mvp);
					}

					graphics::AttributeInfo position = object.mesh->getAttribute(graphics::AttributeType::Position);
					graphics::IndicesInfo indexes = object.mesh->getIndicesInfo();

					uint32_t offset = position.offset;
					Diligent::IBuffer* pBuffs[] = { position.buffer.RawPtr() };
					m_context->SetVertexBuffers(0, 1, pBuffs, &offset, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION, Diligent::SET_VERTEX_BUFFERS_FLAG_RESET);

					m_context->SetIndexBuffer(indexes.buffer, 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

					// Commit shader resources. RESOURCE_STATE_TRANSITION_MODE_TRANSITION mode
					// makes sure that resources are transitioned to required states.
					m_context->CommitShaderResources(object.resourceBinding, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

					Diligent::DrawIndexedAttribs attribs;
					attribs.IndexType = indexes.type;
					attribs.NumIndices = indexes.count;

					attribs.Flags = Diligent::DRAW_FLAG_VERIFY_ALL;

					m_context->DrawIndexed(attribs);
				}
			}
		}
	private:
		Diligent::RefCntAutoPtr<Diligent::IBuffer> m_constantBuffer;
	};
} // engine
#endif // ENGINE_BASIC_NODE_H_
