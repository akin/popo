#ifndef ENGINE_EFFECT_NODE_H_
#define ENGINE_EFFECT_NODE_H_

#include <enginecommon.h>
#include <graphics/node.h>
#include <graphics/effect.h>

#include <RenderDevice.h>
#include <DeviceContext.h>
#include <SwapChain.h>
#include <RefCntAutoPtr.hpp>

namespace engine
{
	class EffectNode : public graphics::Node
	{
	public:
		EffectNode(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device, Diligent::RefCntAutoPtr<Diligent::IDeviceContext> context)
			: graphics::Node("Effect")
			, m_device(device)
			, m_context(context)
		{
		}

		virtual ~EffectNode() = default;

		void setEffect(std::shared_ptr<graphics::Effect> effect)
		{
			m_effect = effect;
		}

		bool initialize() override
		{
			// From effect get Shaders createInfo
			Diligent::GraphicsPipelineStateCreateInfo createInfo = m_effect->getCreateInfo();

			// From output get output format
			auto format = m_output->getInputConnectionFormat();

			// TODO! Better render target configuration!
			createInfo.GraphicsPipeline.NumRenderTargets = 1;
			createInfo.GraphicsPipeline.RTVFormats[0] = format.color;
			createInfo.GraphicsPipeline.DSVFormat = format.depth;

			// What kind of primitives
			// Primitive topology defines what kind of primitives will be rendered by this pipeline state
			// Disable depth testing
			// No back face culling for this tutorial
			createInfo.GraphicsPipeline.PrimitiveTopology = Diligent::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			createInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_NONE;
			createInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = false;

			// create pipeline state
			m_device->CreateGraphicsPipelineState(createInfo, &m_pipelinestate);

			return true;
		}

	protected:
		Diligent::RefCntAutoPtr<Diligent::IRenderDevice> m_device;
		Diligent::RefCntAutoPtr<Diligent::IDeviceContext> m_context;

		std::shared_ptr<graphics::Effect> m_effect;
		Diligent::RefCntAutoPtr<Diligent::IPipelineState> m_pipelinestate;
	};
} // engine
#endif // ENGINE_EFFECT_NODE_H_
