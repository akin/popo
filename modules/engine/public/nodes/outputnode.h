#ifndef ENGINE_OUTPUT_NODE_H_
#define ENGINE_OUTPUT_NODE_H_

#include <enginecommon.h>
#include <graphics/node.h>

#include <RenderDevice.h>
#include <DeviceContext.h>
#include <SwapChain.h>
#include <RefCntAutoPtr.hpp>

namespace engine
{
	class OutputNode final : public graphics::Node
	{
	public:
		OutputNode(Diligent::RefCntAutoPtr<Diligent::IDeviceContext> context, Diligent::RefCntAutoPtr<Diligent::ISwapChain> swapchain)
			: graphics::Node("Output")
			, m_context(context)
			, m_swapchain(swapchain)
		{
		}

		~OutputNode() final
		{
		}

		graphics::ConnectionFormat getInputConnectionFormat() final
		{
			graphics::ConnectionFormat format;
			auto swapChainFormat = m_swapchain->GetDesc();
			format.color = swapChainFormat.ColorBufferFormat;
			format.depth = swapChainFormat.DepthBufferFormat;
			format.resolution.x = swapChainFormat.Width;
			format.resolution.y = swapChainFormat.Height;
			return format;
		}

		void resize(const glm::uvec2& size) final
		{
			if (m_swapchain)
			{
				m_swapchain->Resize(size.x, size.y);
			}
		}

		void render(graphics::RenderData& renderData) final
		{
			m_swapchain->Present();
		}

		void bindAsOuput() final
		{
			// Set render targets before issuing any draw command.
			// Note that Present() unbinds the back buffer if it is set as render target.
			auto* pRTV = m_swapchain->GetCurrentBackBufferRTV();
			auto* pDSV = m_swapchain->GetDepthBufferDSV();
			m_context->SetRenderTargets(1, &pRTV, pDSV, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

			// Clear the back buffer
			const float ClearColor[] = { 0.350f, 0.350f, 0.350f, 1.0f };
			// Let the engine perform required state transitions
			m_context->ClearRenderTarget(pRTV, ClearColor, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
			m_context->ClearDepthStencil(pDSV, Diligent::CLEAR_DEPTH_FLAG, 1.f, 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
		}
	private:
		Diligent::RefCntAutoPtr<Diligent::IDeviceContext> m_context;
		Diligent::RefCntAutoPtr<Diligent::ISwapChain> m_swapchain;
	};
} // engine
#endif // ENGINE_OUTPUT_NODE_H_
