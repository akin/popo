#ifndef ENGINE_CULLING_H_INCLUDED_POPO
#define ENGINE_CULLING_H_INCLUDED_POPO

#include <enginecommon.h>
#include <glm/glm.hpp>

namespace engine
{
	class Culling
	{
	public:
		glm::vec3 offset{0.0f};
        float radius{0.0f};
	};
} // ns engine

#endif // ENGINE_CULLING_H_INCLUDED_POPO