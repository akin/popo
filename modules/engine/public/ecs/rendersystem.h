#ifndef ENGINE_RENDERSYSTEM_H_
#define ENGINE_RENDERSYSTEM_H_

#include <ecs/common.h>
#include <ecs/system.h>
#include <unordered_map>
#include <graphics/renderobject.h>
#include <memory>

namespace engine
{
	class RenderSystem final : public ecs::System
	{
	public:
		RenderSystem(ecs::Context& context);

		~RenderSystem() final;

		graphics::RenderObject& getOrCreate(ecs::EntityID id);

		const std::vector<graphics::RenderObject>& getRenderObjects();

		void remove(ecs::EntityID id) final;

		void update(const ecs::Timestamp& time) final;
	private:
		std::vector<graphics::RenderObject> m_data;
		std::vector<ecs::EntityID> m_id;
		std::unordered_map<ecs::EntityID, size_t> m_index;
	};
} // ns engine

#endif // ENGINE_RENDERSYSTEM_H_