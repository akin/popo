#ifndef ENGINE_TRANSFORMSYSTEM_H_INCLUDED_POPO
#define ENGINE_TRANSFORMSYSTEM_H_INCLUDED_POPO

#include <ecs/common.h>
#include <ecs/system.h>
#include <ecs/transform.h>
#include <unordered_map>

namespace engine
{
	class TransformSystem final : public ecs::System
	{
	public:
		TransformSystem(ecs::Context& context);

		~TransformSystem() final;

		Transform& getOrCreate(ecs::EntityID id);

		const Transform* get(ecs::EntityID id) const;

		void remove(ecs::EntityID id) final;

		void update(const ecs::Timestamp& time) final;
	private:
		std::unordered_map<ecs::EntityID, Transform> m_data;
	};
} // ns engine

#endif // ENGINE_TRANSFORMSYSTEM_H_INCLUDED_POPO