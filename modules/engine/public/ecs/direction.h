#ifndef ENGINE_DIRECTION_H_INCLUDED_POPO
#define ENGINE_DIRECTION_H_INCLUDED_POPO

#include <enginecommon.h>
#include <glm/glm.hpp>

namespace engine
{
	class Direction
	{
	public:
		Direction() = default;

		Direction(const glm::mat4& world)
		{
			glm::mat3 mat(world);

			up = mat * UP;
			right = mat * RIGHT;
			forward = mat * FORWARD;
		}

		glm::vec3 up;
		glm::vec3 right;
		glm::vec3 forward;
	};
} // ns engine

#endif // ENGINE_DIRECTION_H_INCLUDED_POPO