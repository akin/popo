#ifndef ECS_INPUTSYSTEM_H_
#define ECS_INPUTSYSTEM_H_

#include <ecs/common.h>
#include <ecs/system.h>
#include <input/semantic.h>
#include <input/action/key.h>
#include <input/action/mouse.h>
#include <input/listener.h>
#include <ecs/inputstate.h>
#include <unordered_map>
#include <memory>

namespace engine
{
	using InputAction = std::function<void(const InputState&)>;

	class InputSystem final :
		public ecs::System,
		public input::Listener<input::KeyEvent>,
		public input::Listener<input::MouseMoveEvent>
	{
	public:
		InputSystem(ecs::Context& context);

		InputSystem(const InputSystem&) = delete;

		~InputSystem() final;

		input::SemanticMap& getSemantics();

		void remove(ecs::EntityID id) final;

		void update(const ecs::Timestamp& time) final;

		void handle(const input::KeyEvent& event) final;
		void handle(const input::MouseMoveEvent& event) final;

		void addInputAction(ecs::EntityID id, std::string inputaction, InputAction func);
	private:
		std::unordered_map<ecs::EntityID, std::unordered_map<std::string, InputAction>> m_inputActions;

		InputState m_currentState;

		input::SemanticMap m_semantics;
	};
} // ns engine

#endif // ECS_INPUTSYSTEM_H_