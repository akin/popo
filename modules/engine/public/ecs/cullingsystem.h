#ifndef ENGINE_CULLINGSYSTEM_H_INCLUDED_POPO
#define ENGINE_CULLINGSYSTEM_H_INCLUDED_POPO

#include <ecs/common.h>
#include <ecs/system.h>
#include <ecs/culling.h>
#include <unordered_map>

namespace engine
{
	class CullingSystem final : public ecs::System
	{
	public:
		CullingSystem(ecs::Context& context);

		~CullingSystem() final;

		Culling& getOrCreate(ecs::EntityID id);

		void remove(ecs::EntityID id) final;

		void update(const ecs::Timestamp& time) final;

		ecs::EntityID rayQuery(glm::vec3 origin, glm::vec3 direction) const;
	private:
		std::unordered_map<ecs::EntityID, Culling> m_data;
	};
} // ns engine

#endif // ENGINE_CULLINGSYSTEM_H_INCLUDED_POPO