#ifndef ENGINE_CAMERASYSTEM_H_INCLUDED
#define ENGINE_CAMERASYSTEM_H_INCLUDED

#include <ecs/common.h>
#include <ecs/system.h>
#include <graphics/renderview.h>
#include <graphics/renderobject.h>
#include <graphics/renderdata.h>
#include <unordered_map>

namespace engine
{
	class CameraSystem final : public ecs::System
	{
	public:
		CameraSystem(ecs::Context& context);

		~CameraSystem() final;

		graphics::RenderView& getOrCreate(ecs::EntityID id);

		void remove(ecs::EntityID id) final;

		void update(const ecs::Timestamp& time) final;

		graphics::RenderData getRenderData();
	private:
		std::vector<graphics::RenderView> m_view;
		std::vector<graphics::RenderSet> m_set;

		std::vector<ecs::EntityID> m_id;
		std::unordered_map<ecs::EntityID, size_t> m_index;

		graphics::RenderData m_renderData;
	};
} // ns engine

#endif // ENGINE_CAMERASYSTEM_H_INCLUDED