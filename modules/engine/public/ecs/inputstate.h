#ifndef ECS_INPUTSTATE_H_
#define ECS_INPUTSTATE_H_

#include <functional>
#include <memory>
#include <input/semantic.h>
#include <input/action/key.h>
#include <input/action/mouse.h>

namespace engine
{
	class InputState
	{
	public:
		InputState(const input::SemanticMap& map);

		void update();

		void setKey(input::SemanticID semantic, input::KeyState state);
		input::KeyState getKey(input::SemanticID semantic) const;
		inline input::KeyState getKey(input::Semantic semantic) const
		{
			return getKey(static_cast<input::SemanticID>(semantic));
		}
		
		void setMousePosition(input::MouseState state);
		input::MouseState getMousePosition() const;
	private:
		const input::SemanticMap& m_map;
		std::vector<input::KeyState> m_keys;

		input::MouseState m_mouse;
	};
} // ns engine

#endif // ECS_INPUTSTATE_H_