#ifndef ENGINE_TRANSFORM_H_INCLUDED_POPO
#define ENGINE_TRANSFORM_H_INCLUDED_POPO

#include <enginecommon.h>
#include <ecs/direction.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace engine
{
	class Transform
	{
	public:
		glm::vec3 position;
		glm::vec3 scale{ 1.0f };
		glm::quat rotation;
		glm::mat4 local;
		glm::mat4 world;

		Direction getDirection()
		{
			return Direction{ world };
		}

		void update()
		{
			static const glm::mat4 identity = glm::mat4(1.0f);

			glm::mat4 local = glm::translate(identity, position);
			local *= glm::toMat4(rotation);
			local *= glm::scale(identity, scale);

			//world = parent.world * local;
			world = local;
		}

		void move(const glm::vec3& amount)
		{
			position += amount;
		}

		void lookAt(const glm::vec3& point)
		{
			glm::vec3 direction = glm::normalize(point - position);
			rotation = glm::quatLookAt(direction, engine::UP);
		}

		void lookAtDirection(const glm::vec3& direction)
		{
			rotation = glm::quatLookAt(direction, engine::UP);
		}

		glm::vec3 getWorldPosition(const glm::vec3 offset = glm::vec3{ 0.0f }) const
		{
			glm::vec4 pos{ offset, 1.0f };
			pos = world * pos;

			return { pos.x, pos.y, pos.z };
		}
	};
} // ns engine

#endif // ENGINE_TRANSFORM_H_INCLUDED_POPO