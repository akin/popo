#ifndef ENGINE_WIRE_EFFECT_H_
#define ENGINE_WIRE_EFFECT_H_

#include <graphics/effect.h>

namespace engine
{
	class WireEffect final : public graphics::Effect
	{
	public:
		WireEffect()
			: graphics::Effect(Diligent::PIPELINE_TYPE::PIPELINE_TYPE_GRAPHICS, "Wireframe")
		{
		}

		~WireEffect() final
		{
		}

		void initialize(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device) final
		{
			Diligent::ShaderCreateInfo info;
			// Tell the system that the shader source code is in HLSL.
			// For OpenGL, the engine will convert this into GLSL under the hood
			info.SourceLanguage = Diligent::SHADER_SOURCE_LANGUAGE_HLSL;
			// OpenGL backend requires emulated combined HLSL texture samplers (g_Texture + g_Texture_sampler combination)
			info.UseCombinedTextureSamplers = true;

			// Create a vertex shader
			{
				info.Desc.ShaderType = Diligent::SHADER_TYPE_VERTEX;
				info.EntryPoint = "main";
				info.Desc.Name = "Triangle vertex shader";
				info.Source = R"(
struct VertexIn
{
	float3 position : ATTRIB0;
};

struct VertexOut
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

cbuffer Constants
{
    float4x4 MVP;
};

void main(in VertexIn vertexIn, out VertexOut vertexOut) 
{
	// transform the position into homogeneous coordinates (projective geometry)
	float4 outputPos = { vertexIn.position.x, vertexIn.position.y, vertexIn.position.z, 1.0f };
	vertexOut.position = mul(outputPos, MVP);

	// set the color (set full alpha)
	float4 outputCol = { 0.0f, 0.5f, 0.75f, 1.0f };
	vertexOut.color = outputCol;
}
)";
				device->CreateShader(info, &m_vertexShader);
			}

			// Create a pixel shader
			{
				info.Desc.ShaderType = Diligent::SHADER_TYPE_PIXEL;
				info.EntryPoint = "main";
				info.Desc.Name = "Triangle pixel shader";
				info.Source = R"(
struct FragmentIn
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

struct FragmentOut
{ 
	float4 color : SV_TARGET; 
};

void main(in FragmentIn fragmentIn, out FragmentOut fragmentOut)
{
	fragmentOut.color = fragmentIn.color;
}
)";
				device->CreateShader(info, &m_fragmentShader);
			}

			// Attribute 0 - vertex position
			addAttribute(Diligent::LayoutElement{ 0, 0, 3, Diligent::VT_FLOAT32, false });

			// Constant buffer
			addVariable({ Diligent::SHADER_TYPE_VERTEX, "Constants", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC });
		}


		Diligent::GraphicsPipelineStateCreateInfo getCreateInfo() final
		{
			Diligent::GraphicsPipelineStateCreateInfo info = Effect::getCreateInfo();
			info.pVS = m_vertexShader.RawPtr();
			info.pPS = m_fragmentShader.RawPtr();
			return info;
		}
	private:
		Diligent::RefCntAutoPtr<Diligent::IShader> m_vertexShader;
		Diligent::RefCntAutoPtr<Diligent::IShader> m_fragmentShader;
	};

} // engine
#endif // ENGINE_WIRE_EFFECT_H_
