#ifndef ENGINE_CONFIG_LOADER_H_
#define ENGINE_CONFIG_LOADER_H_

#include <input/semantic.h>
#include <input/keymap.h>

namespace engine
{
	bool loadConfig(const std::string& path, input::KeyMap& keymap, input::SemanticMap& semantics);
} // ns engine

#endif // ENGINE_CONFIG_LOADER_H_