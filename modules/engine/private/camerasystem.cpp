#include <ecs/camerasystem.h>
#include <ecs/rendersystem.h>
#include <ecs/transformsystem.h>
#include <ecs/typeid.h>
#include <ecs/context.h>

#include <vecutils.h>

#include <spdlog/spdlog.h>

#include <assert.h>

namespace engine
{
	CameraSystem::CameraSystem(ecs::Context& context)
		: System(context, "camera", type_id<CameraSystem>)
	{
	}

	CameraSystem::~CameraSystem()
	{
	}

	graphics::RenderView& CameraSystem::getOrCreate(ecs::EntityID id)
	{
		auto iter = m_index.find(id);
		if (iter != m_index.end())
		{
			return m_view[iter->second];
		}
		m_index[id] = m_id.size();
		m_id.push_back(id);
		m_view.push_back({});
		m_set.push_back({});
		return m_view.back();
	}

	void CameraSystem::remove(ecs::EntityID id)
	{		// Not called while "update running"
		auto iter = m_index.find(id);
		if (iter != m_index.end())
		{
			if (m_id.size() <= 1)
			{
				m_view.clear();
				m_set.clear();
				m_id.clear();
				m_index.clear();
				return;
			}

			size_t index = iter->second;
			swapBack(m_view, index);
			swapBack(m_set, index);
			swapBack(m_id, index);

			// Correct index records:
			m_index.erase(iter);
			m_index[m_id[index]] = index;
		}
	}

	void CameraSystem::update(const ecs::Timestamp& time)
	{
		// Update camera matrixes etc.
		// Gather lists of renderables..
		auto* tsystem = m_context.getSystem<TransformSystem>();
		assert(tsystem != nullptr);

		auto* rsystem = m_context.getSystem<RenderSystem>();

		m_renderData.sets.resize(m_id.size());
		m_renderData.views.resize(m_id.size());
		for (size_t i = 0; i < m_id.size(); ++i)
		{
			ecs::EntityID& id = m_id[i];
			graphics::RenderView& view = m_view[i];
			graphics::RenderSet& set = m_set[i];

			// TODO, bounding checks!
			set = rsystem->getRenderObjects();

			// Update camera position
			// Camera exists in the "scene" but in reality we move the whole world
			// according to camera, so its world position needs to be inversed.
			Transform& transform = tsystem->getOrCreate(id);
			view.view = glm::inverse(transform.world);

			// update RenderData
			m_renderData.sets[i] = set;
			m_renderData.views[i] = view;
		}
	}

	graphics::RenderData CameraSystem::getRenderData()
	{
		return m_renderData;
	}
} // ns engine
