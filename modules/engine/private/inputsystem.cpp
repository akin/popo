#include <ecs/inputsystem.h>
#include <ecs/typeid.h>
#include <ecs/context.h>

#include <vecutils.h>
#include <spdlog/spdlog.h>
#include <assert.h>
#include <iostream>

namespace engine
{
	InputSystem::InputSystem(ecs::Context& context)
		: System(context, "input", type_id<InputSystem>)
		, m_currentState(m_semantics)
	{
		input::loadSemanticDefaults(m_semantics);
		m_currentState.update();
	}

	InputSystem::~InputSystem()
	{
	}

	input::SemanticMap& InputSystem::getSemantics()
	{
		return m_semantics;
	}

	void InputSystem::remove(ecs::EntityID id)
	{
		// Not called while "update running"
		auto iter = m_inputActions.find(id);
		if (iter != m_inputActions.end())
		{
			m_inputActions.erase(iter);
		}
	}

	void InputSystem::update(const ecs::Timestamp& time)
	{
		// TODO! inputs should happen in fixedTimeSteps, so an action can start and stop inside a frame,
		// the smallest unit is 1 frame, if the action also stops durning that frame, that actions stays active
		// full frame. for first implementation YOLO.
		for (auto& entityIter : m_inputActions)
		{
			for (auto& funcIter : entityIter.second)
			{
				funcIter.second(m_currentState);
			}
		}
		m_currentState.update();
	}

	void InputSystem::handle(const input::KeyEvent& event)
	{
		m_currentState.setKey(event.id, event.state);
	}

	void InputSystem::handle(const input::MouseMoveEvent& event)
	{
		m_currentState.setMousePosition(event);
	}

	void InputSystem::addInputAction(ecs::EntityID id, std::string inputaction, InputAction func)
	{
		auto& actions = m_inputActions[id];
		actions[inputaction] = func;
	}
} // ns engine
