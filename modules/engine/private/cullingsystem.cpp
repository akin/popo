#include <ecs/cullingsystem.h>
#include <ecs/transformsystem.h>
#include <ecs/typeid.h>
#include <ecs/context.h>

#include <glm/gtx/intersect.hpp>

#include <spdlog/spdlog.h>

#include <assert.h>

namespace engine
{
	CullingSystem::CullingSystem(ecs::Context& context)
		: System(context, "culling", type_id<CullingSystem>)
	{
	}

	CullingSystem::~CullingSystem()
	{
	}

	Culling& CullingSystem::getOrCreate(ecs::EntityID id)
	{
		return m_data[id];
	}

	void CullingSystem::remove(ecs::EntityID id)
	{
		// Not called while "update running"
		auto iter = m_data.find(id);
		if (iter != m_data.end())
		{
			m_data.erase(iter);
		}
	}

	void CullingSystem::update(const ecs::Timestamp& time)
	{
	}

	ecs::EntityID CullingSystem::rayQuery(glm::vec3 origin, glm::vec3 direction) const
	{
		auto* tsystem = m_context.getSystem<TransformSystem>();

		ecs::EntityID entity = ecs::invalidEntity;
		float closestHit = std::numeric_limits<float>::max();
		for (const auto& item : m_data)
		{
			const Transform* transform = tsystem->get(item.first);
			if (transform == nullptr)
			{
				continue;
			}

			glm::vec3 position = transform->getWorldPosition(item.second.offset);

			glm::vec3 hitPosition{ 0.0f };
			glm::vec3 hitNormal{ 0.0f };

			if (glm::intersectRaySphere(
				origin,
				direction,
				position,
				item.second.radius,
				hitPosition,
				hitNormal
			))
			{
				float distance = glm::distance(origin, hitPosition);
				if (distance < closestHit)
				{
					closestHit = distance;
					entity = item.first;
				}
			}
		}
		return entity;
	}
} // ns engine
