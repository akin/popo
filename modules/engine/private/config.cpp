#include <loader/config.h>

#include <vecutils.h>
#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>

#include <fstream>
#include <assert.h>
#include <iostream>

// for convenience
using json = nlohmann::json;

namespace
{
    void parseSemantics(const nlohmann::json& js, input::SemanticMap& semantics)
    {
        if(!js.is_array())
        {
            return;
        }
        for(const auto& item : js)
        {
            if(!item.is_string())
            {
                continue;
            }
            semantics.getOrCreate(item);
        }
    }
    void parseKeyMapping(const nlohmann::json& js, input::KeyMap& keymap, input::SemanticMap& semantics)
    {
        if(!js.is_object())
        {
            return;
        }
        for(auto& kmoIter : js.items())
        {
            if(kmoIter.key() != "default")
            {
                continue;
            }

            if(!kmoIter.value().is_object())
            {
                continue;
            }

            for(auto& kv : kmoIter.value().items())
            {
                if(kv.key().empty() || (!kv.value().is_string()))
                {
                    continue;
                }
                std::string value = kv.value();
                if(value.empty())
                {
                    continue;
                }

                input::SemanticID id = semantics.getOrCreate(value);
                keymap[kv.key()] = id;
            }
        }
    }
}

namespace engine
{
	bool loadConfig(const std::string& path, input::KeyMap& keymap, input::SemanticMap& semantics)
	{
        nlohmann::json js;
        {
            std::ifstream input(path);
            if(!input.is_open() || input.bad()) 
            {
                return false;
            }

            input >> js;
        }

        {
            auto iter = js.find("key semantics");
            if(iter != js.end())
            {
                parseSemantics(*iter, semantics);
            }
        }

        {
            auto iter = js.find("key mapping");
            if(iter != js.end())
            {
                parseKeyMapping(*iter, keymap, semantics);
            }
        }

		return true;
	}
} // ns engine
