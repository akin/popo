#include <ecs/rendersystem.h>
#include <ecs/typeid.h>
#include <ecs/context.h>
#include <ecs/transformsystem.h>

#include <spdlog/spdlog.h>
#include <vecutils.h>

#include <assert.h>

namespace engine
{
	RenderSystem::RenderSystem(ecs::Context& context)
		: ecs::System(context, "render", type_id<RenderSystem>)
	{
	}

	RenderSystem::~RenderSystem()
	{
	}

	graphics::RenderObject& RenderSystem::getOrCreate(ecs::EntityID id)
	{
		auto iter = m_index.find(id);
		if (iter != m_index.end())
		{
			return m_data[iter->second];
		}
		m_index[id] = m_id.size();
		m_id.push_back(id);
		m_data.push_back({});
		return m_data.back();
	}

	const std::vector<graphics::RenderObject>& RenderSystem::getRenderObjects()
	{
		return m_data;
	}

	void RenderSystem::remove(ecs::EntityID id)
	{
		// Not called while "update running"
		auto iter = m_index.find(id);
		if (iter != m_index.end())
		{
			if (m_data.size() <= 1)
			{
				m_data.clear();
				m_id.clear();
				m_index.clear();
				return;
			}

			size_t index = iter->second;
			swapBack(m_data, index);
			swapBack(m_id, index);

			// Correct index records:
			m_index.erase(iter);
			m_index[m_id[index]] = index;
		}
	}

	void RenderSystem::update(const ecs::Timestamp& time)
	{
		auto* tsystem = m_context.getSystem<TransformSystem>();
		assert(tsystem != nullptr);

		for (size_t i = 0; i < m_id.size(); ++i)
		{
			ecs::EntityID& id = m_id[i];
			graphics::RenderObject& ro = m_data[i];

			Transform& transform = tsystem->getOrCreate(id);
			ro.matrix = transform.world;
		}
	}
} // ns engine
