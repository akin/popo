#include <ecs/transformsystem.h>
#include <ecs/typeid.h>
#include <ecs/context.h>

#include <spdlog/spdlog.h>

#include <assert.h>

namespace engine
{
	TransformSystem::TransformSystem(ecs::Context& context)
		: System(context, "transform", type_id<TransformSystem>)
	{
	}

	TransformSystem::~TransformSystem()
	{
	}

	Transform& TransformSystem::getOrCreate(ecs::EntityID id)
	{
		return m_data[id];
	}

	const Transform* TransformSystem::get(ecs::EntityID id) const
	{
		auto iter = m_data.find(id);
		if (iter != m_data.end())
		{
			return &(iter->second);
		}
		return nullptr;
	}

	void TransformSystem::remove(ecs::EntityID id)
	{
		// Not called while "update running"
		auto iter = m_data.find(id);
		if (iter != m_data.end())
		{
			m_data.erase(iter);
		}
	}

	void TransformSystem::update(const ecs::Timestamp& time)
	{
		// The idea would be to add bunch of "tasks" to context workers here..
		// The tasks would need to be self organizing so that they have "priorities"
		// organizing Systems in some way of "TransformSystem needs to be before RenderSystem!"
		// instead lets let the tasks organize themselfs that way so.. somehow..
		// RenderTasks needs to be last, and TransformTasks before them..
		for (auto& iter : m_data)
		{
			iter.second.update();
		}
	}
} // ns engine
