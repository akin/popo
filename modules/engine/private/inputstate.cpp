#include <ecs/inputstate.h>

#include <vecutils.h>
#include <spdlog/spdlog.h>
#include <assert.h>

namespace
{
    void addMouse(const input::MouseState& src, input::MouseState& target)
    {
        target.delta += src.delta;
        target.position = src.position;
    }
}

namespace engine
{
    InputState::InputState(const input::SemanticMap& map)
    : m_map(map)
    {
        update();
    }

    void InputState::update()
    {
        // Mouse deltas reset for next frame
        m_mouse.delta.x = 0;
        m_mouse.delta.y = 0;

        m_keys.resize(m_map.getMaximum());
    }
    
    void InputState::setKey(input::SemanticID semantic, input::KeyState state)
    {
        if(static_cast<size_t>(semantic) >= m_keys.size())
        {
            assert(false);
            return;
        }
        m_keys[static_cast<size_t>(semantic)] = state;
    }

    input::KeyState InputState::getKey(input::SemanticID semantic) const
    {
        if(static_cast<size_t>(semantic) >= m_keys.size())
        {
            return input::KeyState::none;
        }
        return m_keys[static_cast<size_t>(semantic)];
    }
		
    void InputState::setMousePosition(input::MouseState state)
    {
        addMouse(state, m_mouse);
    }

    input::MouseState InputState::getMousePosition() const
    {
        return m_mouse;
    }
} // ns engine
