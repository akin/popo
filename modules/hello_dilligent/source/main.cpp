
#include <iostream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <SDL.h>
#include <utility/system.h>
#include <utility/window.h>

#ifndef NOMINMAX
#    define NOMINMAX
#endif

#include <EngineFactoryVk.h>

#include <RenderDevice.h>
#include <DeviceContext.h>
#include <SwapChain.h>
#include <RefCntAutoPtr.hpp>

using namespace Diligent;

// For this tutorial, we will use simple vertex shader
// that creates a procedural triangle

// Diligent Engine can use HLSL source on all supported platforms.
// It will convert HLSL to GLSL in OpenGL mode, while Vulkan backend will compile it directly to SPIRV.

static const char* VSSource = R"(
struct PSInput 
{ 
    float4 Pos   : SV_POSITION; 
    float3 Color : COLOR; 
};

void main(in  uint    VertId : SV_VertexID,
          out PSInput PSIn) 
{
    float4 Pos[3];
    Pos[0] = float4(-0.5, -0.5, 0.0, 1.0);
    Pos[1] = float4( 0.0, +0.5, 0.0, 1.0);
    Pos[2] = float4(+0.5, -0.5, 0.0, 1.0);

    float3 Col[3];
    Col[0] = float3(1.0, 0.0, 0.0); // red
    Col[1] = float3(0.0, 1.0, 0.0); // green
    Col[2] = float3(0.0, 0.0, 1.0); // blue

    PSIn.Pos   = Pos[VertId];
    PSIn.Color = Col[VertId];
}
)";

// Pixel shader simply outputs interpolated vertex color
static const char* PSSource = R"(
struct PSInput 
{ 
    float4 Pos   : SV_POSITION; 
    float3 Color : COLOR; 
};

struct PSOutput
{ 
    float4 Color : SV_TARGET; 
};

void main(in  PSInput  PSIn,
          out PSOutput PSOut)
{
    PSOut.Color = float4(PSIn.Color.rgb, 1.0);
}
)";


class App
{
public:
	App()
	{
	}

	~App()
	{
		m_context->Flush();
	}

	bool initialize(utility::Window* window)
	{
		if (window == nullptr)
		{
			return false;
		}
		SwapChainDesc swapchainDesc;

		auto getEngineFactory = LoadGraphicsEngineVk();

		EngineVkCreateInfo engineInfo;
		engineInfo.EnableValidation = true;

		auto* factory = getEngineFactory();
		factory->CreateDeviceAndContextsVk(engineInfo, &m_device, &m_context);
		if (!m_swapchain)
		{
			Diligent::NativeWindow nativeWindow{ window->getHandle().hwnd };
			factory->CreateSwapChainVk(m_device, m_context, swapchainDesc, nativeWindow, &m_swapchain);
			return true;
		}

		return false;
	}

	void createResources()
	{
		// Pipeline state object encompasses configuration of all GPU stages

		GraphicsPipelineStateCreateInfo PSOCreateInfo;

		// Pipeline state name is used by the engine to report issues.
		// It is always a good idea to give objects descriptive names.
		PSOCreateInfo.PSODesc.Name = "Simple triangle PSO";

		// This is a graphics pipeline
		PSOCreateInfo.PSODesc.PipelineType = PIPELINE_TYPE_GRAPHICS;

		// clang-format off
		// This tutorial will render to a single render target
		PSOCreateInfo.GraphicsPipeline.NumRenderTargets = 1;
		// Set render target format which is the format of the swap chain's color buffer
		PSOCreateInfo.GraphicsPipeline.RTVFormats[0] = m_swapchain->GetDesc().ColorBufferFormat;
		// Use the depth buffer format from the swap chain
		PSOCreateInfo.GraphicsPipeline.DSVFormat = m_swapchain->GetDesc().DepthBufferFormat;
		// Primitive topology defines what kind of primitives will be rendered by this pipeline state
		PSOCreateInfo.GraphicsPipeline.PrimitiveTopology = PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		// No back face culling for this tutorial
		PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = CULL_MODE_NONE;
		// Disable depth testing
		PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = False;
		// clang-format on

		ShaderCreateInfo ShaderCI;
		// Tell the system that the shader source code is in HLSL.
		// For OpenGL, the engine will convert this into GLSL under the hood
		ShaderCI.SourceLanguage = SHADER_SOURCE_LANGUAGE_HLSL;
		// OpenGL backend requires emulated combined HLSL texture samplers (g_Texture + g_Texture_sampler combination)
		ShaderCI.UseCombinedTextureSamplers = true;
		// Create a vertex shader
		RefCntAutoPtr<IShader> pVS;
		{
			ShaderCI.Desc.ShaderType = SHADER_TYPE_VERTEX;
			ShaderCI.EntryPoint = "main";
			ShaderCI.Desc.Name = "Triangle vertex shader";
			ShaderCI.Source = VSSource;
			m_device->CreateShader(ShaderCI, &pVS);
		}

		// Create a pixel shader
		RefCntAutoPtr<IShader> pPS;
		{
			ShaderCI.Desc.ShaderType = SHADER_TYPE_PIXEL;
			ShaderCI.EntryPoint = "main";
			ShaderCI.Desc.Name = "Triangle pixel shader";
			ShaderCI.Source = PSSource;
			m_device->CreateShader(ShaderCI, &pPS);
		}

		// Finally, create the pipeline state
		PSOCreateInfo.pVS = pVS;
		PSOCreateInfo.pPS = pPS;
		m_device->CreateGraphicsPipelineState(PSOCreateInfo, &m_pipelinestate);
	}

	void render()
	{
		// Set render targets before issuing any draw command.
		// Note that Present() unbinds the back buffer if it is set as render target.
		auto* pRTV = m_swapchain->GetCurrentBackBufferRTV();
		auto* pDSV = m_swapchain->GetDepthBufferDSV();
		m_context->SetRenderTargets(1, &pRTV, pDSV, RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

		// Clear the back buffer
		const float ClearColor[] = { 0.350f, 0.350f, 0.350f, 1.0f };
		// Let the engine perform required state transitions
		m_context->ClearRenderTarget(pRTV, ClearColor, RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
		m_context->ClearDepthStencil(pDSV, CLEAR_DEPTH_FLAG, 1.f, 0, RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

		// Set the pipeline state in the immediate context
		m_context->SetPipelineState(m_pipelinestate);

		// Typically we should now call CommitShaderResources(), however shaders in this example don't
		// use any resources.

		DrawAttribs drawAttrs;
		drawAttrs.NumVertices = 3; // Render 3 vertices
		m_context->Draw(drawAttrs);
	}

	void present()
	{
		m_swapchain->Present();
	}

	void windowResize(Uint32 width, Uint32 height)
	{
		if (m_swapchain)
			m_swapchain->Resize(width, height);
	}

private:
	RefCntAutoPtr<IRenderDevice> m_device;
	RefCntAutoPtr<IDeviceContext> m_context;
	RefCntAutoPtr<ISwapChain> m_swapchain;
	RefCntAutoPtr<IPipelineState> m_pipelinestate;
};

int main(int count, char** args)
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}
	spdlog::info("Welcome to POPO!");
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	utility::System system;
	std::unique_ptr<utility::Window> window = system.createWindow();

	window->init({ 800, 600 }, { 110,110 }, "Hello world");

	std::unique_ptr<App> app;
	app.reset(new App);

	if (!app->initialize(window.get()))
	{
		return -1;
	}

	app->createResources();

	unsigned int counter = 0;
	while (counter < 20)
	{
		app->render();
		app->present();
		counter++;
		SDL_Delay(100);
	}

	return 0;
}
