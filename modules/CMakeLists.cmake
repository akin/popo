
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/common")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/input")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/filesystem")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/platform")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/graphics")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/ecs")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/mop")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/engine")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/toymaze")

set_target_properties(ToyMaze PROPERTIES FOLDER "modules")

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/toyparticles")
#add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/hello_dilligent")

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/opengl_simple_app")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/opengl_gltf_app")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/opengl_image_app")

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/hello_main")
