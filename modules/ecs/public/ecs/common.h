#ifndef ECS_COMMON_H_INCLUDED_POPO
#define ECS_COMMON_H_INCLUDED_POPO

#include <cstdint>

namespace ecs
{
#define SECONDS2NANO 1000000000

	using EntityID = uint32_t;
	const EntityID invalidEntity = 0;

	using NanoSeconds = int64_t;
	using Seconds = float;

	struct Timestamp
	{
	public:
		uint64_t frameID = 0;
		NanoSeconds deltaNano = 0;
		NanoSeconds totalNano = 0;

		Seconds deltaSeconds = 0.0f;
		Seconds totalSeconds = 0.0f;

		void update()
		{
			deltaSeconds = (float)deltaNano / SECONDS2NANO;
			totalSeconds = (float)totalNano / SECONDS2NANO;
		}
	};
} // ns ecs

#endif // ECS_COMMON_H_INCLUDED_POPO