
#ifndef TYPEID_WTF_
#define TYPEID_WTF_

// https://mikejsavage.co.uk/blog/cpp-tricks-type-id.html
inline int g_type_id_seq = 0;
template< typename T > inline const int type_id = g_type_id_seq++;

#endif