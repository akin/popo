#ifndef ECS_SYSTEM_H_INCLUDED_POPO
#define ECS_SYSTEM_H_INCLUDED_POPO

#include <ecs/common.h>
#include <vector>
#include <string>
#include <memory>

namespace ecs
{
	class Context;
	class System
	{
	public:
		System(const System&) = delete;

		virtual ~System();

		virtual void update(const Timestamp& time) = 0;

		virtual void remove(EntityID id) = 0;

		const std::string& getName() const;
		int getID() const;
	protected:
		System(Context& context, const std::string& name, int id);

		Context& m_context;
	private:
		std::string m_name;
		const int m_id;
	}; // ns ecs
} // ecs
#endif // ECS_SYSTEM_H_INCLUDED_POPO