#ifndef ECS_CONTEXT_H_INCLUDED_POPO
#define ECS_CONTEXT_H_INCLUDED_POPO

#include <ecs/common.h>
#include <ecs/typeid.h>
#include <vector>
#include <unordered_map>
#include <string>
#include <memory>

namespace ecs
{
	class System;
	class Context final
	{
	public:
		Context();
		Context(const Context& other) = delete;
		~Context();

		EntityID createEntity();

		void freeEntity(EntityID id);

		template<class TSystem>
		void createSystem()
		{
			std::unique_ptr<TSystem> system = std::make_unique<TSystem>(*this);
			m_systems[system->getID()] = std::move(system);
		}

		void update(const Timestamp& time);

		template <class SystemType>
		SystemType* getSystem()
		{
			auto& iter = m_systems.find(type_id<SystemType>);
			if (iter == m_systems.end())
			{
				return nullptr;
			}
			return static_cast<SystemType*>(iter->second.get());
		}
	private:
		const EntityID m_startID = 10;
		EntityID m_freeID = m_startID;

		std::vector<EntityID> m_deferred;
		std::unordered_map<int, std::unique_ptr<System>> m_systems;
	};
} // ns ecs

#endif // ECS_CONTEXT_H_INCLUDED_POPO