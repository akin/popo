#include <ecs/system.h>
#include <spdlog/spdlog.h>

#include <assert.h>

namespace ecs
{
	System::System(Context& context, const std::string& name, int id)
	: m_context(context)
	, m_name(name)
	, m_id(id)
	{
	}
	
	System::~System()
	{
	}

    const std::string& System::getName() const
	{
		return m_name;
	}

    int System::getID() const
	{
		return m_id;
	}
} // ns ecs
