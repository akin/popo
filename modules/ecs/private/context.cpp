#include <ecs/context.h>
#include <ecs/system.h>
#include <spdlog/spdlog.h>

#include <assert.h>

namespace ecs
{
	Context::Context()
	{
	}

	Context::~Context()
	{
	}

	EntityID Context::createEntity()
	{
		return ++m_freeID;
	}

	void Context::freeEntity(EntityID id)
	{
		m_deferred.push_back(id);
		spdlog::info("{0}:{1} Entity freed {2}.", __FILE__, __LINE__, id);
	}

	void Context::update(const Timestamp& time)
	{
		// Cleaning
		for (EntityID id : m_deferred)
		{
			for (auto& iter : m_systems)
			{
				iter.second->remove(id);
			}
		}
		m_deferred.clear();

		// Request systems to fill workers
		for (auto& iter : m_systems)
		{
			iter.second->update(time);
		}

		// Execute workers here
		// TODO!
	}
} // ns ecs
