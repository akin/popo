#include <graphics/mesh.h>

namespace graphics
{
	void Mesh::setAttribute(AttributeInfo info)
	{
		m_attributes.push_back(info);
	}

	AttributeInfo Mesh::getAttribute(AttributeType type)
	{
		for (auto& info : m_attributes)
		{
			if (info.type == type)
			{
				return info;
			}
		}
		assert(false);
		return {};
	}

	void Mesh::setIndices(IndicesInfo info)
	{
		m_indices = info;
	}

	IndicesInfo& Mesh::getIndicesInfo()
	{
		return m_indices;
	}
	
	void Mesh::setName(const std::string& name)
	{
		m_name = name;
	}

	const std::string& Mesh::getName() const
	{
		return m_name;
	}
} // ns graphics

