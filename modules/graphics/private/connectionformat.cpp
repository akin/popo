#include <graphics/connectionformat.h>

namespace graphics
{
	ConnectionFormat::ConnectionFormat(Diligent::TEXTURE_FORMAT color, Diligent::TEXTURE_FORMAT depth, glm::uvec2 resolution)
		: color(color)
		, depth(depth)
		, resolution(resolution)
	{
	}
} // ns graphics

