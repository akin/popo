#include <graphics/effect.h>

namespace graphics
{
Effect::Effect(Diligent::PIPELINE_TYPE type, const std::string& name)
    : m_name(name)
    , m_type(type)
{
}

Diligent::GraphicsPipelineStateCreateInfo Effect::getCreateInfo()
{
    Diligent::GraphicsPipelineStateCreateInfo info;
    info.PSODesc.Name = m_name.c_str();
    info.PSODesc.PipelineType = m_type;

    if (!m_attributeLayout.empty())
    {
        info.GraphicsPipeline.InputLayout.LayoutElements = m_attributeLayout.data();
        info.GraphicsPipeline.InputLayout.NumElements = static_cast<uint32_t>(m_attributeLayout.size());
    }
    if (!m_variables.empty())
    {
        info.PSODesc.ResourceLayout.Variables = m_variables.data();
        info.PSODesc.ResourceLayout.NumVariables = static_cast<uint32_t>(m_variables.size());
    }

    return info;
}

void Effect::addAttribute(const Diligent::LayoutElement& element)
{
    m_attributeLayout.push_back(element);
}

void Effect::addVariable(const Diligent::ShaderResourceVariableDesc& desc)
{
    m_variables.push_back(desc);
}
} // ns graphics

