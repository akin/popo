#include <graphics/material.h>

namespace graphics
{
	void Material::set(MaterialRenderPassValue value)
	{
		m_values[value.pass] = std::move(value);
	}

	void Material::setName(const std::string& name)
	{
		m_name = name;
	}

	const std::string& Material::getName() const
	{
		return m_name;
	}
} // ns graphics

