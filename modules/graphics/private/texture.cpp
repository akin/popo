#include <graphics/texture.h>

namespace graphics
{
Texture& Texture::set1D(uint32_t resolution)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_1D;
    m_desc.Width = resolution;
    m_desc.Height = 0;
    m_desc.ArraySize = 1;
    return *this;
}

Texture& Texture::set1DArray(uint32_t resolution, uint32_t arraysize)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_1D_ARRAY;
    m_desc.Width = resolution;
    m_desc.Height = 0;
    m_desc.ArraySize = arraysize;
    return *this;
}

Texture& Texture::set2D(const glm::uvec2& resolution)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_2D;
    m_desc.Width = resolution.x;
    m_desc.Height = resolution.y;
    m_desc.ArraySize = 1;
    return *this;
}

Texture& Texture::set2DArray(const glm::uvec2& resolution, uint32_t arraysize)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_2D_ARRAY;
    m_desc.Width = resolution.x;
    m_desc.Height = resolution.y;
    m_desc.ArraySize = arraysize;
    return *this;
}

Texture& Texture::set3D(const glm::uvec3& resolution)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_3D;
    m_desc.Width = resolution.x;
    m_desc.Height = resolution.y;
    m_desc.Depth = resolution.z;
    return *this;
}

Texture& Texture::setCube(const glm::uvec2& resolution)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_CUBE;
    m_desc.Width = resolution.x;
    m_desc.Height = resolution.y;
    m_desc.ArraySize = 1;
    return *this;
}

Texture& Texture::setCubeArray(const glm::uvec2& resolution, uint32_t arraysize)
{
    m_desc.Type = Diligent::RESOURCE_DIM_TEX_CUBE_ARRAY;
    m_desc.Width = resolution.x;
    m_desc.Height = resolution.y;
    m_desc.ArraySize = arraysize;
    return *this;
}

Texture& Texture::setName(const std::string& name)
{
    m_name = name;
    m_desc.Name = m_name.c_str();
    return *this;
}

Texture& Texture::setFormat(Diligent::TEXTURE_FORMAT format)
{
    m_desc.Format = format;
    return *this;
}

Texture& Texture::setUsage(Diligent::USAGE usage)
{
    m_desc.Usage = usage;
    return *this;
}

Texture& Texture::setBindFlags(Diligent::BIND_FLAGS bindFlags)
{
    m_desc.BindFlags = bindFlags;
    return *this;
}

bool Texture::init(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device)
{
    Diligent::ITexture* texture;
    device->CreateTexture(m_desc, nullptr, &texture);
    m_texture = texture;
    return true;
}
} // ns graphics

