#include <graphics/node.h>

namespace graphics
{
Node::Node(const std::string& name)
    : m_name(name)
{
}

bool Node::initialize()
{
    return true;
}

void Node::setOutput(std::shared_ptr<Node> output)
{
    m_output = output;
}

ConnectionFormat Node::getInputConnectionFormat()
{
    return {};
}

void Node::resize(const glm::uvec2& size)
{
}

void Node::attach(RenderObject& object)
{
}

void Node::bindAsOuput()
{
}

const std::string& Node::getName() const
{
    return m_name;
}
} // ns graphics

