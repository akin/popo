#include <graphics/pipeline.h>

namespace graphics
{
	Pipeline::Pipeline(const std::string& name)
		: m_name(name)
	{
	}

	const std::string& Pipeline::getName() const
	{
		return m_name;
	}

	void Pipeline::attach(RenderObject& object)
	{
		for (auto& node : m_nodes)
		{
			node->attach(object);
		}
	}

	void Pipeline::render(RenderData& renderData)
	{
		for (auto& node : m_nodes)
		{
			node->render(renderData);
		}
	}

	void Pipeline::resize(const glm::uvec2& size)
	{
		for (auto& node : m_nodes)
		{
			node->resize(size);
		}
	}

	void Pipeline::addNode(std::shared_ptr<Node> node)
	{
		m_nodes.push_back(node);
	}
} // ns graphics

