#include <graphics/effectpipeline.h>

namespace graphics
{
	void EffectPipeline::setEffect(RenderPassID pass, const std::shared_ptr<Effect>& effect)
	{
		auto& data = m_effects[pass];
		data.pass = pass;
		data.effect = effect;
	}

	std::shared_ptr<Effect> EffectPipeline::getEffectForPass(RenderPassID pass) const
	{
		auto iter = m_effects.find(pass);
		if (iter == m_effects.end())
		{
			return nullptr;
		}
		return iter->second.effect;
	}

    std::vector<RenderPassID> EffectPipeline::getRenderPasses() const
    {
		std::vector<RenderPassID> values;
		values.reserve(m_effects.size());
		for (auto& iter : m_effects)
		{
			values.push_back(iter.first);
		}
		return std::move(values);
    }
} // ns graphics

