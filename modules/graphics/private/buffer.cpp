#include <graphics/buffer.h>

namespace graphics
{
Buffer& Buffer::setName(const std::string& name)
{
    m_name = name;
    m_desc.Name = m_name.c_str();
    return *this;
}

Buffer& Buffer::setByteSize(uint32_t size)
{
    m_desc.uiSizeInBytes = size;
    return *this;
}

Buffer& Buffer::setUsage(Diligent::USAGE usage)
{
    m_desc.Usage = usage;
    return *this;
}

Buffer& Buffer::setBindFlags(Diligent::BIND_FLAGS bindFlags)
{
    m_desc.BindFlags = bindFlags;
    return *this;
}

Buffer& Buffer::setCpuAccessFlags(Diligent::BIND_FLAGS bindFlags)
{
    m_desc.BindFlags = bindFlags;
    return *this;
}

Buffer& Buffer::setCpuAccessFlags(Diligent::CPU_ACCESS_FLAGS cpuAccessFlags)
{
    m_desc.CPUAccessFlags = cpuAccessFlags;
    return *this;
}

bool Buffer::init(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device)
{
    Diligent::IBuffer* buffer;
    device->CreateBuffer(m_desc, nullptr, &buffer);
    m_buffer = buffer;
    return true;
}
} // ns graphics

