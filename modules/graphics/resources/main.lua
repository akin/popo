
local simple = require("test.simple")
local complex = require("test.complex")
local cube = require("test.cube")

cube.test()
simple.test()
complex.test()
