local cube =  {}

function cube.test() 
    print("Generating and saving unitcube.")
    local mesh = createUnitCube()
    if not saveMesh(mesh, "unitcube.glb") then
        print("Failed to save unitcube!")
    end
end

return cube
