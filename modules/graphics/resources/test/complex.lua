local complex =  {}

function complex.test() 
    local mapSize = IVec2:new(400, 400)
    local startPoint = IVec2:new(10, 10)
    local endPoint = IVec2:new(mapSize.x - 10, mapSize.y - 10)
    
    local emptyTile = Tile:new(32, Vec4:new(0.55, 0.55, 0.75, 1.0))
    local wallTile = Tile:new(219, Vec4:new(0.21, 0.21, 0.42, 1.0))
    local startTile = Tile:new(83, Vec4:new(0.0, 1.0, 0.0, 1.0))
    local endTile = Tile:new(69, Vec4:new(0.0, 0.0, 1.0, 1.0))
    local routeTile = Tile:new(83, Vec4:new(1.0, 1.0, 1.0, 1.0))
    
    local config = MazeConfig:new()
    config.size = mapSize
    config.points["start"] = startPoint
    config.points["end"] = endPoint
    
    config.tiles["start"] = startTile
    config.tiles["end"] = endTile
    config.tiles["wall"] = wallTile
    config.tiles["empty"] = emptyTile
    config.tiles["route"] = routeTile
    
    print("Creating maze")
    local seed = 3

    function drawMaze(maze)
        connection = maze:getShortestPath(startPoint, endPoint, emptyTile)
        
        maze:setTiles(connection, routeTile)
        maze:setTile(startPoint, startTile)
        maze:setTile(endPoint, endTile)
    end

    for complexity = 1, 400, 1 do
        local prefix = "maze_"
        print("Generate round " .. complexity)

        -- Random
        maze = createMazeRandom(config, complexity, 10, 3, seed)
        drawMaze(maze)
        filename = prefix .. "random_" .. complexity .. ".png"
        if not saveMazeToImage(maze, filename) then
            print("Failed to save " .. filename)
        end

        -- Triangle
        maze = createMazeTriangles(config, complexity, seed)
        drawMaze(maze)
        filename = prefix .. "triangle_" .. complexity .. ".png"
        if not saveMazeToImage(maze, filename) then
            print("Failed to save " .. filename)
        end

        -- Voronoi
        maze = createMazeVoronoi(config, complexity, seed)
        drawMaze(maze)
        filename = prefix .. "voronoi_" .. complexity .. ".png"
        if not saveMazeToImage(maze, filename) then
            print("Failed to save " .. filename)
        end
    end   
end

return complex
