#ifndef GRAPHICS_RENDEROBJECT_H_
#define GRAPHICS_RENDEROBJECT_H_

#include "common.h"
#include "mesh.h"
#include <memory>

namespace graphics
{
	class RenderObject
	{
	public:
		std::shared_ptr<Mesh> mesh;

		glm::mat4 matrix;

		Diligent::RefCntAutoPtr<Diligent::IShaderResourceBinding> resourceBinding;
	};

	using RenderSet = std::vector<RenderObject>;
} // graphics
#endif // GRAPHICS_RENDEROBJECT_H_
