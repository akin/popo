#ifndef GRAPHICS_TEXTURE_H_
#define GRAPHICS_TEXTURE_H_

#include "common.h"
#include <Texture.h>
#include <RenderDevice.h>
#include <RefCntAutoPtr.hpp>
#include <GraphicsTypes.h>

namespace graphics
{
	/*
	Texture texture;
	if(texture.setName("MyTexture")
		.set2D({128, 128})
		.setFormat(Diligent::TEX_FORMAT_RGBA8_UNORM)
		.setUsage(Diligent::USAGE_DEFAULT)
		.setBindFlags(Diligent::BIND_SHADER_RESOURCE | Diligent::BIND_RENDER_TARGET | Diligent::BIND_UNORDERED_ACCESS)
		.init(device))
		{
			assert(false);
		}
	*/
	class Texture
	{
	public:
		Texture& set1D(uint32_t resolution);

		Texture& set1DArray(uint32_t resolution, uint32_t arraysize);

		Texture& set2D(const glm::uvec2& resolution);

		Texture& set2DArray(const glm::uvec2& resolution, uint32_t arraysize);

		Texture& set3D(const glm::uvec3& resolution);

		Texture& setCube(const glm::uvec2& resolution);

		Texture& setCubeArray(const glm::uvec2& resolution, uint32_t arraysize);

		Texture& setName(const std::string& name);

		Texture& setFormat(Diligent::TEXTURE_FORMAT format);

		Texture& setUsage(Diligent::USAGE usage);

		Texture& setBindFlags(Diligent::BIND_FLAGS bindFlags);

		bool init(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device);
	private:
		std::string m_name;
		Diligent::TextureDesc m_desc;
		Diligent::RefCntAutoPtr<Diligent::ITexture> m_texture;
	};
} // ns graphics

#endif // GRAPHICS_TEXTURE_H_
