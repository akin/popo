#ifndef GRAPHICS_MESH_H_
#define GRAPHICS_MESH_H_

#include <string>
#include "common.h"
#include "buffer.h"

namespace graphics
{

	struct AttributeInfo
	{
		Diligent::RefCntAutoPtr<Diligent::IBuffer> buffer;
		AttributeType type;
		uint32_t offset = 0;
	};

	struct IndicesInfo
	{
		Diligent::RefCntAutoPtr<Diligent::IBuffer> buffer;
		Diligent::VALUE_TYPE type = Diligent::VT_UINT32;
		uint32_t count;
		uint32_t offset = 0;
	};

	class Mesh
	{
	public:
		void setAttribute(AttributeInfo info);

		AttributeInfo getAttribute(AttributeType type);

		void setIndices(IndicesInfo info);

		IndicesInfo& getIndicesInfo();

		float radius{ 0.0f };
		float scale{ 1.0f };
		glm::vec3 pivot{ 0.0f };

		void setName(const std::string& name);
		const std::string& getName() const;
	private:
		std::vector<AttributeInfo> m_attributes;
		IndicesInfo m_indices;
		std::string m_name;
	};
} // graphics
#endif // GRAPHICS_MESH_H_
