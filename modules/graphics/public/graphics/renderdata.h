#ifndef GRAPHICS_RENDERDATA_H_
#define GRAPHICS_RENDERDATA_H_

#include "common.h"
#include "renderobject.h"
#include "renderview.h"

namespace graphics
{
	class RenderData
	{
	public:
		std::vector<RenderSet> sets;
		std::vector<RenderView> views;
	};
} // graphics
#endif // GRAPHICS_RENDERDATA_H_
