#ifndef GRAPHICS_COMMON_H_
#define GRAPHICS_COMMON_H_

#include <glm/glm.hpp>
#include <cstdint>
#include <vector>
#include <string>

namespace graphics
{
	enum class AttributeType : uint32_t
	{
		None = 0,
		Position,
		TextureCoordinate,
		Normal,
		Tangent,
		BiTangent,
		Color,
		BoneWeight,
		BoneIndex,
	};

	struct Rect2D
	{
		glm::uvec2 offset;
		glm::uvec2 extent;
	};

	using RenderPassID = uint32_t;
} // graphics

#endif // GRAPHICS_COMMON_H_
