#ifndef GRAPHICS_MESH_GENERATOR_H_
#define GRAPHICS_MESH_GENERATOR_H_

#include <graphics/common.h>
#include <graphics/mesh.h>

namespace graphics
{
	namespace generator
	{
		std::shared_ptr<Mesh> createUnitCube(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device)
		{
			auto mesh = std::make_shared<Mesh>();

			// Generate Cube positions
			{
				float unit = 1.0f;
				std::vector<glm::vec3> positions;
				positions.push_back({ -unit, -unit, -unit });
				positions.push_back({ -unit,  unit, -unit });
				positions.push_back({ unit,  unit, -unit });
				positions.push_back({ unit, -unit, -unit });

				positions.push_back({ -unit, -unit, unit });
				positions.push_back({ -unit,  unit, unit });
				positions.push_back({ unit,  unit, unit });
				positions.push_back({ unit, -unit, unit });

				Diligent::BufferDesc desc;
				desc.Name = "unit cube positions";
				desc.uiSizeInBytes = static_cast<uint32_t>(positions.size() * sizeof(glm::vec3));
				desc.Usage = Diligent::USAGE::USAGE_IMMUTABLE;
				desc.BindFlags = Diligent::BIND_FLAGS::BIND_VERTEX_BUFFER;

				Diligent::BufferData data;
				data.pData = positions.data();
				data.DataSize = static_cast<uint32_t>(positions.size() * sizeof(glm::vec3));

				AttributeInfo info;
				info.type = AttributeType::Position;
				device->CreateBuffer(desc, &data, &info.buffer);

				mesh->setAttribute(info);
			}

			// Generate Cube indices
			{
				std::vector<uint16_t> indices;

				auto addQuad = [&indices](uint16_t i, uint16_t i2, uint16_t i3, uint16_t i4) {
					indices.push_back(i);
					indices.push_back(i2);
					indices.push_back(i4);

					indices.push_back(i2);
					indices.push_back(i3);
					indices.push_back(i4);
				};

				addQuad(0, 1, 2, 3); // front
				addQuad(7, 6, 5, 4); // back

				addQuad(4, 5, 1, 0); // left
				addQuad(3, 2, 6, 7); // right

				addQuad(4, 0, 3, 7); // bottom
				addQuad(1, 5, 6, 2); // top

				Diligent::BufferDesc desc;
				desc.Name = "unit cube indices";
				desc.uiSizeInBytes = static_cast<uint32_t>(indices.size() * sizeof(uint16_t));
				desc.Usage = Diligent::USAGE::USAGE_IMMUTABLE;
				desc.BindFlags = Diligent::BIND_FLAGS::BIND_INDEX_BUFFER;

				Diligent::BufferData data;
				data.pData = indices.data();
				data.DataSize = static_cast<uint32_t>(indices.size() * sizeof(uint16_t));

				IndicesInfo info;
				info.type = Diligent::VT_UINT16;
				info.count = static_cast<uint32_t>(indices.size());
				device->CreateBuffer(desc, &data, &info.buffer);

				mesh->setIndices(info);
			}
			
			// Set culling parameters.
			float d = glm::sqrt(1.0f + 1.0f);
			mesh->radius = glm::sqrt(d*d + 1.0f);

			return mesh;
		}
	} // generator
} // graphics
#endif // GRAPHICS_MESH_GENERATOR_H_
