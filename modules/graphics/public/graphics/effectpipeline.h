#ifndef GRAPHICS_EFFECTPIPELINE_H_
#define GRAPHICS_EFFECTPIPELINE_H_

#include "common.h"
#include <string>
#include <unordered_map>
#include <memory>
#include <graphics/effect.h>

namespace graphics
{
	class RenderPassEffect
	{
	public:
		RenderPassID pass;
		std::shared_ptr<Effect> effect;
	};

	class EffectPipeline
	{
	public:
		void setEffect(RenderPassID pass, const std::shared_ptr<Effect>& effect);

		std::shared_ptr<Effect> getEffectForPass(RenderPassID pass) const;

		std::vector<RenderPassID> getRenderPasses() const;
	private:
		std::unordered_map<RenderPassID, RenderPassEffect> m_effects;
	};
} // graphics
#endif // GRAPHICS_EFFECTPIPELINE_H_
