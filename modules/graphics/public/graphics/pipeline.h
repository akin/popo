#ifndef GRAPHICS_PIPELINE_H_
#define GRAPHICS_PIPELINE_H_

#include "common.h"
#include "node.h"
#include "renderdata.h"
#include <memory>

namespace graphics
{
	class Pipeline
	{
	public:
		Pipeline(const std::string& name);

		const std::string& getName() const;

		void attach(RenderObject& object);

		void render(RenderData& renderData);

		void resize(const glm::uvec2& size);

		void addNode(std::shared_ptr<Node> node);
	private:
		std::string m_name;
		std::vector<std::shared_ptr<Node>> m_nodes;
	};
} // graphics
#endif // GRAPHICS_PIPELINE_H_
