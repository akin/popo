#ifndef GRAPHICS_BUFFER_H_
#define GRAPHICS_BUFFER_H_

#include <Buffer.h>
#include <RenderDevice.h>
#include <RefCntAutoPtr.hpp>
#include <GraphicsTypes.h>

namespace graphics
{	/*
	Buffer buffer;
	if(buffer.setName("MyBuffer")
		.setByteSize({1024})
		.setUsage(Diligent::USAGE_DYNAMIC)
		.setBindFlags(Diligent::BIND_UNIFORM_BUFFER)
		.setCpuAccessFlags(Diligent::CPU_ACCESS_WRITE)
		.init(device))
		{
			assert(false);
		}
	*/
	class Buffer
	{
	public:
		Buffer& setName(const std::string& name);

		Buffer& setByteSize(uint32_t size);

		Buffer& setUsage(Diligent::USAGE usage);

		Buffer& setBindFlags(Diligent::BIND_FLAGS bindFlags);

		Buffer& setCpuAccessFlags(Diligent::BIND_FLAGS bindFlags);
		
		Buffer& setCpuAccessFlags(Diligent::CPU_ACCESS_FLAGS cpuAccessFlags);

		bool init(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device);
	private:
		std::string m_name;
		Diligent::BufferDesc m_desc;
		Diligent::RefCntAutoPtr<Diligent::IBuffer> m_buffer;
	};
} // ns graphics

#endif // GRAPHICS_BUFFER_H_
