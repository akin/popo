#ifndef GRAPHICS_CONNECTION_FORMAT_H_
#define GRAPHICS_CONNECTION_FORMAT_H_

#include "common.h"
#include "buffer.h"

namespace graphics
{
	class ConnectionFormat
	{
	public:
		ConnectionFormat() = default;
		
		ConnectionFormat(Diligent::TEXTURE_FORMAT color, Diligent::TEXTURE_FORMAT depth, glm::uvec2 resolution = {0, 0});

        Diligent::TEXTURE_FORMAT color;
        Diligent::TEXTURE_FORMAT depth;
        glm::uvec2 resolution;
	};
} // graphics
#endif // GRAPHICS_CONNECTION_FORMAT_H_
