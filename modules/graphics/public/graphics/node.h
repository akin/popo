#ifndef GRAPHICS_NODE_H_
#define GRAPHICS_NODE_H_

#include "common.h"
#include "connectionformat.h"
#include "renderdata.h"
#include <memory>

namespace graphics
{
	class Node
	{
	public:
		Node(const std::string& name);

		virtual ~Node() = default;

		virtual bool initialize();

		void setOutput(std::shared_ptr<Node> output);

		virtual ConnectionFormat getInputConnectionFormat();

		virtual void resize(const glm::uvec2& size);

		virtual void attach(RenderObject& object);

		virtual void render(RenderData& renderData) = 0;

		virtual void bindAsOuput();

		const std::string& getName() const;
	protected:
		std::shared_ptr<Node> m_output;
	private:
		std::string m_name;
	};
} // graphics
#endif // GRAPHICS_NODE_H_
