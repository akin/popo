#ifndef GRAPHICS_RENDERVIEW_H_
#define GRAPHICS_RENDERVIEW_H_

#include "common.h"

namespace graphics
{
	class RenderView
	{
	public:
        Rect2D position;

		glm::mat4 view;
		glm::mat4 projection;
	};
} // graphics
#endif // GRAPHICS_RENDERVIEW_H_
