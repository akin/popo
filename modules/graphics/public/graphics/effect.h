#ifndef GRAPHICS_EFFECT_H_
#define GRAPHICS_EFFECT_H_

#include "common.h"
#include <PipelineState.h>
#include <RenderDevice.h>
#include <RefCntAutoPtr.hpp>

namespace graphics
{
	class Effect
	{
	public:
		Effect(Diligent::PIPELINE_TYPE type = Diligent::PIPELINE_TYPE::PIPELINE_TYPE_GRAPHICS, const std::string& name = "");
		
		virtual ~Effect() = default;

		virtual void initialize(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device) = 0;

		virtual Diligent::GraphicsPipelineStateCreateInfo getCreateInfo();
	protected:
		void addAttribute(const Diligent::LayoutElement& element);
		
		void addVariable(const Diligent::ShaderResourceVariableDesc& desc);
	private:
		std::vector<Diligent::LayoutElement> m_attributeLayout;
		std::vector<Diligent::ShaderResourceVariableDesc> m_variables;

		Diligent::PIPELINE_TYPE m_type;
		std::string m_name;
	};
} // graphics
#endif // GRAPHICS_EFFECT_H_
