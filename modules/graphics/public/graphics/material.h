#ifndef GRAPHICS_MATERIAL_H_
#define GRAPHICS_MATERIAL_H_

#include "common.h"
#include <string>
#include <unordered_map>
#include <memory>
#include <graphics/effect.h>

namespace graphics
{
	struct MaterialRenderPassValue
	{
		RenderPassID pass;
		std::shared_ptr<graphics::Effect> effect;
	};

	class Material
	{
	public:
		void set(MaterialRenderPassValue value);

		void setName(const std::string& name);
		const std::string& getName() const;
	private:
		std::string m_name;
		std::unordered_map<RenderPassID, MaterialRenderPassValue> m_values;
	};
} // graphics
#endif // GRAPHICS_MATERIAL_H_
