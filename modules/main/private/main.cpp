#include <iostream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <SDL.h>

#include <utility/system.h>
#include <utility/window.h>

#include <graphics/context.h>

int main(int argc, char* argv[])
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}
	spdlog::info("Welcome to POPO!");
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	utility::System system;

	graphics::context context;

	auto window = system.createWindow();

	window->init({ 800, 600 }, { 110,110 }, "Hello world");

	SDL_Delay(2000);

	return 0;
}
