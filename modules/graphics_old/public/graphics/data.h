#ifndef GRAPHICS_DATA_H_INCLUDED_POPO
#define GRAPHICS_DATA_H_INCLUDED_POPO

#include <stdint.h>
#include <string>

namespace graphics
{
struct DataInfo
{
    std::string name;
    size_t byteSize;
};

} // ns graphics

#endif // GRAPHICS_DATA_H_INCLUDED_POPO