#ifndef GRAPHICS_DEVICE_H_INCLUDED_POPO
#define GRAPHICS_DEVICE_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>
#include <graphics/context.h>
#include <vector>
#include <string>
#include <memory>

namespace graphics
{
class Device
{
public:
    Device(Context& context);
    virtual ~Device();

    bool init(const DeviceInfo& info, const Surface& surface);
    void destroy();
    void addExtension(const char* extension);
    const VkDevice& getDevice() const;
    const VkPhysicalDevice& getPhysicalDevice() const;
    const DeviceInfo& getDeviceInfo() const;

    VkFormatFeatureFlags getFormatFlags(VkFormat format);

    bool flush();
    bool finish();
protected:
    Context& m_context;

    // VK Device
    std::vector<const char*> m_deviceLayers;
    std::vector<const char*> m_deviceExtensions;
    DeviceInfo m_deviceInfo;
    VkDevice m_device = VK_NULL_HANDLE;

    // Queue
    QueueInfo m_graphics;
    QueueInfo m_compute;
    QueueInfo m_transfer;
    QueueInfo m_present;
};
} // ns graphics

#endif // GRAPHICS_DEVICE_H_INCLUDED_POPO