#ifndef GRAPHICS_SURFACE_H_INCLUDED_POPO
#define GRAPHICS_SURFACE_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>

#include <utility/window.h>

#include <graphics/graph/node.h>
#include <graphics/resourceblock.h>

#include <graphics/resources/buffer.h>
#include <graphics/resources/image.h>
#include <graphics/resources/imageview.h>

#include <graphics/descriptor/descriptorpool.h>
#include <graphics/descriptor/descriptorset.h>
#include <graphics/descriptor/pipelinelayout.h>

#include <graphics/swapchain.h>
#include <graphics/framebuffer.h>
#include <graphics/framedata.h>

#include <graphics/imagetypes.h>
#include <graphics/imagenode.h>
#include <vector>

namespace graphics
{
	class Context;
	struct DeviceInfo;
	class Surface
	{
	public:
		Surface(Context& context);
		~Surface();

		const VkSurfaceKHR& getSurface() const;
		const VkSurfaceCapabilitiesKHR& getCapabilities() const;

		bool isSupportedFormat(VkFormat format, VkColorSpaceKHR colorSpace) const;
		bool isSupportedPresentMode(VkPresentModeKHR presentMode) const;

		bool initialize(const DeviceInfo& device, const utility::Window& window);
		void destroy();

		bool draw(SwapChain& swapchain);

		void incrementReferenceCount();
		void decrementReferenceCount();

	private:
		Context& m_context;

		// Surface
		VkSurfaceCapabilitiesKHR m_capabilities;
		std::vector<VkSurfaceFormatKHR> m_formats;
		std::vector<VkPresentModeKHR> m_presentModes;
		VkSurfaceKHR m_surface = VK_NULL_HANDLE;

		IntrusivePtr<ResourceBlock> m_stagingBlock;
		Buffer* m_stagingBuffer;

		// Command Pool
		VkCommandPool m_commandPoolGraphics = VK_NULL_HANDLE;
		VkCommandPool m_commandPoolTransfer = VK_NULL_HANDLE;

		// Semaphore for images
		VkSemaphore m_imageAvailable = VK_NULL_HANDLE;
		VkSemaphore m_renderFinished = VK_NULL_HANDLE;

		// frame stuffs
		VkFence m_frameFence = VK_NULL_HANDLE;

		// Descriptor things
		IntrusivePtr<DescriptorPool> m_descriptorPool;
		IntrusivePtr<DescriptorLayoutSet> m_descriptorLayoutSet;

		// Frame datas
		std::vector<std::unique_ptr<FrameData>> m_frames;
	};
} // ns graphics

#endif // GRAPHICS_SURFACE_H_INCLUDED_POPO