#ifndef GRAPHICS_CONTEXT_H_INCLUDED_POPO
#define GRAPHICS_CONTEXT_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>
#include <vector>
#include <string>
#include <memory>

namespace graphics
{
class Context
{
public:
    Context();
    virtual ~Context();

    const std::vector<DeviceInfo>& getDevices() const;
    void setSettings(const Settings& settings);

    bool init();
    void destroy();
    void addExtension(const char* extension);
    const VkInstance& getInstance() const;
protected:
    Settings m_settings;

    // VK instance
    std::vector<const char*> m_instanceLayers;
    std::vector<const char*> m_instanceExtensions;
    std::vector<DeviceInfo> m_devices;

    VkDebugUtilsMessengerEXT m_debugUtil;
    VkInstance m_instance = VK_NULL_HANDLE;
};
} // ns graphics

#endif // GRAPHICS_CONTEXT_H_INCLUDED_POPO