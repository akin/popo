#ifndef GRAPHICS_IMAGE_H_INCLUDED_POPO
#define GRAPHICS_IMAGE_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>
#include <graphics/imagetypes.h>

namespace graphics
{
class ResourceBlock;
class Buffer;
class ImageView;
class Image
{
    friend class ResourceBlock;
public:
    Image(ResourceBlock& block);
    ~Image();

    void setup(const bntypes::ImageInfo& info);
    void setType(VkImageType value);
    void setFormat(VkFormat value);
    void setSize(uvec2 value);
    void setDepth(uint32_t value);
    void setMipCount(uint32_t value);
    void setArrayCount(uint32_t value);
    void setUsage(VkImageUsageFlags value);
    void setAspectMask(VkImageAspectFlags value);

    void setData(const IntrusivePtr<Buffer>& src, VkCommandBuffer& commandBuffer, uint32_t width, uint32_t height, uint32_t depth, uint32_t mipLevel, uint32_t arrayLevel, uint32_t arrayCount);

    uint32_t getMipCount() const;
    uint32_t getArrayCount() const;
    
    size_t getAllocatedByteSize() const;
    size_t getAllocatedByteOffset() const;

    VkFormat getFormat() const;
    const VkImage& getImage() const;

    const IntrusivePtr<ImageView>& getView();
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    // Called from ResourceBlock
    bool initialize();
    bool postInitialize();
    void destroy();

    void setAllocatedByteSize(size_t size);
    void setAllocatedByteOffset(size_t offset);
private:
    ResourceBlock& m_block;

    VkImageType m_type = VK_IMAGE_TYPE_2D;
    VkFormat m_format = VK_FORMAT_UNDEFINED;
    uvec2 m_dimensions;
    uint32_t m_depth = 1;
    uint32_t m_mipCount = 1;
    uint32_t m_arrayCount = 1;
    VkImageUsageFlags m_usage;
    VkImageAspectFlags m_aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;
    size_t m_allocatedByteSize = 0;
    size_t m_allocatedByteOffset = 0;

    VkImage m_image = VK_NULL_HANDLE;

    IntrusivePtr<ImageView> m_view;

    std::string m_name;
};
} // ns graphics

#endif // GRAPHICS_IMAGE_H_INCLUDED_POPO