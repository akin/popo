#ifndef BONA_GRAPHICS_BUFFER_H_
#define BONA_GRAPHICS_BUFFER_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>

namespace bngfx
{
class ResourceBlock;
class Buffer
{
    friend class ResourceBlock;
public:
    Buffer(ResourceBlock& block);
    ~Buffer();

    void setByteSize(size_t size);
    template <typename CType> void setSize(size_t size);
    void setUsage(VkBufferUsageFlags value);
    size_t getAllocatedByteSize() const;
    size_t getAllocatedByteOffset() const;
    size_t getByteSize() const;

    uint32_t getBufferOffset() const;

    bool map(void*& target, size_t offset, size_t size);
    void unmap();

    bool setData(const void* data, size_t offset, size_t size);
    void setData(const Buffer* src, VkCommandBuffer& commandBuffer, size_t srcOffset, size_t dstOffset, size_t dataSize);
    
    const VkBuffer& getBuffer() const;
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    // Called from ResourceBlock
    bool initialize();
    bool postInitialize();
    void destroy();

    void setAllocatedByteSize(size_t size);
    void setAllocatedByteOffset(size_t offset);
private:
    ResourceBlock& m_block;
    size_t m_byteSize = 0;
    VkBufferUsageFlags m_usage;

    size_t m_allocatedByteSize = 0;
    size_t m_allocatedByteOffset = 0;

    VkBuffer m_buffer = VK_NULL_HANDLE;

    std::string m_name;
};

template <typename CType> void Buffer::setSize(size_t size)
{
    setByteSize(size * sizeof(CType));
}

} // ns bngfx

#endif // BONA_GRAPHICS_BUFFER_H_