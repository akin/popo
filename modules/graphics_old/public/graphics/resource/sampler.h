#ifndef BONA_GRAPHICS_SAMPLER_H_
#define BONA_GRAPHICS_SAMPLER_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>

namespace bngfx
{
class Context;
class Sampler
{
public:
    explicit Sampler(Context& context);
    ~Sampler();

    void setFilters(VkFilter min, VkFilter mag);
    void setMipmapMode(VkSamplerMipmapMode value);
    void setMipMapLod(float bias, float min, float max);
    void setAddressmodes(VkSamplerAddressMode modeU, VkSamplerAddressMode modeV, VkSamplerAddressMode modeW);
    void setAnisotropy(bool enabled, float max = 16.0f);
    void setCompareOp(bool enabled, VkCompareOp op = VK_COMPARE_OP_ALWAYS);
    void setUnnormalizedCoordinates(bool value);

    const VkSampler& getSampler() const;

    bool initialize();
    void destroy();

    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    Context& m_context;
    VkSampler m_sampler = VK_NULL_HANDLE;

    VkFilter m_filterMin = VK_FILTER_LINEAR;
    VkFilter m_filterMag = VK_FILTER_LINEAR;
    VkSamplerMipmapMode m_mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    VkSamplerAddressMode m_addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    VkSamplerAddressMode m_addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    VkSamplerAddressMode m_addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    VkCompareOp m_compareOp = VK_COMPARE_OP_ALWAYS;
    float m_mipLodBias = 0.0f;
    float m_mipLodMin = 0.0f;
    float m_mipLodMax = 0.0f;
    float m_anisotropyMax = 16.0f;

    bool m_anisotropyEnabled = true;
    bool m_compareOpEnabled = false;
    bool m_unnormalizedCoordinates = false;
};
} // ns bngfx

#endif // BONA_GRAPHICS_SAMPLER_H_