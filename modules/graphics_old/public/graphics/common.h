#ifndef GRAPHICS_COMMON_H_INCLUDED_POPO
#define GRAPHICS_COMMON_H_INCLUDED_POPO

#define STAGING_BUFFER_SIZE ((1 << 20) * 16)

#include <common/math.h>
#include <common/intrusiveptr.h>
#include <common/platform.h>
#include <graphics/imagetypes.h>

#include <vector>
#include <string>
#include <functional>

namespace graphics
{
	// Bring whole "common namespace" into graphics.
	using namespace common;

	using RenderTypeID = uint32_t;
	using DescriptorID = uint32_t;
	using AttributeID = uint32_t;
	using DataFormat = DataFormat;

	enum class DescriptorSetID : uint32_t
	{
		object = 0,
		material = 1,
		MAX
	};

	enum class DescriptorType : uint32_t
	{
		none = 0,
		image,
		buffer
	};
	enum QueueFamilyFlags
	{
		none = 0,
		transfer = 1 << 0,
		graphics = 1 << 1,
		compute = 1 << 2,
		sparse = 1 << 3,
	};
	enum class PrimitiveType
	{
		none = 0,
		points = 1,
		triangles = 2,
	};
	enum class FaceType
	{
		none = 0,
		back = 1,
		front = 2,
		backAndFront = 3,
	};
	enum class WindingType
	{
		none = 0,
		cw = 1,
		ccw = 2,
	};
	enum class ShaderType
	{
		none = 0,
		vertex,
		fragment,
		compute,
		tesselationControl,
		tesselationEvaluation,
		geometry,
		mesh,
		rayGeneration,
		rayIntersection,
		rayAnyHit,
		rayClosestHit,
		rayMiss,
		rayCallable
	};

	struct Settings
	{
		std::string name = "App";
		std::string engine_name = "VulkanApp";

		uint32_t version_major = 1;
		uint32_t version_minor = 2;
		uint32_t version_patch = 0;
		uint32_t engine_version_major = 1;
		uint32_t engine_version_minor = 0;
		uint32_t engine_version_patch = 0;

		bool validation = false;
		bool anisotropicFiltering = true;
	};

	struct DescriptorData {
		DescriptorID id;
		DescriptorType type;
		DescriptorSetID set;
		uint32_t binding;
		uint32_t count;
		DataFormat format;
		uint32_t blocksize;
		BitFlags<ShaderType> shader;
	};

	struct AttributeData {
		AttributeID id;
		uint32_t location;
		DataFormat format;
	};

	struct DataFormatName
	{
		std::string name;
		DataFormat format;
	};
	extern const std::vector<DataFormatName> gc_dataFormats;
} // ns graphics

#endif // GRAPHICS_COMMON_H_INCLUDED_POPO