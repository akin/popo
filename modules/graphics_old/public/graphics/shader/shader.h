#ifndef BONA_GRAPHICS_SHADER_H_
#define BONA_GRAPHICS_SHADER_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <unordered_map>
#include <string>

namespace bngfx
{
class Context;
class Shader
{
public:
    Shader(Context& context);
    ~Shader();

    const VkShaderModule& getShader() const;
    ShaderType getType() const;
    const std::string& getEntryPoint() const;

    const std::vector<DescriptorData>& getDescriptorData() const;
    const std::vector<AttributeData>& getInputAttributeData() const;
    const std::vector<AttributeData>& getOutputAttributeData() const;

    void setDescriptor(const DescriptorData& value);
    void setInputAttribute(const AttributeData& value);
    void setOutputAttribute(const AttributeData& value);

    void setEntryPoint(const char* entrypoint);
    void setType(ShaderType type);

    bool initialize(const void* spirvData, uint32_t bytesize);
    void destroy();
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    Context& m_context;
    ShaderType m_type = ShaderType::none;
    std::string m_entryPoint = "main";
    std::string m_name;

    std::vector<DescriptorData> m_descriptors;
    std::vector<AttributeData> m_inputAttributes;
    std::vector<AttributeData> m_outputAttributes;

    VkShaderModule m_shaderModule = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_SHADER_H_