#ifndef BONA_GRAPHICS_SHADERMANAGER_H_
#define BONA_GRAPHICS_SHADERMANAGER_H_

#include <bngfx/common.h>
#include <bngfx/shader/shader.h>
#include <bngfx/shader/rendererinfo.h>
#include <bncommon/idmap.h>
#include <string>
#include <unordered_map>

namespace bngfx
{
class ShaderManager
{
public:
    ShaderManager(Context& context);
    ~ShaderManager();

    IntrusivePtr<Shader> createShader(const std::string& name);
    IntrusivePtr<RendererInfo> createRendererInfo(const std::string& name);

    IntrusivePtr<Shader> getShader(const std::string& name) const;
    IntrusivePtr<RendererInfo> getRendererInfo(const std::string& name) const;

    DescriptorID getOrCreateDescriptorIDForName(const std::string& name);
    RenderTypeID getOrCreateRenderTypeIDForName(const std::string& name);
    AttributeID getOrCreateAttributeIDForName(const std::string& name);
    DataFormat getOrCreateDataFormatForName(const std::string& name);
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    void initDefaultData();

    Context& m_context;

    std::unordered_map<std::string, IntrusivePtr<Shader>> m_shaders;
    std::unordered_map<std::string, IntrusivePtr<RendererInfo>> m_rendererInfos;

    bncommon::IDMap<std::string, DescriptorID, 1> m_descriptors;
    bncommon::IDMap<std::string, RenderTypeID, 1> m_renderTypes;
    bncommon::IDMap<std::string, AttributeID, 1> m_attributes;
    bncommon::IDMap<std::string, DataFormat, static_cast<DataFormat>(bntypes::ImageFormat::count)> m_dataFormats;
};
} // ns bngfx

#endif // BONA_GRAPHICS_SHADERMANAGER_H_