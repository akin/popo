#ifndef GRAPHICS_FRAMEDATA_H_INCLUDED_POPO
#define GRAPHICS_FRAMEDATA_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>

#include <graphics/resources/buffer.h>
#include <graphics/resources/image.h>
#include <graphics/resources/imageview.h>

#include <graphics/descriptor/descriptorset.h>
#include <graphics/framebuffer.h>

namespace graphics
{
	class Context;
	struct DeviceInfo;

	class FrameData
	{
		FrameData(Context& context)
        : framebuffer(context)
        , descriptorSet(context)
		{
		}

		VkImage image = VK_NULL_HANDLE;
		VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
		std::unique_ptr<ImageView> view;

		FrameBuffer framebuffer;
		DescriptorSet descriptorSet;
        
		IntrusivePtr<ResourceBlock> gpuBlock;
		IntrusivePtr<ResourceBlock> cpuBlock;

		Image* depthImage = nullptr;
		Buffer* ubo = nullptr;
	};
} // ns graphics

#endif // GRAPHICS_FRAMEDATA_H_INCLUDED_POPO