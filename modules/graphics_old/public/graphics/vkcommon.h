#ifndef GRAPHICS_VKCOMMON_H_INCLUDED_POPO
#define GRAPHICS_VKCOMMON_H_INCLUDED_POPO

#define VK_NO_PROTOTYPES
#define VK_USE_PLATFORM_WIN32_KHR
#include <volk.h>
#include <string>
//#include <vulkan/vulkan.hpp>

#define NO_ALLOCATOR nullptr

#ifdef WIN32
# define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#elif XLIB
# define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_XLIB_SURFACE_EXTENSION_NAME
#elif XCB
# define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_XCB_SURFACE_EXTENSION_NAME
#endif

namespace graphics 
{

struct DeviceInfo
{
    size_t id;
    uint32_t vendorID;
    uint32_t deviceID;
    std::string name;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkPhysicalDeviceFeatures features;
    VkPhysicalDeviceProperties properties;
    VkPhysicalDeviceMemoryProperties memoryProperties;
};

struct QueueInfo
{
    VkQueue queue = VK_NULL_HANDLE;
    uint32_t familyIndex = 0;
    std::vector<float> priorities{0.0f};
    bool found = false;
};

const char* G_VALIDATION_LAYER = "VK_LAYER_KHRONOS_validation";

} // ns graphics

#endif // GRAPHICS_VKCOMMON_H_INCLUDED_POPO