
#ifndef GRAPHICS_UTILS_H_INCLUDED_POPO
#define GRAPHICS_UTILS_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>
#include <graphics/imagetypes.h>
#include <common/bitflags.h>
#include <string>

namespace graphics 
{
std::string toString(VkResult result);

VkShaderStageFlagBits shaderTypeToVkShaderStageFlagBits(ShaderType type);

VkPrimitiveTopology primitiveTypeToVkPrimitiveTopology(PrimitiveType type);

VkCullModeFlagBits faceTypeToVkCullModeFlagBits(FaceType type);

VkFrontFace windingTypeToVkFrontFace(WindingType type);

VkFormat dataFormatToVkFormat(bntypes::DataFormat format);

VkImageType imageTypeToVkImageType(bntypes::ImageType type);

VkShaderStageFlagBits bitFlagsToVkShaderStageFlags(const BitFlags<ShaderType>& flags);
} // ns graphics

#endif // GRAPHICS_UTILS_H_INCLUDED_POPO