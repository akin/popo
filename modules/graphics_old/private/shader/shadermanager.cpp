#include <bngfx/shader/shaderemanager.h>

namespace bngfx
{
ShaderManager::ShaderManager(Context& context)
: m_context(context)
{
    initDefaultData();
}

ShaderManager::~ShaderManager()
{
}

IntrusivePtr<Shader> ShaderManager::createShader(const std::string& name)
{
    IntrusivePtr<Shader> ret = new Shader{m_context};
    ret->setName(name);
    m_shaders[name] = ret;
    return ret;
}

IntrusivePtr<RenderInfo> ShaderManager::createRendererInfo(const std::string& name)
{
    IntrusivePtr<RendererInfo> ret = new RendererInfo{ m_context };
    ret->setName(name);
    m_rendererInfos[name] = ret;
    return ret;
}

IntrusivePtr<Shader> ShaderManager::getShader(const std::string& name) const
{
    auto iter = m_shaders.find(name);
    if(iter != m_shaders.end())
    {
        return iter->second;
    }
    return {nullptr};
}

IntrusivePtr<RenderInfo> ShaderManager::getRendererInfo(const std::string& name) const
{
    auto iter = m_rendererInfos.find(name);
    if(iter != m_rendererInfos.end())
    {
        return iter->second;
    }
    return {nullptr};
}

DescriptorID ShaderManager::getOrCreateDescriptorIDForName(const std::string& name)
{
    return m_descriptors.getOrCreateID(name);
}

RenderTypeID ShaderManager::getOrCreateRenderTypeIDForName(const std::string& name)
{
    return m_renderTypes.getOrCreateID(name);
}

AttributeID ShaderManager::getOrCreateAttributeIDForName(const std::string& name)
{
    return m_attributes.getOrCreateID(name);
}

DataFormat ShaderManager::getOrCreateDataFormatForName(const std::string& name)
{
    return m_dataFormats.getOrCreateID(name);
}

void ShaderManager::initDefaultData()
{
    for (const DataFormatName& dfn : gc_dataFormats)
    {
        m_dataFormatTypes.set(dfn.name, dfn.format);
    }
}

void ShaderManager::incrementReferenceCount()
{
    ++m_refCount;
}

void ShaderManager::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
 