#include <bngfx/shader/shader.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Shader::Shader(Context& context)
: m_context(context)
{
}

Shader::~Shader()
{
    destroy();
}

const VkShaderModule& Shader::getShader() const
{
    return m_shaderModule;
}

ShaderType Shader::getType() const
{
    return m_type;
}

const std::string& Shader::getEntryPoint() const
{
    return m_entryPoint;
}

const std::vector<DescriptorData>& Shader::getDescriptorData() const
{
    return m_descriptors;
}

const std::vector<AttributeData>& Shader::getInputAttributeData() const
{
    return m_inputAttributes;
}

const std::vector<AttributeData>& Shader::getOutputAttributeData() const
{
    return m_outputAttributes;
}

void Shader::setDescriptor(const DescriptorData& value)
{
    m_descriptors.push_back(value);
}

void Shader::setInputAttribute(const AttributeData& value)
{
    m_inputAttributes.push_back(value);
}

void Shader::setOutputAttribute(const AttributeData& value)
{
    m_outputAttributes.push_back(value);
}

void Shader::setEntryPoint(const char* entrypoint)
{
    m_entryPoint = entrypoint;
}

void Shader::setType(ShaderType type)
{
    m_type = type;
}

bool Shader::initialize(const void* spirvData, uint32_t bytesize)
{
    if(m_shaderModule != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;

    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = bytesize;
    createInfo.pCode = static_cast<const uint32_t*>(spirvData);
    
    error = vkCreateShaderModule(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_shaderModule);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreateShaderModule '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void Shader::destroy()
{
	if (m_shaderModule != VK_NULL_HANDLE)
	{
        vkDestroyShaderModule(m_context.getDevice(), m_shaderModule, NO_ALLOCATOR);
        m_shaderModule = VK_NULL_HANDLE;
	}
}

void Shader::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Shader::getName() const
{
    return m_name;
}

void Shader::incrementReferenceCount()
{
    ++m_refCount;
}

void Shader::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
