#include <graphics/context.h>
#include <graphics/surface.h>
#include "utils.h"

#include <unordered_set>
#include <sstream>
#include <iostream>
#include <spdlog/spdlog.h>

#include <assert.h>

namespace graphics
{
	Context::Context()
	{
	}

	Context::~Context()
	{
		destroyDevice();
		destroyInstance();
	}

	const std::vector<DeviceInfo>& Context::getDevices() const
	{
		return m_devices;
	}

	void Context::setSettings(const Settings& settings)
	{
		m_settings = settings;
	}

	bool Context::init()
	{
		if (m_instance != VK_NULL_HANDLE)
		{
			spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
			assert(false);
			return false;
		}

		VkResult error;
		uint32_t count;

		error = volkInitialize();
		if (error != VK_SUCCESS)
		{
			spdlog::error("{0}:{1} Failed to initialize volk '{2}'.", __FILE__, __LINE__, toString(error));
			assert(false);
			return false;
		}

		// https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Validation_layers
		if (m_settings.validation)
		{
			m_instanceExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
			m_instanceLayers.push_back(G_VALIDATION_LAYER);
		}

		// validate instance layers
		{
			std::vector<VkLayerProperties> properties;
			error = vkEnumerateInstanceLayerProperties(&count, nullptr);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed vkEnumerateInstanceLayerProperties '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
			properties.resize(count);
			if (count > 0)
			{
				error = vkEnumerateInstanceLayerProperties(&count, properties.data());
				if (error != VK_SUCCESS)
				{
					spdlog::error("{0}:{1} Failed vkEnumerateInstanceLayerProperties '{2}'.", __FILE__, __LINE__, toString(error));
					assert(false);
					return false;
				}
			}

			// Go through layers.. find if they are available..
			{
				bool allFound = true;
				for (const char* name : m_instanceLayers)
				{
					bool found = false;
					for (const auto& property : properties)
					{
						if (strcmp(name, property.layerName) == 0)
						{
							found = true;
							break;
						}
					}

					if (!found)
					{
						allFound = false;
						spdlog::error("{0}:{1} Failed to find '{2}' instance layer.", __FILE__, __LINE__, name);
					}
				}
				if (!allFound)
				{
					assert(false && "Failed to find layer, not supported?");
					return false;
				}
			}
		}
		// validate instance extensions
		{
			std::vector<VkExtensionProperties> properties;
			error = vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed vkEnumerateInstanceExtensionProperties '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
			properties.resize(count);
			if (count > 0)
			{
				error = vkEnumerateInstanceExtensionProperties(nullptr, &count, properties.data());
				if (error != VK_SUCCESS)
				{
					spdlog::error("{0}:{1} Failed vkEnumerateInstanceExtensionProperties '{2}'.", __FILE__, __LINE__, toString(error));
					assert(false);
					return false;
				}
			}

			{
				bool allFound = true;
				for (const char* name : m_instanceExtensions)
				{
					bool found = false;
					for (const auto& property : properties)
					{
						if (strcmp(name, property.extensionName) == 0)
						{
							found = true;
							break;
						}
					}

					if (!found)
					{
						allFound = false;
						spdlog::error("{0}:{1} Failed to find '{2}' instance extension.", __FILE__, __LINE__, name);
					}
				}
				if (!allFound)
				{
					assert(false && "Failed to find extension, not supported?");
					return false;
				}
			}
		}

		VkApplicationInfo appInfo;
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pNext = nullptr;
		appInfo.pApplicationName = m_settings.name.c_str();
		appInfo.applicationVersion = VK_MAKE_VERSION(m_settings.version_major, m_settings.version_minor, m_settings.version_patch);
		appInfo.pEngineName = m_settings.engine_name.c_str();
		appInfo.engineVersion = VK_MAKE_VERSION(m_settings.engine_version_major, m_settings.engine_version_minor, m_settings.engine_version_patch);
		appInfo.apiVersion = VK_API_VERSION_1_1;

		VkInstanceCreateInfo instInfo;
		instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instInfo.pNext = nullptr;
		instInfo.flags = 0;
		instInfo.pApplicationInfo = &appInfo;
		instInfo.enabledExtensionCount = static_cast<uint32_t>(m_instanceExtensions.size());
		instInfo.ppEnabledExtensionNames = m_instanceExtensions.data();
		instInfo.enabledLayerCount = static_cast<uint32_t>(m_instanceLayers.size());
		instInfo.ppEnabledLayerNames = m_instanceLayers.data();

		error = vkCreateInstance(&instInfo, NO_ALLOCATOR, &m_instance);
		if (error != VK_SUCCESS)
		{
			spdlog::error("{0}:{1} Failed vkCreateInstance '{2}'.", __FILE__, __LINE__, toString(error));
			assert(false);
			return false;
		}

		volkLoadInstance(m_instance);

		// Setup validation
		if (m_settings.validation)
		{
			VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
			createInfo.messageSeverity =
				//VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
				VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
				VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
				VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
				0;

			createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			createInfo.pUserData = this; // Optional
			createInfo.pfnUserCallback = [](
				VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
				VkDebugUtilsMessageTypeFlagsEXT messageType,
				const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
				void* pUserData) -> VKAPI_ATTR VkBool32 VKAPI_CALL {
					Context* context = static_cast<Context*>(pUserData);

					std::ostringstream stream;
					if (pCallbackData->flags & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)
					{
						stream << "[general]";
					}
					if (pCallbackData->flags & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
					{
						stream << "[validation]";
					}
					if (pCallbackData->flags & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
					{
						stream << "[performance]";
					}
					spdlog::error("{0}:{1} {2} ({3}) -> {4}.", __FILE__, __LINE__, stream.str(), pCallbackData->pMessageIdName, pCallbackData->pMessage);

					return VK_FALSE;
			};

			error = vkCreateDebugUtilsMessengerEXT(m_instance, &createInfo, NO_ALLOCATOR, &m_debugUtil);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed vkCreateDebugUtilsMessengerEXT '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
		}

		// Query devices right away
		error = vkEnumeratePhysicalDevices(m_instance, &count, nullptr);
		if (error != VK_SUCCESS)
		{
			spdlog::error("{0}:{1} Failed vkEnumeratePhysicalDevices '{2}'.", __FILE__, __LINE__, toString(error));
			assert(false);
			return false;
		}

		std::vector<VkPhysicalDevice> queriedDevices;
		queriedDevices.resize(count);
		error = vkEnumeratePhysicalDevices(m_instance, &count, queriedDevices.data());
		if (error != VK_SUCCESS)
		{
			spdlog::error("{0}:{1} Failed vkEnumeratePhysicalDevices '{2}'.", __FILE__, __LINE__, toString(error));
			assert(false);
			return false;
		}

		// Populate device list with _proper_ data.
		m_devices.resize(queriedDevices.size());
		for (size_t i = 0; i < queriedDevices.size(); ++i)
		{
			auto& target = m_devices[i];
			target.physicalDevice = queriedDevices[i];

			target.features = {};
			vkGetPhysicalDeviceFeatures(target.physicalDevice, &target.features);
			vkGetPhysicalDeviceProperties(target.physicalDevice, &target.properties);
			vkGetPhysicalDeviceMemoryProperties(target.physicalDevice, &target.memoryProperties);

			target.id = i;
			target.vendorID = target.properties.vendorID;
			target.deviceID = target.properties.deviceID;
			target.name = target.properties.deviceName;
		}

		return true;
	}

	void Context::destroy()
	{
		if (m_instance != VK_NULL_HANDLE)
		{
			if (m_settings.validation)
			{
				vkDestroyDebugUtilsMessengerEXT(m_instance, m_debugUtil, NO_ALLOCATOR);
			}

			vkDestroyInstance(m_instance, NO_ALLOCATOR);
			m_instance = VK_NULL_HANDLE;
		}
	}

	void Context::addExtension(const char* extension)
	{
		// is extension already in?
		for (size_t i = 0; i < m_instanceExtensions.size(); ++i)
		{
			if (strcmp(m_instanceExtensions[i], extension) == 0)
			{
				return;
			}
		}
		m_instanceExtensions.push_back(extension);
	}

	const VkInstance& Context::getInstance() const
	{
		return m_instance;
	}
} // ns bngfx
