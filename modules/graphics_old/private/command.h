
#ifndef GRAPHICS_COMMAND_H_INCLUDED_POPO
#define GRAPHICS_COMMAND_H_INCLUDED_POPO

#include <graphics/common.h>
#include <graphics/vkcommon.h>
#include <graphics/resource/image.h>

namespace graphics
{
	class Command
	{
	public:
		Command(const VkDevice& device, const VkCommandPool& pool, const VkQueue& queue);

		VkResult beginOneTimeCommand(VkCommandBuffer& commandBuffer);
		VkResult endOneTimeCommand(VkCommandBuffer& commandBuffer);

		void transitionImageLayout(VkCommandBuffer& commandBuffer, Image* image, VkImageLayout oldLayout, VkImageLayout newLayout);
	private:
		const VkDevice& m_device;
		const VkCommandPool& m_pool;
		const VkQueue& m_queue;
	};
} // ns graphics

#endif // GRAPHICS_COMMAND_H_INCLUDED_POPO