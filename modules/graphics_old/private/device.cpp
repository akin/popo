#include <graphics/device.h>
#include <graphics/surface.h>
#include "utils.h"

#include <unordered_set>
#include <sstream>
#include <iostream>
#include <spdlog/spdlog.h>

#include <assert.h>

namespace graphics
{
	Device::Device(Context& context)
    : m_context(context)
	{
	}

	Device::~Device()
	{
		destroy();
	}

	bool Device::init(const DeviceInfo& info, const Surface& surface)
	{
		if (m_device != VK_NULL_HANDLE)
		{
			spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
			assert(false);
			return false;
		}

		VkResult error;
		uint32_t count;

		m_deviceInfo = info;

		// Lets set some features on
		// Apparently not _all_ features are put on by extensions and shenanigans, we actually need to modify the VkPhysicalDeviceFeatures structure for this :D
		{
			m_deviceInfo.features.samplerAnisotropy = m_settings.anisotropicFiltering ? VK_TRUE : VK_FALSE;
		}

		if (m_settings.validation)
		{
			m_deviceLayers.push_back(G_VALIDATION_LAYER);
		}

		// validate device layers
		{
			std::vector<VkLayerProperties> properties;
			error = vkEnumerateDeviceLayerProperties(m_deviceInfo.physicalDevice, &count, nullptr);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed vkEnumerateDeviceLayerProperties '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
			properties.resize(count);
			if (count > 0)
			{
				error = vkEnumerateDeviceLayerProperties(m_deviceInfo.physicalDevice, &count, properties.data());
				if (error != VK_SUCCESS)
				{
					spdlog::error("{0}:{1} Failed vkEnumerateDeviceLayerProperties '{2}'.", __FILE__, __LINE__, toString(error));
					assert(false);
					return false;
				}
			}

			// Go through layers.. find if they are available..
			{
				bool allFound = true;
				for (const char* name : m_deviceLayers)
				{
					bool found = false;
					for (const auto& property : properties)
					{
						if (strcmp(name, property.layerName) == 0)
						{
							found = true;
							break;
						}
					}

					if (!found)
					{
						allFound = false;
						spdlog::error("{0}:{1} Failed to find '{2}' device layer.", __FILE__, __LINE__, name);
					}
				}
				if (!allFound)
				{
					assert(false && "Failed to find layer, not supported?");
					return false;
				}
			}
		}
		// validate device extensions
		{
			std::vector<VkExtensionProperties> properties;
			error = vkEnumerateDeviceExtensionProperties(m_deviceInfo.physicalDevice, nullptr, &count, nullptr);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed VkExtensionProperties '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
			properties.resize(count);
			if (count > 0)
			{
				error = vkEnumerateDeviceExtensionProperties(m_deviceInfo.physicalDevice, nullptr, &count, properties.data());
				if (error != VK_SUCCESS)
				{
					spdlog::error("{0}:{1} Failed vkEnumerateDeviceExtensionProperties '{2}'.", __FILE__, __LINE__, toString(error));
					assert(false);
					return false;
				}
			}

			{
				bool allFound = true;
				for (const char* name : m_deviceExtensions)
				{
					bool found = false;
					for (const auto& property : properties)
					{
						if (strcmp(name, property.extensionName) == 0)
						{
							found = true;
							break;
						}
					}

					if (!found)
					{
						allFound = false;
						spdlog::error("{0}:{1} Failed to find '{2}' device extension.", __FILE__, __LINE__, name);
					}
				}
				if (!allFound)
				{
					assert(false && "Failed to find extension, not supported?");
					return false;
				}
			}
		}

		// Create queue infos
		std::vector<VkDeviceQueueCreateInfo> queueInfos;
		{
			// parse queues
			std::vector<VkQueueFamilyProperties> properties;
			{
				vkGetPhysicalDeviceQueueFamilyProperties(m_deviceInfo.physicalDevice, &count, nullptr);
				properties.resize(count);
				vkGetPhysicalDeviceQueueFamilyProperties(m_deviceInfo.physicalDevice, &count, properties.data());

				std::unordered_set<uint32_t> familyIndex;

				// Search present
				for (uint32_t i = 0; i < count; ++i)
				{
					VkBool32 presentSupport = false;
					vkGetPhysicalDeviceSurfaceSupportKHR(m_deviceInfo.physicalDevice, i, surface.getSurface(), &presentSupport);

					if (presentSupport)
					{
						m_present.familyIndex = i;
						m_present.found = true;
						break;
					}
				}

				// Search graphics
				for (uint32_t i = 0; i < properties.size(); ++i)
				{
					auto& property = properties[i];

					if (property.queueFlags & VK_QUEUE_GRAPHICS_BIT)
					{
						m_graphics.familyIndex = i;
						m_graphics.found = true;
						break;
					}
				}

				// Search compute
				for (uint32_t i = 0; i < properties.size(); ++i)
				{
					auto& property = properties[i];

					if (property.queueFlags & VK_QUEUE_COMPUTE_BIT)
					{
						m_compute.familyIndex = i;
						m_compute.found = true;
						break;
					}
				}

				// Search transfer
				for (uint32_t i = 0; i < properties.size(); ++i)
				{
					auto& property = properties[i];

					if (property.queueFlags & VK_QUEUE_TRANSFER_BIT)
					{
						m_transfer.familyIndex = i;
						m_transfer.found = true;
						break;
					}
				}

				std::vector<QueueInfo*> infos;
				infos.push_back(&m_graphics);
				infos.push_back(&m_compute);
				infos.push_back(&m_transfer);
				infos.push_back(&m_present);

				std::unordered_set<uint32_t> familyIndexes;
				VkDeviceQueueCreateInfo deviceQueueCreateInfo;
				deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
				deviceQueueCreateInfo.pNext = nullptr;
				for (auto* info : infos)
				{
					// Skip already existing queue requests.
					if (familyIndexes.find(info->familyIndex) != familyIndexes.end())
					{
						continue;
					}

					deviceQueueCreateInfo.flags = 0;
					deviceQueueCreateInfo.queueCount = static_cast<uint32_t>(info->priorities.size());
					deviceQueueCreateInfo.pQueuePriorities = info->priorities.data();
					deviceQueueCreateInfo.queueFamilyIndex = info->familyIndex;
					queueInfos.push_back(deviceQueueCreateInfo);

					familyIndexes.insert(info->familyIndex);
				}
			}
		}

		VkDeviceCreateInfo deviceCreateInfo;
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.pNext = nullptr;
		deviceCreateInfo.flags = 0;
		deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueInfos.size());
		deviceCreateInfo.pQueueCreateInfos = queueInfos.data();
		deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(m_deviceLayers.size());
		deviceCreateInfo.ppEnabledLayerNames = m_deviceLayers.data();
		deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(m_deviceExtensions.size());
		deviceCreateInfo.ppEnabledExtensionNames = m_deviceExtensions.data();
		deviceCreateInfo.pEnabledFeatures = &info.features;

		error = vkCreateDevice(m_deviceInfo.physicalDevice, &deviceCreateInfo, NO_ALLOCATOR, &m_device);
		if (error != VK_SUCCESS)
		{
			spdlog::error("{0}:{1} Failed vkCreateDevice '{2}'.", __FILE__, __LINE__, toString(error));
			assert(false);
			return false;
		}

		// Fetch queues
		{
			// family index & queu index (there can be multiple queues in this family).
			vkGetDeviceQueue(m_device, m_present.familyIndex, 0, &m_present.queue);
			vkGetDeviceQueue(m_device, m_graphics.familyIndex, 0, &m_graphics.queue);
			vkGetDeviceQueue(m_device, m_compute.familyIndex, 0, &m_compute.queue);
			vkGetDeviceQueue(m_device, m_transfer.familyIndex, 0, &m_transfer.queue);
		}

		return true;
	}

	void Device::destroy()
	{
		if (m_device != VK_NULL_HANDLE)
		{
			vkDestroyDevice(m_device, NO_ALLOCATOR);
			m_device = VK_NULL_HANDLE;
		}
	}

	void Device::addExtension(const char* extension)
	{
		// is extension already in?
		for (size_t i = 0; i < m_deviceExtensions.size(); ++i)
		{
			if (strcmp(m_deviceExtensions[i], extension) == 0)
			{
				return;
			}
		}
		m_deviceExtensions.push_back(extension);
	}

	const VkDevice& Device::getDevice() const
	{
		return m_device;
	}

	const VkPhysicalDevice& Device::getPhysicalDevice() const
	{
		return m_deviceInfo.physicalDevice;
	}

	const DeviceInfo& Device::getDeviceInfo() const
	{
		return m_deviceInfo;
	}

	VkFormatFeatureFlags Device::getFormatFlags(VkFormat format)
	{
		if (m_deviceInfo.physicalDevice == VK_NULL_HANDLE)
		{
			spdlog::error("{0}:{1} PhysicalDevice is set to NULL. This indicates context is not initialized properly. (format: {2})", __FILE__, __LINE__, format);
			assert(false);
			return 0;
		}

		VkFormatProperties properties;
		vkGetPhysicalDeviceFormatProperties(m_deviceInfo.physicalDevice, format, &properties);
		return properties.optimalTilingFeatures;
	}

	bool Device::flush()
	{
		VkResult error;
		if (m_device != VK_NULL_HANDLE)
		{
			error = vkQueueWaitIdle(m_present.queue);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed vkQueueWaitIdle '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
		}
		return true;
	}

	bool Device::finish()
	{
		VkResult error;
		if (m_device != VK_NULL_HANDLE)
		{
			error = vkDeviceWaitIdle(m_device);
			if (error != VK_SUCCESS)
			{
				spdlog::error("{0}:{1} Failed vkDeviceWaitIdle '{2}'.", __FILE__, __LINE__, toString(error));
				assert(false);
				return false;
			}
		}
		return true;
	}

} // ns bngfx
