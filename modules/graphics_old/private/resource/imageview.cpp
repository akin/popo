#include <graphics/resource/image.h>
#include <graphics/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace graphics
{
ImageView::ImageView(Context& context)
: m_context(context)
{
}

ImageView::~ImageView()
{
    destroy();
}

void ImageView::setAspectMask(VkImageAspectFlags value)
{
    m_aspectFlags = value;
}

void ImageView::setMipBaseLevel(uint32_t value)
{
    m_mipBaseLevel = value;
}

void ImageView::setMipCount(uint32_t value)
{
    m_mipCount = value;
}

void ImageView::setArrayBaseLevel(uint32_t value)
{
    m_arrayBaseLevel = value;
}

void ImageView::setArrayCount(uint32_t value)
{
    m_arrayCount = value;
}

const VkImageView& ImageView::getView() const
{
    return m_view;
}

bool ImageView::initialize(VkImage image, VkImageViewType type, VkFormat format)
{
    if(m_view != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;

    VkImageViewCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    createInfo.image = image;
    createInfo.viewType = type;
    createInfo.format = format;
    createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.subresourceRange.aspectMask = m_aspectFlags;
    createInfo.subresourceRange.baseMipLevel = m_mipBaseLevel;
    createInfo.subresourceRange.levelCount = m_mipCount;
    createInfo.subresourceRange.baseArrayLayer = m_arrayBaseLevel;
    createInfo.subresourceRange.layerCount = m_arrayCount;
    
    error = vkCreateImageView(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_view);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreateImageView '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void ImageView::destroy()
{
    assert(m_refCount == 0 && "On destroy the imageview reference count should be 0, if it is not, there is a logic problem!");
	if (m_view != VK_NULL_HANDLE)
	{
		vkDestroyImageView(m_context.getDevice(), m_view, NO_ALLOCATOR);
		m_view = VK_NULL_HANDLE;
	}
}

void ImageView::incrementReferenceCount()
{
    ++m_refCount;
}

void ImageView::decrementReferenceCount()
{
    --m_refCount;
}

} // ns graphics
