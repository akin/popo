#include <bngfx/resource/resourceblock.h>
#include <bngfx/resource/image.h>
#include <bngfx/resource/buffer.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
ResourceBlock::ResourceBlock(Context& context)
: m_context(context)
{
}

ResourceBlock::~ResourceBlock()
{
    destroy();
}

Context& ResourceBlock::getContext() const
{
    return m_context;
}

const VkDeviceMemory& ResourceBlock::getMemory() const
{
    return m_memory;
}

void ResourceBlock::setMemoryPropertyFlags(VkMemoryPropertyFlags value)
{
    m_memoryPropertyFlags = value;
}

IntrusivePtr<Image> ResourceBlock::createImage()
{
    IntrusivePtr<Image> value = new Image(*this);
    m_images.push_back(value);
    return value;
}

IntrusivePtr<Buffer> ResourceBlock::createBuffer()
{
    IntrusivePtr<Buffer> value = new Buffer(*this);
    m_buffers.push_back(value);
    return value;
}

bool ResourceBlock::allocate()
{
    if (m_memory != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    VkResult error;
    uint64_t requiredMemorySize = 0;
    uint32_t memoryType = UINT32_MAX;
    uint64_t currentOffset = 0;

    // Initialize images
    bool success = true;
    for(auto item : m_images)
    {
        if(!item->initialize())
        {
            spdlog::error("{0}:{1} Failed to initialize image '{2}'.", __FILE__, __LINE__, item->getName());
            success = false;
            continue;
        }

        VkMemoryRequirements requirement{};
        vkGetImageMemoryRequirements(m_context.getDevice(), item->getImage(), &requirement);

        memoryType &= requirement.memoryTypeBits;

        // Alignment schenanigans and sizes.
        uint64_t missalignment = currentOffset % requirement.alignment;
        uint64_t alignPadding = 0;
        if (missalignment != 0)
        {
            alignPadding = requirement.alignment - missalignment;
        }
        uint64_t itemTotalSize = requirement.size + alignPadding;
        item->setAllocatedByteOffset(currentOffset + alignPadding);
        item->setAllocatedByteSize(itemTotalSize);

        requiredMemorySize += itemTotalSize;
        currentOffset += itemTotalSize;
    }

    // Initialize buffers
    for (auto item : m_buffers)
    {
        if (!item->initialize())
        {
            spdlog::error("{0}:{1} Failed to initialize buffer '{2}'.", __FILE__, __LINE__, item->getName());
            success = false;
            continue;
        }

        VkMemoryRequirements requirement{};
        vkGetBufferMemoryRequirements(m_context.getDevice(), item->getBuffer(), &requirement);

        memoryType &= requirement.memoryTypeBits;

        // Alignment schenanigans and sizes.
        uint64_t missalignment = currentOffset % requirement.alignment;
        uint64_t alignPadding = 0;
        if (missalignment != 0)
        {
            alignPadding = requirement.alignment - missalignment;
        }
        uint64_t itemTotalSize = requirement.size + alignPadding;
        item->setAllocatedByteOffset(currentOffset + alignPadding);
        item->setAllocatedByteSize(itemTotalSize);

        requiredMemorySize += itemTotalSize;
        currentOffset += itemTotalSize;
    }

    if(!success)
    {
        spdlog::error("{0}:{1} Failed to initialize resourceblock.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    if (memoryType == 0)
    {
        spdlog::error("{0}:{1} Failed to initialize resourceblock. No memory available.", __FILE__, __LINE__);
        assert(false);
        return false;
    }


    auto memoryProperties = m_context.getDeviceInfo().memoryProperties;

    // Allocate memory block
    VkMemoryAllocateInfo allocateInfo = {};
    allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocateInfo.allocationSize = requiredMemorySize;
    m_byteSize = requiredMemorySize;

    success = false;
    for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
    {
        // if the index is compatible with data requests.
        if (memoryType & (1 << i))
        {
            // Extra requirements laid out by the ResourceBlock:
            if ((memoryProperties.memoryTypes[i].propertyFlags & m_memoryPropertyFlags) == m_memoryPropertyFlags)
            {
                allocateInfo.memoryTypeIndex = i;
                success = true;
            }
        }
    }

    if (!success)
    {
        spdlog::error("{0}:{1} Failed to initialize resourceblock. Could not initialze.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    error = vkAllocateMemory(m_context.getDevice(), &allocateInfo, NO_ALLOCATOR, &m_memory);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkAllocateMemory '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    // Assign images memory
    for (auto item : m_images)
    {
        error = vkBindImageMemory(m_context.getDevice(), item->getImage(), m_memory, item->getAllocatedByteOffset());
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkBindImageMemory ('{3}') '{2}'.", __FILE__, __LINE__, toString(error), item->getName());
            assert(false);
            return false;
        }
        if (!item->postInitialize())
        {
            spdlog::error("{0}:{1} Failed to initialize image '{2}'.", __FILE__, __LINE__, item->getName());
            success = false;
            continue;
        }
    }

    // Assign buffers memory
    for (auto item : m_buffers)
    {
        error = vkBindBufferMemory(m_context.getDevice(), item->getBuffer(), m_memory, item->getAllocatedByteOffset());
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkBindBufferMemory ('{3}') '{2}'.", __FILE__, __LINE__, toString(error), item->getName());
            assert(false);
            return false;
        }
        if (!item->postInitialize())
        {
            spdlog::error("{0}:{1} Failed to initialize buffer '{2}'.", __FILE__, __LINE__, item->getName());
            success = false;
            continue;
        }
    }

    if (!success)
    {
        spdlog::error("{0}:{1} Failed to initialize resourceblock. Could not postInitialize.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    return true;
}

void ResourceBlock::destroy()
{
    // Destroy buffers
    m_buffers.clear();

    // Destroy images
    m_images.clear();

    // Destroy memory block
    if (m_memory != VK_NULL_HANDLE)
    {
        vkFreeMemory(m_context.getDevice(), m_memory, NO_ALLOCATOR);
        m_memory = VK_NULL_HANDLE;
    }
}

void ResourceBlock::incrementReferenceCount()
{
    ++m_refCount;
}

void ResourceBlock::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
