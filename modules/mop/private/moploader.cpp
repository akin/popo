#include <loader/moploader.h>

#include <spdlog/spdlog.h>

#include <fstream>
#include <assert.h>
#include <iostream>
#include <istream>
#include <functional>

#include <flatbuffers/flatbuffers.h>
#include "mop/mesh_generated.h"
#include "mop/material_generated.h"

namespace {
	graphics::AttributeType getAttributeTypeFromString(const std::string& str)
	{
		static const std::unordered_map<std::string, graphics::AttributeType> data{
			{"position", graphics::AttributeType::Position},
			{"normal", graphics::AttributeType::Normal},
			{"tangent", graphics::AttributeType::Tangent},
			{"texture_coordinate", graphics::AttributeType::TextureCoordinate},
			{"color", graphics::AttributeType::Color},
			{"bone_joint", graphics::AttributeType::BoneIndex},
			{"bone_weight", graphics::AttributeType::BoneWeight},
		};

		auto iter = data.find(str);
		if (iter == data.end())
		{
			return graphics::AttributeType::None;
		}
		return iter->second;
	}

	size_t getByteSize(const std::string str)
	{
		const size_t len = str.size();
		if (len > 3)
		{
			if (strncmp(str.data(), "vec", 3) == 0)
			{
				// vec2/3/4_uint8/float32/float8
				// vec????
				int num = str[4] - '0';
				size_t size = getByteSize(str.substr(5));
				return size * num;
			}
			else if (strncmp(str.data(), "mat", 3) == 0)
			{
				// mat2/3/4_uint8/float32/float8
				// mat????
				int num = str[4] - '0';
				size_t size = getByteSize(str.substr(5));
				return size * (num * num);
			}

			static std::unordered_map<std::string, size_t> mapping = {
				{"uint", 4},
				{"sint", 4},
				{"floa", 5},
				{"unor", 5},
				{"snor", 5},
			};
			auto iter = mapping.find(str.substr(0, 4));
			if (iter != mapping.end())
			{
				int number = std::stoi(str.substr(iter->second));
				return number / 8;
			}
		}
		return 0;
	}

	size_t getByteSize(const flatbuffers::String* value)
	{
		if (value == nullptr)
		{
			return 0;
		}
		return getByteSize(value->str());
	}

	glm::vec3 convert(const Mop::Vec3* value)
	{
		if (value == nullptr)
		{
			return glm::vec3{ 0.0f };
		}
		return {
			value->x(),
			value->y(),
			value->z()
		};
	}

	std::string convert(const flatbuffers::String* value)
	{
		if (value == nullptr)
		{
			return "";
		}
		return value->str();
	}
}

#include <fs/handle.h>
namespace mop
{
	std::shared_ptr<graphics::Mesh> loadMesh(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device, filesystem::Handle& file)
	{
		if (!file.isFile())
		{
			spdlog::error("File {0} -> is not a file!", file.getPath());
			return {};
		}


		if (file.getBytesize() == 0)
		{
			spdlog::error("File {0} -> Empty file!", file.getPath());
			return {};
		}

		void* data = file.map();
		if (data == nullptr)
		{
			spdlog::error("File {0} -> Error opening file!", file.getPath());
			return {};
		}
		const Mop::Mesh* mesh = Mop::GetMesh(data);
		if (mesh == nullptr)
		{
			spdlog::error("File {0} -> Failed to read mesh!", file.getPath());
			return {};
		}

		auto layers = mesh->layer();
		if (layers == nullptr)
		{
			spdlog::error("File {0} -> Mesh contains no layers!", file.getPath());
			return {};
		}

		auto parent = file.getParentHandle();
		std::unordered_map<std::string, Diligent::RefCntAutoPtr<Diligent::IBuffer>> buffers;
		auto loadBuffer = [&buffers, &parent, &device](const flatbuffers::String* path, Diligent::BIND_FLAGS flags) -> Diligent::RefCntAutoPtr<Diligent::IBuffer> {
			if (path == nullptr)
			{
				return {};
			}

			std::unique_ptr<filesystem::Handle> file = parent->getHandle(path->str());
			if (!file || !file->isFile())
			{
				spdlog::error("File {0} -> Provided filehandle is not a file!", file->getPath());
				return {};
			}

			auto iter = buffers.find(file->getPath());
			if (iter != buffers.end())
			{
				return iter->second;
			}

			// Have to load
			void* data = file->map();
			size_t size = file->getBytesize();

			std::string name = file->getPath();
			Diligent::BufferDesc desc;
			desc.Name = name.c_str();
			desc.uiSizeInBytes = static_cast<uint32_t>(size);
			desc.Usage = Diligent::USAGE::USAGE_IMMUTABLE;
			desc.BindFlags = flags;

			Diligent::BufferData info;
			info.pData = data;
			info.DataSize = static_cast<uint32_t>(size);

			Diligent::RefCntAutoPtr<Diligent::IBuffer> buffer;
			device->CreateBuffer(desc, &info, &buffer);

			buffers[name] = buffer;
			return buffer;
		};

		auto result = std::make_shared<graphics::Mesh>();

		result->setName(convert(mesh->name()));
		result->radius = mesh->radius();
		result->pivot = convert(mesh->pivot());
		result->scale = mesh->scale();

		assert(layers->size() == 1 && "only supporting one layer!");
		for (size_t layerIndex = 0; layerIndex < layers->size(); ++layerIndex)
		{
			auto layer = (*layers)[layerIndex];

			// Attributes
			auto attributes = layer->attribute();
			if (attributes != nullptr)
			{
				for (size_t attrIndex = 0; attrIndex < attributes->size(); ++attrIndex)
				{
					auto attribute = (*attributes)[attrIndex];

					auto format = attribute->format();
					auto index = attribute->index();
					auto bufferview = attribute->view();

					graphics::AttributeInfo info;
					info.buffer = loadBuffer(bufferview->path(), Diligent::BIND_FLAGS::BIND_VERTEX_BUFFER);

					info.type = getAttributeTypeFromString(attribute->semantic()->str());
					if (info.type == graphics::AttributeType::None)
					{
						spdlog::error("File {0} -> Unknown attribute type! {1}", file.getPath(), attribute->semantic()->str());
						continue;
					}

					result->setAttribute(info);
				}
			}

			// Indices
			size_t bytesPerIndex = getByteSize(layer->indices_type());
			size_t indicesCount = layer->indices_count();
			auto indices = layer->indices();
			if (indices != nullptr)
			{
				graphics::IndicesInfo info;
				switch (bytesPerIndex)
				{
				case 1:
					info.type = Diligent::VT_UINT8;
					break;
				case 2:
					info.type = Diligent::VT_UINT16;
					break;
				case 4:
					info.type = Diligent::VT_UINT32;
					break;
				default:
					spdlog::error("Unknown index type! %d", bytesPerIndex);
					return {};
				}

				info.count = static_cast<uint32_t>(indicesCount);
				info.buffer = loadBuffer(indices->path(), Diligent::BIND_FLAGS::BIND_INDEX_BUFFER);
				result->setIndices(info);
			}
		}

		return result;
	}

	std::shared_ptr<graphics::Material> loadMaterial(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device, const graphics::EffectPipeline& pipeline, filesystem::Handle& file)
	{
		if (!file.isFile())
		{
			spdlog::error("File {0} -> is not a file!", file.getPath());
			return {};
		}

		if (file.getBytesize() == 0)
		{
			spdlog::error("File {0} -> Empty file!", file.getPath());
			return {};
		}

		void* data = file.map();
		if (data == nullptr)
		{
			spdlog::error("File {0} -> Error opening file!", file.getPath());
			return {};
		}
		const Mop::Material* material = Mop::GetMaterial(data);
		if (material == nullptr)
		{
			spdlog::error("File {0} -> Failed to read material!", file.getPath());
			return {};
		}

		std::vector<graphics::RenderPassID> passes = pipeline.getRenderPasses();

		auto result = std::make_shared<graphics::Material>();

		result->setName(convert(material->name()));

		// go through render passes
		for (graphics::RenderPassID pass : passes)
		{
			std::shared_ptr<graphics::Effect> effect = pipeline.getEffectForPass(pass);
			graphics::MaterialRenderPassValue passValues;
			passValues.pass = pass;
			passValues.effect = effect;

			// Populate material values for the effect.

		}

		return result;
	}
} // ns mop
