#ifndef MOP_LOADER_H_
#define MOP_LOADER_H_

#include <graphics/common.h>
#include <graphics/mesh.h>
#include <graphics/material.h>
#include <graphics/effectpipeline.h>
#include <fs/handle.h>

namespace mop
{
	std::shared_ptr<graphics::Mesh> loadMesh(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device, filesystem::Handle& file);
	std::shared_ptr<graphics::Material> loadMaterial(Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device, const graphics::EffectPipeline& pipeline, filesystem::Handle& file);
} // ns mop

#endif // MOP_LOADER_H_