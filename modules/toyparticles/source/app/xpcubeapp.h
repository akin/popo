#ifndef XP_CUBE_APP_H_
#define XP_CUBE_APP_H_

#include "xpapp.h"
#include <platform/window.h>
#include <platform/system.h>

#include <graphics/node.h>
#include <nodes/outputnode.h>
#include <nodes/basicnode.h>
#include <effects/wireeffect.h>

#include <graphics/renderobject.h>
#include <graphics/renderdata.h>
#include <graphics/mesh.h>
#include <graphics/utils/meshgenerator.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <ecs/context.h>
#include <ecs/transformsystem.h>
#include <ecs/rendersystem.h>
#include <ecs/camerasystem.h>
#include <ecs/inputsystem.h>
#include <ecs/cullingsystem.h>

#include <fs/disk.h>
#include <fs/handle.h>

#include <loader/config.h>
#include <loader/moploader.h>

#include <input/action/application.h>
#include <input/semantic.h>
#include <input/keymap.h>

#include <chrono>

namespace xp
{
	template <class CType>
	using Handler = std::function<void(const CType&)>;
	using ApplicationHandler = Handler<input::ApplicationEvent>;

	class CubeApp final : public xp::App, input::Listener<input::ApplicationEvent>
	{
	public:
		CubeApp(platform::System& system, platform::Window& window)
			: m_system(system)
			, m_window(window)
		{
			m_system.application.addListener(this);
		}

		~CubeApp() final = default;

		void setupFirstPersonCamera(ecs::EntityID entity)
		{
			engine::InputSystem* input = m_ecs.getSystem<engine::InputSystem>();

			float speed = 0.5f;
			input->addInputAction(entity, "move", [this, entity, speed](const engine::InputState& state) {
				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);
				auto dir = transform.getDirection();
				glm::vec3 direction{ 0.0f };

				if (state.getKey(input::Semantic::forward) == input::KeyState::down)
				{
					direction += dir.forward;
				}
				if (state.getKey(input::Semantic::backward) == input::KeyState::down)
				{
					direction -= dir.forward;
				}
				if (state.getKey(input::Semantic::right_strafe) == input::KeyState::down)
				{
					direction += dir.right;
				}
				if (state.getKey(input::Semantic::left_strafe) == input::KeyState::down)
				{
					direction -= dir.right;
				}

				if (direction.x != 0.0f || direction.y != 0.0f || direction.z != 0.0f)
				{
					transform.move(glm::normalize(direction) * speed);
				}
				});

			glm::vec2 resolution{ m_window.getWidth(), m_window.getHeight() };
			input->addInputAction(entity, "mouse look", [this, entity, speed, resolution](const engine::InputState& state) {
				if (state.getKey(input::Semantic::mouse3) == input::KeyState::down)
				{
					engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);
					auto dir = transform.getDirection();
					auto mouse = state.getMousePosition();

					glm::vec2 normalizedDelta{ (float)mouse.delta.x / resolution.x, (float)-mouse.delta.y / resolution.y };
					glm::vec3 delta = (dir.right * normalizedDelta.x) + (dir.up * normalizedDelta.y);
					glm::vec3 direction = dir.forward + delta;

					transform.lookAtDirection(glm::normalize(direction));
				}
				});

			input->addInputAction(entity, "quitter", [this](const engine::InputState& state) {
				if (state.getKey(input::Semantic::quit) == input::KeyState::down)
				{
					m_running = false;
				}
				});

			input->addInputAction(entity, "rayshooter", [this, entity, resolution](const engine::InputState& state) {
				if (state.getKey(input::Semantic::mouse1) == input::KeyState::down)
				{
					engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);
					graphics::RenderView& view = m_ecs.getSystem<engine::CameraSystem>()->getOrCreate(entity);

					engine::CullingSystem* csystem = m_ecs.getSystem<engine::CullingSystem>();
					auto dir = transform.getDirection();
					auto mouse = state.getMousePosition();

					glm::vec3 position{ 0.0f };
					glm::vec3 direction{ 0.0f };
					unProject(view.projection, transform.world, mouse.position, resolution, position, direction);

					ecs::EntityID hitID = csystem->rayQuery(position, direction);
					if (hitID != ecs::invalidEntity)
					{
						auto& trans = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(hitID);

						trans.move(glm::vec3{ 0.0f, 0.1f, 0.0f });
					}
				}
				});
		}

		void unProject(const glm::mat4& projection, const glm::mat4& model, const glm::vec2 inPosition, const glm::vec2 resolution, glm::vec3& position, glm::vec3& direction)
		{
			const glm::vec2 screenCubePosition{ ((float)inPosition.x / (resolution.x * 0.5f)) - 1.0f, ((float)-inPosition.y / (resolution.y * 0.5f)) + 1.0f };

			auto inverseProjection = glm::inverse(projection);
			glm::vec4 screenspaceWorldPosition = inverseProjection * glm::vec4(screenCubePosition.x, screenCubePosition.y, 0.0f, 1.0f);
			screenspaceWorldPosition /= screenspaceWorldPosition.w;
			position = glm::vec3(model * screenspaceWorldPosition);

			screenspaceWorldPosition = inverseProjection * glm::vec4(screenCubePosition.x, screenCubePosition.y, -1.0f, 1.0f);
			screenspaceWorldPosition /= screenspaceWorldPosition.w;
			glm::vec3 position2 = glm::vec3(model * screenspaceWorldPosition);
			direction = glm::normalize(position - position2);
		}

		void setDebugPosition(glm::vec3 position)
		{
			auto& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(m_debug);
			transform.position = position;
		}

		void setupGraphics(ecs::EntityID entity, std::shared_ptr<graphics::Mesh> mesh)
		{
			graphics::RenderObject& ro = m_ecs.getSystem<engine::RenderSystem>()->getOrCreate(entity);
			ro.mesh = mesh;
			m_pipeline->attach(ro);

			engine::Culling& culli = m_ecs.getSystem<engine::CullingSystem>()->getOrCreate(entity);
			culli.offset = mesh->pivot;
			culli.radius = mesh->radius;
		}

		bool initialize() final
		{
			if (!xp::App::initializeWindow(&m_window))
			{
				return false;
			}
			//m_window.setLockFocus(true);
			std::shared_ptr<graphics::Node> outputNode = std::make_shared<engine::OutputNode>(
				m_context,
				m_swapchain
				);
			outputNode->initialize();

			std::shared_ptr<engine::WireEffect> effect = std::make_shared<engine::WireEffect>();
			effect->initialize(m_device);
			std::shared_ptr<engine::BasicNode> node = std::make_shared<engine::BasicNode>(
				m_device,
				m_context
				);
			node->setEffect(effect);
			node->setOutput(outputNode);
			node->initialize();
			m_pipeline->addNode(node);

			m_pipeline->addNode(outputNode);

			// Init ecs..
			m_ecs.createSystem<engine::TransformSystem>();
			m_ecs.createSystem<engine::RenderSystem>();
			m_ecs.createSystem<engine::CameraSystem>();
			m_ecs.createSystem<engine::InputSystem>();
			m_ecs.createSystem<engine::CullingSystem>();

			// Setup input
			{
				engine::InputSystem* input = m_ecs.getSystem<engine::InputSystem>();
				// Keyconfig
				input::KeyMap keymap;
				input::loadKeymapDefaults(keymap, input->getSemantics());

				engine::loadConfig("resources/config.json", keymap, input->getSemantics());

				m_system.setKeyMap(keymap);

				m_system.key.addListener(input);
				m_system.mouse.addListener(input);
			}

			// Resources!
			m_debug = m_ecs.createEntity();

			m_entity = m_ecs.createEntity();
			m_entity2 = m_ecs.createEntity();
			m_entity3 = m_ecs.createEntity();
			m_cameraEntity = m_ecs.createEntity();

			std::shared_ptr<graphics::Mesh> mesh = graphics::generator::createUnitCube(m_device);

			std::shared_ptr<graphics::Mesh> helmMesh;
			if (true) // TODO!
			{
				filesystem::Disk disk;
				std::unique_ptr<filesystem::Handle> fh = disk.getHandle("C:/dev/popo/tools/mop/pyconvert/tmp/generate/gen_0.mesh");

				mesh = mop::loadMesh(m_device, *fh);

				mesh->radius = 2.0f;
			}

			{
				ecs::EntityID entity = m_debug;
				setupGraphics(entity, mesh);

				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);

				transform.position = glm::vec3{ 0.0f , 10.0f, 0.0f };
				transform.scale = glm::vec3{ 0.1f , 0.1f, 0.1f };
				transform.rotation = glm::quat(glm::vec3(0.0, 0.0, 0.0));
			}

			{
				ecs::EntityID entity = m_entity;
				setupGraphics(entity, mesh);

				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);

				transform.position = glm::vec3{ 0.0f , 5.0f, 0.0f };
				transform.scale = glm::vec3{ 1.0f , 1.0f, 1.0f };
				transform.rotation = glm::quat(glm::vec3(0.0, 0.0, 0.0));
			}
			{
				ecs::EntityID entity = m_entity2;
				setupGraphics(entity, mesh);

				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);

				transform.position = glm::vec3{ 3.0f , 0.0f, 0.0f };
				transform.scale = glm::vec3{ 1.0f , 1.0f, 1.0f };
				transform.rotation = glm::quat(glm::vec3(0.0, 0.0, 0.0));
			}
			{
				ecs::EntityID entity = m_entity3;
				setupGraphics(entity, mesh);

				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);

				transform.position = glm::vec3{ -3.0f , 0.0f, 0.0f };
				transform.scale = glm::vec3{ 1.0f , 1.0f, 1.0f };
				transform.rotation = glm::quat(glm::vec3(0.0, 0.0, 0.0));
			}
			{
				ecs::EntityID entity = m_cameraEntity;
				graphics::RenderView& view = m_ecs.getSystem<engine::CameraSystem>()->getOrCreate(entity);

				view.projection = glm::perspective(glm::radians(45.0f), (float)m_window.getWidth() / (float)m_window.getHeight(), 0.1f, 100.0f);

				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(entity);

				transform.position = glm::vec3(5.0f, 5.0f, 5.0f);
				transform.lookAt(glm::vec3(0.0f, 0.0f, 0.0f));

				setupFirstPersonCamera(entity);
			}

			return true;
		}

		bool update() final
		{
			Time::time_point now = Time::now();
			if (m_timestamp.frameID == 0)
			{
				m_time = now;
			}
			std::chrono::duration<uint64_t, std::nano> duration = now - m_time;
			m_time = now;

			m_timestamp.frameID++;
			m_timestamp.deltaNano = duration.count();
			m_timestamp.totalNano += m_timestamp.deltaNano;
			m_timestamp.update();

			{
				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(m_entity);
				transform.rotation = glm::angleAxis(m_timestamp.totalSeconds * glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
			}
			{
				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(m_entity2);
				transform.rotation = glm::angleAxis(m_timestamp.totalSeconds * glm::radians(90.f), glm::vec3(1.f, 0.f, 0.f));
			}
			{
				engine::Transform& transform = m_ecs.getSystem<engine::TransformSystem>()->getOrCreate(m_entity3);
				transform.rotation = glm::angleAxis(m_timestamp.totalSeconds * glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
			}

			m_system.update();
			m_ecs.update(m_timestamp);

			engine::CameraSystem* cameraSystem = m_ecs.getSystem<engine::CameraSystem>();

			m_renderData = cameraSystem->getRenderData();

			if (!m_running)
			{
				return false;
			}

			return true;
			//return m_timestamp.totalSeconds < 10.0f;
		}

		void render() final
		{
			m_pipeline->render(m_renderData);
		}

		void handle(const input::ApplicationEvent& event) override
		{
			if (event.state == input::ApplicationState::terminate || event.state == input::ApplicationState::quitRequested)
			{
				m_running = false;
			}
		}
	private:
		typedef std::chrono::high_resolution_clock Time;

		platform::System& m_system;
		platform::Window& m_window;

		Time::time_point m_time;

		bool m_running = true;

		graphics::RenderData m_renderData;
		ecs::Context m_ecs;
		ecs::Timestamp m_timestamp;

		ecs::EntityID m_debug = ecs::invalidEntity;

		ecs::EntityID m_entity = ecs::invalidEntity;
		ecs::EntityID m_entity2 = ecs::invalidEntity;
		ecs::EntityID m_entity3 = ecs::invalidEntity;
		ecs::EntityID m_cameraEntity = ecs::invalidEntity;
	};

} // xp
#endif // XP_CUBE_APP_H_