#ifndef XP_APP_H_
#define XP_APP_H_

#include <glm/glm.hpp>
#include <platform/window.h>
#include <common/app.h>

#ifndef NOMINMAX
#    define NOMINMAX
#endif

#include <EngineFactoryVk.h>

#include <RenderDevice.h>
#include <DeviceContext.h>
#include <SwapChain.h>
#include <RefCntAutoPtr.hpp>

#include <graphics/pipeline.h>

namespace xp
{
	class App : public common::App
	{
	public:
		virtual ~App() = default;

	protected:
		bool initializeWindow(platform::Window* window)
		{
			if (window == nullptr)
			{
				return false;
			}
			Diligent::SwapChainDesc swapchainDesc;

			auto getEngineFactory = Diligent::LoadGraphicsEngineVk();

			Diligent::EngineVkCreateInfo engineInfo;
			engineInfo.EnableValidation = true;

			auto* factory = getEngineFactory();
			factory->CreateDeviceAndContextsVk(engineInfo, &m_device, &m_context);

			Diligent::NativeWindow nativeWindow{ window->getHandle().hwnd };
			factory->CreateSwapChainVk(m_device, m_context, swapchainDesc, nativeWindow, &m_swapchain);

			m_pipeline = std::make_unique<graphics::Pipeline>("SimplePipeline");

			return true;
		}

		void windowResize(const glm::uvec2& size)
		{
			if (m_pipeline)
			{
				m_pipeline->resize(size);
			}
		}

		Diligent::RefCntAutoPtr<Diligent::IRenderDevice> m_device;
		Diligent::RefCntAutoPtr<Diligent::IDeviceContext> m_context;
		Diligent::RefCntAutoPtr<Diligent::ISwapChain> m_swapchain;

		std::unique_ptr<graphics::Pipeline> m_pipeline;
	};

} // xp
#endif // XP_APP_H_