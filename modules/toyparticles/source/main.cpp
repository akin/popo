
#include <iostream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <SDL.h>
#include <platform/system.h>
#include <platform/window.h>

#ifndef NOMINMAX
#    define NOMINMAX
#endif

#include <common/app.h>
#include <app/xpcubeapp.h>

int main(int count, char** args)
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}
	spdlog::info("Welcome to POPO!");
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	platform::System system;
	std::unique_ptr<platform::Window> window = system.createWindow();

	window->init({ 800, 600 }, { 110,110 }, "Hello world");

	std::unique_ptr<common::App> app = std::make_unique<xp::CubeApp>(system, *window.get());
	if (!app->initialize())
	{
		return -1;
	}

	while (app->update())
	{
		app->render();
	}

	return 0;
}
