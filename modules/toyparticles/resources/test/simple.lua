-- simple.lua
local simple =  {}

function simple.test() 
    mapSize = IVec2:new(400, 400)
    startPoint = IVec2:new(10, 10)
    endPoint = IVec2:new(mapSize.x - 10, mapSize.y - 10)
    
    emptyTile = Tile:new(32, Vec4:new(0.55, 0.55, 0.75, 1.0))
    wallTile = Tile:new(219, Vec4:new(0.21, 0.21, 0.42, 1.0))
    startTile = Tile:new(83, Vec4:new(0.0, 1.0, 0.0, 1.0))
    endTile = Tile:new(69, Vec4:new(0.0, 0.0, 1.0, 1.0))
    routeTile = Tile:new(83, Vec4:new(1.0, 1.0, 1.0, 1.0))
    
    config = MazeConfig:new()
    config.size = mapSize
    config.points["start"] = startPoint
    config.points["end"] = endPoint
    
    config.tiles["start"] = startTile
    config.tiles["end"] = endTile
    config.tiles["wall"] = wallTile
    config.tiles["empty"] = emptyTile
    config.tiles["route"] = routeTile
    
    print("Creating maze")
    seed = 3
    complexity = 200
    --maze = createMazeRandom(config, complexity, 10, 3, seed)
    --maze = createMazeTriangles(config, complexity, seed)
    maze = createMazeVoronoi(config, complexity, seed)
    
    print("Finding shortest path")
    connection = maze:getShortestPath(startPoint, endPoint, emptyTile)
    
    maze:setTiles(connection, routeTile)
    maze:setTile(startPoint, startTile)
    maze:setTile(endPoint, endTile)
    
    print("Saving file")
    if not saveMazeToImage(maze, "maze.png") then
        print("Failed to save file!")
    end
end

return simple