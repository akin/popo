// https://www.sandeepnambiar.com/getting-started-with-bgfx/
// outputs
vec4 v_color0 : COLOR0;

// inputs
vec3 a_position : POSITION;
vec4 a_color0 : COLOR0;