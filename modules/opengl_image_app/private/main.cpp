#include <iostream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <graphics_common.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define GL_CHECK_ERROR() { \
	GLenum result = glGetError(); \
	if(result != GL_NO_ERROR) { \
		handleGlError(__FILE__, __LINE__, result); \
	}\
}

static void handleGlError(const char* file, int line, GLenum error)
{
	std::cerr << file << ":" << line << " Error! " << error << std::endl;
	assert(false);
}

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

const std::string g_vertexShaderSource{
R"SHADER(
precision highp float;

attribute vec3 position;
attribute vec2 uv;

uniform mat4 model;
uniform mat4 camera;

varying vec2 texcoord;

void main()
{
	mat4 mvp = camera * model;
	gl_Position = mvp * vec4(position, 1.0);
	texcoord = uv;
}

)SHADER"
};

const std::string g_fragmentShaderSource{
R"SHADER(
precision highp float;

uniform sampler2D texture;

varying vec2 texcoord;

void main()
{
	gl_FragColor = texture2D(texture, texcoord);
}

)SHADER"
};

#define HANDLE_GL_ERROR {GLenum err; while((err = glGetError()) != GL_NO_ERROR) {assert(false);}}

class Effect
{
public:
	bool init(const std::string& vertexSource, const std::string& fragmentSource)
	{
		id = glCreateProgram();
		HANDLE_GL_ERROR;
		{
			vertexShader = glCreateShader(GL_VERTEX_SHADER);
			HANDLE_GL_ERROR;

			const GLchar* src = static_cast<const GLchar*>(vertexSource.data());
			const GLint size = static_cast<GLint>(vertexSource.size());
			glShaderSource(vertexShader, 1, &src, &size);
			HANDLE_GL_ERROR;
			if (!compileShader(vertexShader))
			{
				glDeleteShader(vertexShader);
				assert(false);
				return false;
			}
			glAttachShader(id, vertexShader);
			HANDLE_GL_ERROR;
		}
		{
			fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
			HANDLE_GL_ERROR;
			const GLchar* src = static_cast<const GLchar*>(fragmentSource.data());
			const GLint size = static_cast<GLint>(fragmentSource.size());
			glShaderSource(fragmentShader, 1, &src, &size);
			HANDLE_GL_ERROR;
			if (!compileShader(fragmentShader))
			{
				glDeleteShader(fragmentShader);
				assert(false);
				return false;
			}
			glAttachShader(id, fragmentShader);
			HANDLE_GL_ERROR;
		}
		link();

		HANDLE_GL_ERROR;
		return true;
	}

	~Effect()
	{
		glDeleteProgram(id);
		glDeleteShader(fragmentShader);
		glDeleteShader(vertexShader);
	}

	void use()
	{
		glUseProgram(id);
		HANDLE_GL_ERROR;
	}

	bool compileShader(uint32_t shader)
	{
		glCompileShader(shader);
		HANDLE_GL_ERROR;

		GLint isCompiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

			return false;
		}
		return true;
	}

	bool link()
	{
		glLinkProgram(id);
		HANDLE_GL_ERROR;

		GLint isLinked = 0;
		glGetProgramiv(id, GL_LINK_STATUS, &isLinked);
		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(id, maxLength, &maxLength, &infoLog[0]);

			return false;
		}
		return true;
	}

	uint32_t id = 0;
	uint32_t vertexShader = 0;
	uint32_t fragmentShader = 0;
};

class Buffer
{
public:
	Buffer() = default;

	Buffer(size_t bytesize)
	{
		init(bytesize);
	}

	Buffer(size_t bytesize, const void* data)
	{
		init(bytesize, data);
	}

	bool init(size_t bytesize, const void* data = nullptr)
	{
		GLuint bufferID;
		glGenBuffers(1, &bufferID);
		HANDLE_GL_ERROR;

		id = bufferID;
		size = bytesize;
		glBindBuffer(GL_ARRAY_BUFFER, id);
		HANDLE_GL_ERROR;
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(size), data, GL_STATIC_DRAW);
		HANDLE_GL_ERROR;

		return true;
	}

	~Buffer()
	{
		GLuint bufferID = id;
		glDeleteBuffers(1, &bufferID);
	}

	void set(const void* data)
	{
		glBindBuffer(GL_ARRAY_BUFFER, id);
		HANDLE_GL_ERROR;
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(size), data, GL_STATIC_DRAW);
		HANDLE_GL_ERROR;
	}

	uint32_t id = 0;
	size_t size = 0;
};

class Attribute
{
public:
	Attribute() = default;

	Attribute(const Effect& effect, const std::string& name_)
	{
		init(effect, name_);
	}

	void init(const Effect& effect, const std::string& name_)
	{
		name = name_;
		id = glGetAttribLocation(effect.id, name.c_str());
		HANDLE_GL_ERROR;
		assert(id >= 0);
	}

	std::string name;
	GLint id = 0;
};

class Uniform
{
public:
	Uniform() = default;

	Uniform(const Effect& effect, const std::string& name_)
	{
		init(effect, name_);
	}

	void init(const Effect& effect, const std::string& name_)
	{
		name = name_;
		id = glGetUniformLocation(effect.id, name.c_str());
		HANDLE_GL_ERROR;
		assert(id >= 0);
	}

	std::string name;
	GLint id = 0;
};

struct Vertex
{
	float x, y, z;
	float u, v;
};

class Image
{
public:
	~Image()
	{
		glDeleteTextures(1, &id);
	}

	bool init()
	{
		glGenTextures(1, &id);
		HANDLE_GL_ERROR;

		if (id == 0)
		{
			return false;
		}

		glActiveTexture(GL_TEXTURE0);
		HANDLE_GL_ERROR;
		glBindTexture(GL_TEXTURE_2D, id);
		HANDLE_GL_ERROR;

		// TODO GL_RGB is not defined at this level???
		GLint internalformat = GL_RGBA;
		GLenum format = GL_RGBA;

		assert(extFormat == 4 && "Only supporting RGBA texture format.");

		glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, data.data());
		HANDLE_GL_ERROR;
		glGenerateMipmap(GL_TEXTURE_2D);
		HANDLE_GL_ERROR;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		HANDLE_GL_ERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		HANDLE_GL_ERROR;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		HANDLE_GL_ERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		HANDLE_GL_ERROR;

		glBindTexture(GL_TEXTURE_2D, 0);
		HANDLE_GL_ERROR;

		data = std::move(std::vector<uint8_t>());

		return true;
	}

	float getWidth() const
	{
		return width;
	}

	float getHeight() const
	{
		return width;
	}

	std::vector<uint8_t> data;
	GLuint id = 0;
	int width = 0;
	int height = 0;
	int extFormat = 0;
};

class AttributeObject
{
public:
	bool init()
	{
		glGenVertexArrays(1, &id);
		return true;
	}

	void bind()
	{
		glBindVertexArray(id);
	}

	void unbind()
	{
		glBindVertexArray(0);
	}

	GLuint id;
};

bool loadImage(const std::string& filename, Image& image)
{
	int x, y, c;
	stbi_uc* data = stbi_load(filename.c_str(), &x, &y, &c, 4);
	if (data == nullptr)
	{
		spdlog::error("Failed to load image {0}", filename);
		return false;
	}

	// Load image
	image.width = x;
	image.height = y;
	image.extFormat = c;
	image.data.resize(x * y * c);
	std::memcpy(image.data.data(), data, image.data.size());
	STBI_FREE(data);

	return image.init();
}

int main(int argc, char* argv[])
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		spdlog::error("Failed to initialize GLAD");
		return -1;
	}

	Effect effect;
	effect.init(g_vertexShaderSource, g_fragmentShaderSource);

	Image image;
	loadImage("resources/resources/pigs_500.png", image);

	std::vector<Vertex> vertexData;
	vertexData.push_back({
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f });
	vertexData.push_back({
		0.0f, image.getHeight(), 0.0f,
		0.0f, 1.0f });
	vertexData.push_back({
		image.getWidth(), 0.0f, 0.0f,
		1.0f, 0.0f });
	vertexData.push_back({
		image.getWidth(), image.getHeight(), 0.0f,
		1.0f, 1.0f });

	Buffer buffer{ vertexData.size() * sizeof(Vertex), vertexData.data() };

	Attribute aPosition{ effect, "position" };
	Attribute aUV{ effect, "uv" };
	Uniform uModel{ effect, "model" };
	Uniform uCamera{ effect, "camera" };
	Uniform uTexture{ effect, "texture" };

	AttributeObject vao;
	vao.init();
	vao.bind();

	glEnableVertexAttribArray(aPosition.id);
	GL_CHECK_ERROR();
	glEnableVertexAttribArray(aUV.id);
	GL_CHECK_ERROR();

	glBindBuffer(GL_ARRAY_BUFFER, buffer.id);
	GL_CHECK_ERROR();
	glVertexAttribPointer(aPosition.id, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, x));
	GL_CHECK_ERROR();
	glVertexAttribPointer(aUV.id, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, u));
	GL_CHECK_ERROR();

	glm::mat4 projection;

	projection = glm::ortho(-128.0f, 128.0f, -128.0f, 128.0f, 0.1f, 1000.0f);

	glm::mat4 cameraPos = glm::mat4(1.0);
	glm::mat4 model = glm::mat4(1.0);

	model = glm::translate(model, glm::vec3(-128.0f, -128.0f, -1.0f));

	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);
		GL_CHECK_ERROR();
		glClear(GL_COLOR_BUFFER_BIT);
		GL_CHECK_ERROR();

		effect.use();

		glm::mat4 view;
		//view = glm::inverse(cameraPos);
		view = cameraPos;

		glm::mat4 pv;
		pv = view * projection;

		glUniformMatrix4fv(uCamera.id, 1, GL_FALSE, glm::value_ptr(pv));
		GL_CHECK_ERROR();
		glUniformMatrix4fv(uModel.id, 1, GL_FALSE, glm::value_ptr(model));
		GL_CHECK_ERROR();

		glActiveTexture(GL_TEXTURE0);
		GL_CHECK_ERROR();
		glBindTexture(GL_TEXTURE_2D, image.id);
		GL_CHECK_ERROR();
		glUniform1i(uTexture.id, 0);
		GL_CHECK_ERROR();

		vao.bind();

		glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexData.size());
		GL_CHECK_ERROR();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
