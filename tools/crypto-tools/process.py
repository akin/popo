
import os, sys, shutil
import inspect
from datetime import datetime
from pathlib import Path
import json
import types

class ValueToken:
    def __init__(self):
        self.currency = None
        self.amount = 0.0
        self.message = None
        self.address = None
    
    def validate(self):
        if self.currency == None:
            raise RuntimeError("ValueToken missing currency type")
        if self.amount < 0.0:
            raise RuntimeError("ValueToken cannot be negative")

class Transaction:
    def __init__(self):
        self.type = None
        self.name = None
        self.date = None
        self.rate = None
        self.give = None
        self.receive = None
        self.fee = None
    
    def validate(self):
        if type(self.date) is not datetime:
            raise RuntimeError("Transaction missing date")
        if type(self.name) is not str or self.name == "":
            raise RuntimeError("Transaction missing name")
        if self.give != None:
            self.give.validate()
        if self.receive != None:
            self.receive.validate()
        if self.fee != None:
            self.fee.validate()

class Context:
    def __init__(self):
        self.transactions = []
    
    def addTransaction(self, transaction):
        transaction.validate()
        self.transactions.append(transaction)

class JsonParser:
    def __init__(self, context):
        self.context = context

    def processValueToken(self, item):
        token = ValueToken()
        if "currency" in item:
            token.currency = item["currency"].lower()
        if "amount" in item:
            token.amount = item["amount"]
        if "message" in item:
            token.message = item["message"]
        if "address" in item:
            token.address = item["address"]
        return token

    def processDate(self, date):
        # Excepting date in format: day/month/year hour.minute.second
        # no leading zeros.
        return datetime.strptime(date, '%d/%m/%Y %H.%M.%S')

    def parseFromPath(self, path):
        file = open(path)
        jsdata = json.load(file)
        if "data" not in jsdata:
            return
        for item in jsdata["data"]:
            transaction = Transaction()
            transaction.type = item["type"].lower()
            if "name" in item:
                transaction.name = item["name"]
            if "date" in item:
                transaction.date = self.processDate(item["date"])
            if "rate" in item:
                transaction.rate = item["rate"]
            if "give" in item:
                transaction.give = self.processValueToken(item["give"])
            if "receive" in item:
                transaction.receive = self.processValueToken(item["receive"])
            if "fee" in item:
                transaction.fee = self.processValueToken(item["fee"])
            self.context.addTransaction(transaction)

class CurrencyBalances:
    def __init__(self, type):
        self.type = type
        self.transactions = []

class ProcessProfits:
    def __init__(self, context):
        self.context = context
        self.balances = {}

    def process(self):
        self.context.transactions.sort(key = lambda item: item.date)

        mining = [x for x in self.context.transactions if x.type == "mine"]
        receives = [x for x in self.context.transactions if x.type == "receive"]
        buys = [x for x in self.context.transactions if x.type == "buy"]
        sells = [x for x in self.context.transactions if x.type == "sell"]

        # mining is a transaction where my account receives value
        # receive is a transaction where my account receives value
        # buy is a transaction where my account looses GIVE value but gains RECEIVE value
        # sell is a transaction where my account looses GIVE value but gains RECEIVE value

        ## Finnish tax system:
        # 30% of gains is taxed.
        # BUY/SELL pääomatulon alaista
        # if GIVE is EUR, I do not need to pay taxes
        # if GIVE is anything else than EUR, I need to convert used X to EUR with current rate, and pay whatever profit I made for that amount.
        # if RECEIVE is EUR then the process of calculating taxes is just easier.
        # MINING/RECEIVE ansiotuloveron alaista.

        print(sells)

if __name__ == "__main__":
    import argparse as arggs
    
    # vscode debug launch.js line:
    # "args": ["--in", "example_data.json"]
    arguments_parser = arggs.ArgumentParser(description='Process crypto balances')
    arguments_parser.add_argument('-i','--in', help='input file', required=True)
    args = vars(arguments_parser.parse_args())

    context = Context()

    path = Path(os.path.dirname(__file__), args['in'])
    parser = JsonParser(context)
    parser.parseFromPath(path)

    util = ProcessProfits(context)
    util.process()
