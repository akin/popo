from datetime import datetime
import csv

class RowHelper:
    def __init__(self, keys):
        self.keys = keys
        self.reset()
    
    def reset(self):
        self.data = [''] * len(self.keys) # for this syntax, drugs must have been involved
    
    def getKeys(self):
        return self.keys
    
    def getValues(self):
        return self.data
    
    def setValue(self, key, value):
        self.data[self.getKeys().index(key)] = value

if __name__ == "__main__":
    args = {}
    
    debug = True
    if not debug:
        import argparse as arggs
        # vscode debug launch.js line:
        # "args": ["--in", "example_data.json"]
        arguments_parser = arggs.ArgumentParser(description='Process crypto balances to koinly format')
        arguments_parser.add_argument('-i','--in', help='input file', required=True)
        arguments_parser.add_argument('-o','--out', help='output file', required=True)
        args = vars(arguments_parser.parse_args())
    else:
        args['in'] = 'orders.csv'
        args['out'] = 'outti.csv'

    outfile = open(args['out'], 'w+', newline='')
    writer = csv.writer(outfile, delimiter=',', quotechar='', quoting=csv.QUOTE_NONE)

    outHelper = RowHelper(["Date", "Sent Amount", "Sent Currency", "Received Amount", "Received Currency", "Fee Amount", "Fee Currency", "Net Worth Amount", "Net Worth Currency", "Label", "Description", "TxHash"])

    # Coinmerce -> Koinly
    # incomplete script :(
    with open(args['in']) as infile:
        reader = csv.reader(infile)
        line = 0
        for row in reader:
            line += 1
            if line == 1:
                if ', '.join(row) != "Date, Type, Coin, Amount crypto, Amount fiat, Price":
                    raise RuntimeError("Invalid data")

                writer.writerow(outHelper.getKeys())
                continue
            
            fiatCoin = 'eur'

            # ['2021-05-06 10:27', 'SELL', 'XRP', '50', '68.51', '1.37013054']
            date = datetime.strptime(row[0], '%Y-%m-%d %H:%M') # '2021-05-06 10:27'
            action = row[1] # 'SELL'
            coin = row[2] # 'XRP'
            amount = row[3] #'50'
            fiatAmount = row[4] # '68.51'
            rate = row[5] # '1.37013054'

            outHelper.reset()
            outHelper.setValue("Date", date.strftime('%Y-%m-%d %H:%M:%S') + ' UTC')

            # CM is coinmerce coin, basically 1:1 with euro, which I think should count as EUROs
            if coin == 'CM':
                coin = fiatCoin

            # actions
            if action == 'SELL':
                outHelper.setValue("Sent Amount", amount)
                outHelper.setValue("Sent Currency", coin)
                outHelper.setValue("Received Amount", fiatAmount)
                outHelper.setValue("Received Currency", fiatCoin)
                outHelper.setValue("Label", "sell")
                outHelper.setValue("Description", f'rate {rate}')

            elif action == 'WITHDRAW':
                outHelper.setValue("Received Amount", fiatAmount)
                outHelper.setValue("Received Currency", fiatCoin)
                outHelper.setValue("Label", "withdraw")
                outHelper.setValue("Description", f'rate {rate}')

                # coinmerce sell & withdraw is WEIRD.
                continue
                
            elif action == 'BUY':
                outHelper.setValue("Sent Amount", fiatAmount)
                outHelper.setValue("Sent Currency", fiatCoin)
                outHelper.setValue("Received Amount", amount)
                outHelper.setValue("Received Currency", coin)
                outHelper.setValue("Label", "buy")
                outHelper.setValue("Description", f'rate {rate}')
                
            elif action == 'DEPOSIT':
                outHelper.setValue("Received Amount", amount)
                outHelper.setValue("Received Currency", coin)
                outHelper.setValue("Label", "deposit")
                # coinmerce deposit is WEIRD.
                continue

            else:
                raise RuntimeError(f"Unknown action '{action}'")
            
            writer.writerow(outHelper.getValues())
