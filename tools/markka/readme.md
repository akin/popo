
# Markka Blockchain cryptocurrency.


## License
@copyright THE BEER-WARE LICENSE (c) 2022
----------------------------------------------------------------------------
"THE BEER-WARE LICENSE" (Revision 42):
<mikael_korpela@hotmail.com> wrote this file.  As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. 

https://en.wikipedia.org/wiki/Beerware
----------------------------------------------------------------------------