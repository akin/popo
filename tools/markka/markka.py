
import hashlib
import time
import json

class Block:
    def __init__(self, index, previousHash, timestamp):
        self.index = index
        self.data = []
        self.previousHash = previousHash
        self.timestamp = timestamp
        self.proof = 0
        self.hash = ""

    def getObject(self):
        return {
            "index": self.index,
            "data": self.data,
            "proof": self.proof,
            "previousHash": self.previousHash,
            "timestamp": self.timestamp,
        }
    
    def setTransactions(self, transactions):
        self.data = transactions
    
    def getTransactions(self):
        return self.data

    def calculateHash(self):
        obj = self.getObject()
        hashData = json.dumps(obj)
        return hashlib.sha3_256(hashData.encode()).hexdigest()
    
    def verify(self):
        if len(self.hash) < 4:
            return False
        return self.hash[4:] == "1917"

    def setProof(self, proof):
        self.proof = proof
        self.hash = self.calculateHash()
        return self.verify()

class BlockChain:
    def __init__(self):
        self.transactions = []
        self.blocks = []
        
    def commitBlock(self):
        previousHash = 0
        if len(self.blocks) > 0:
            previousHash = self.getLastBlock().hash
        block = Block(len(self.blocks), previousHash, time.time())
        block.setTransactions(self.transactions)
        self.transactions = []
        self.mine(block)
        self.blocks.append(block)

    def mine(self, block):
        proof = 0
        while not block.setProof(proof):
            proof += 1

    def addTransaction(self, transaction):
        self.transactions.append(transaction)

    def getLastBlock(self):
        return self.blocks[-1]
    
    def getBlockCount(self):
        return len(self.blocks)
    
    def getBlock(self, id):
        return self.blocks[id]
    
    def getObject(self):
        content = []
        for block in self.blocks:
            obj = block.getObject()
            obj["hash"] = block.hash
            content.append(obj)
        return content

class Markka:
    def __init__(self):
        self.bc = BlockChain()

    def send(self, sender, receiver, amount):
        self.bc.addTransaction({
            "type": "transfer",
            "sender": sender,
            "receiver": receiver,
            "value": amount
        })
    
    def commit(self):
        self.bc.commitBlock()
    
    def getBalance(self, user):
        balance = 0.0
        for block in self.bc.blocks:
            for transaction in block.getTransactions():
                if transaction["type"] == "transfer":
                    if transaction["sender"] == user:
                        balance -= transaction["value"]
                    if transaction["receiver"] == user:
                        balance += transaction["value"]
        return balance

    def getLedger(self):
        return self.bc.getObject()

bank = Markka()
bank.send("bank", "Kekkonen", 1000.0)
bank.send("Kekkonen", "Väyrynen", 500.0)
bank.commit()

print("Bank has {}mk".format(bank.getBalance("bank")))
print("Kekkonen has {}mk".format(bank.getBalance("Kekkonen")))
print("Väyrynen has {}mk".format(bank.getBalance("Väyrynen")))

bank.send("bank", "Kekkonen", 1000.0)
bank.send("Kekkonen", "Väyrynen", 500.0)
bank.commit()
print("Bank has {}mk".format(bank.getBalance("bank")))
print("Kekkonen has {}mk".format(bank.getBalance("Kekkonen")))
print("Väyrynen has {}mk".format(bank.getBalance("Väyrynen")))

json_dump = json.dumps(bank.getLedger())
print(json_dump)
