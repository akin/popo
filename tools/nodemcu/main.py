import machine
import time, math
import neopixel

led = machine.PWM(machine.Pin(2), freq=1000)

def pulse(led, timeMs):
    for i in range(20):
        led.duty(int(math.sin(i / 10 * math.pi) * 500 + 500))
        time.sleep_ms(timeMs)

for i in range(2):
    pulse(led, 20)
    
def testPort(port):
    try:
        pin = machine.Pin(port)
        print("port", port, pin.read())
    except:
        print("port", port, "invalid port")

def scanPorts():
    for i in range(16):
        testPort(i)

def neoDemo(np):
    n = np.n

    # cycle
    for i in range(4 * n):
        for j in range(n):
            np[j] = (0, 0, 0)
        np[i % n] = (255, 255, 255)
        np.write()
        time.sleep_ms(25)

    # bounce
    for i in range(4 * n):
        for j in range(n):
            np[j] = (0, 0, 128)
        if (i // n) % 2 == 0:
            np[i % n] = (0, 0, 0)
        else:
            np[n - 1 - (i % n)] = (0, 0, 0)
        np.write()
        time.sleep_ms(60)

    # fade in/out
    for i in range(0, 4 * 256, 8):
        for j in range(n):
            if (i // 256) % 2 == 0:
                val = i & 0xff
            else:
                val = 255 - (i & 0xff)
            np[j] = (val, 0, 0)
        np.write()

    # clear
    for i in range(n):
        np[i] = (0, 0, 0)
    np.write()
    
        
class StripHelper:
    def __init__(self, strip):
        self.strip = strip
    
    def setAllRGBW(self, r,g,b,w):
        for idx in range(self.strip.n):
            self.strip[idx]=(r,g,b,w)
    
    def setRangeRGBW(self, start, stop, r,g,b,w):
        for idx in range(start, stop):
            self.strip[idx]=(r,g,b,w)
    
    def sendRGBW(self, r,g,b,w):
        for idx in reversed(range(self.strip.n - 1)):
            self.strip[idx + 1]=self.strip[idx]
        self.strip[0]=(r,g,b,w)
    
    def setAllRGB(self, r,g,b):
        for idx in range(self.strip.n):
            self.strip[idx]=(r,g,b)
    
    def setRangeRGB(self, start, stop, r,g,b):
        for idx in range(start, stop):
            self.strip[idx]=(r,g,b)
    
    def sendRGB(self, r,g,b):
        for idx in reversed(range(self.strip.n - 1)):
            self.strip[idx + 1]=self.strip[idx]
        self.strip[0]=(r,g,b)
