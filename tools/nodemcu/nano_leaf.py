
import serial
import io
import time
import random

class Chip:
    def __init__(self, port):
        self.serial = serial.Serial()
        self.serial.baudrate = 115200
        self.serial.timeout = None
        self.serial.port = port
        self.serial.open()
        self.count = 0
        self.write("import machine, time, math, neopixel")
        
    def write(self, command):
        print(">> " + command)
        
        self.count += self.serial.write(command.encode())
        self.count += self.serial.write('\r\n'.encode())
        if self.count > 128:
            self.flush()
            
    def writeChar(self, command):
        self.count += self.serial.write(command)
        
    def flush(self):
        time.sleep(0.001)
        self.serial.flush()
        self.count = 0
        
    def close(self):
        self.serial.close()
        
class Strip:
    def __init__(self, chip, pin, count):
        self.chip = chip
        self.pin = pin
        self.ledCount = count
        self.name = "neo"
        self.apply()
        self.chip.write(self.name + "H = StripHelper(" + self.name + ")")

class Color:
    def __init__(self, r, g, b, w = 0.0):
        self.r = r
        self.g = g
        self.b = b
        self.w = w
    
    def interpolate(self, other, normal):
        inv = 1.0 - normal
        return Color(self.r * inv + other.r * normal, self.g * inv + other.g * normal, self.b * inv + other.b * normal, self.w * inv + other.w * normal)

class StripRGB(Strip):
    def __init__(self, chip, pin, count):
        super().__init__(chip, pin, count)
    
    def apply(self):
        self.chip.write(self.name + " = neopixel.NeoPixel(machine.Pin(" + str(self.pin) + "), " + str(self.ledCount) +")")
        self.chip.flush()
        
    def setColor(self, index, color):
        index = index % self.ledCount
        self.chip.write(self.name + "[" + str(index) + "]=(" + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + ")")
        
    def setAll(self, color):
        self.chip.write(self.name + "H.setAllRGB(" + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + ")")
        
    def setRange(self, start, stop, color):
        self.chip.write(self.name + "H.setRangeRGB(" + str(start) + "," + str(stop) + "," + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + ")")
        
    def sendColor(self, color):
        self.chip.write(self.name + "H.sendRGB(" + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + ")")
    
    def flush(self):
        self.chip.write(self.name + ".write()")
        self.chip.flush()
        
class StripRGBW(Strip):
    def __init__(self, chip, pin, count):
        super().__init__(chip, pin, count)
    
    def apply(self):
        self.chip.write(self.name + " = neopixel.NeoPixel(machine.Pin(" + str(self.pin) + "), " + str(self.ledCount) +", bpp=4)")
        self.chip.flush()
        
    def setColor(self, index, color):
        index = index % self.ledCount
        self.chip.write(self.name + "[" + str(index) + "]=(" + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + "," + str(int(color.w*0xFF)) + ")")
        
    def setAll(self, color):
        self.chip.write(self.name + "H.setAllRGBW(" + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + "," + str(int(color.w*0xFF)) + ")")
        
    def setRange(self, start, stop, color):
        self.chip.write(self.name + "H.setRangeRGBW(" + str(start) + "," + str(stop) + "," + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + "," + str(int(color.w*0xFF)) + ")")
        
    def sendColor(self, color):
        self.chip.write(self.name + "H.sendRGBW(" + str(int(color.r*0xFF)) + "," + str(int(color.g*0xFF)) + "," + str(int(color.b*0xFF)) + "," + str(int(color.w*0xFF)) + ")")
    
    def flush(self):
        self.chip.write(self.name + ".write()")
        self.chip.flush()
        
        
chip = Chip("COM3")
strip = StripRGB(chip, pin=4, count=6)

# reset
strip.setAll(Color(0.0, 0.0, 0.0, 0.0))
strip.flush()

def neoDemo(strip):
    n = strip.ledCount

    # bounce
    start = Color(0.0, 0.5, 0.3, 0.5)
    end = Color(0.5, 1.0, 0.9, 1.0)
    for i in range(150):
        strip.setAll(start.interpolate(end, (i % 30.0) / 30.0))
        strip.flush()
        time.sleep(0.001)
    
    # bounce2
    for i in range(10 * n):
        strip.setAll(Color(0.0, 0.5, 0.3, 0.0))
        strip.setColor(i % n, Color(1.0, 1.0, 1.0, 1.0))
        strip.flush()
        time.sleep(0.001)
        
    # random stream
    for i in range(10 * n):
        strip.sendColor(Color(random.random(), random.random(), random.random(), random.random()))
        
        time.sleep(0.025)
        strip.flush()

    
    # clear
    strip.setAll(Color(0.0, 0.0, 0.0, 0.0))
    strip.flush()

neoDemo(strip)

time.sleep(3) # wait for demo to finish
