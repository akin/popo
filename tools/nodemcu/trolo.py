
import serial
import io
import time

class Chip:
    def __init__(self, port):
        self.serial = serial.Serial()
        self.serial.baudrate = 115200
        self.serial.timeout = 1
        self.serial.port = port
        self.serial.open()
        self.sio = io.TextIOWrapper(io.BufferedRWPair(self.serial, self.serial))
        self.write("import machine, time, math, neopixel")
        
    def write(self, command):
        self.writeNoFlush(command)
        self.sio.flush()
        
    def writeNoFlush(self, command):
        print(">> " + command)
        command = command + "\n"
        self.sio.write(command)
        
    def close(self):
        self.serial.close()
        
class Strip:
    def __init__(self, chip):
        self.chip = chip
        self.pin = 4
        self.ledCount = 7
        self.name = "neo"
    
    def apply(self):
        self.chip.write(self.name + " = neopixel.NeoPixel(machine.Pin(" + str(self.pin) + "), " + str(self.ledCount) +")")
        
    def setColor(self, index, r, g, b):
        index = index % self.ledCount
        self.chip.writeNoFlush(self.name + "[" + str(index) + "]=(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + ")")
    
    def setAll(self, r, g, b):
        for idx in range(self.ledCount):
            self.setColor(idx, r, g, b)
    
    def flush(self):
        self.chip.write(self.name + ".write()")  
        
        
chip = Chip("COM3")

strip = Strip(chip)
strip.apply()
chip.write("neoDemo(neo)")
time.sleep(5) # wait for demo to finish

strip.setAll(0,0,0)
strip.setColor(0, 1.0, 1.0, 1.0)
strip.flush()
time.sleep(5)

def neoDemo(strip):
    n = strip.ledCount
    
    # cycle
    for i in range(4 * n):
        strip.setAll(0,0,0)
        strip.setColor(i % n, 1.0, 1.0, 1.0)
        strip.flush()
        time.sleep(0.025)

    # bounce
    for i in range(4 * n):
        strip.setAll(0,0,0.5)
        
        if (i // n) % 2 == 0:
            strip.setColor(i % n, 0.0, 0.0, 0.0)
        else:
            strip.setColor(n - 1 - (i % n), 0.0, 0.0, 0.0)
        strip.flush()
        time.sleep(0.060)
    
    # clear
    strip.setAll(0,0,0)
    strip.flush()

neoDemo(strip)

time.sleep(3) # wait for demo to finish
