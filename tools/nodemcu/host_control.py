
import serial
import io
import time
import random

class Chip:
    def __init__(self, port):
        self.serial = serial.Serial()
        self.serial.baudrate = 115200
        self.serial.timeout = None
        self.serial.port = port
        self.serial.open()
        self.count = 0
        self.write("import machine, time, math, neopixel")
        
    def write(self, command):
        print(">> " + command)
        
        self.count += self.serial.write(command.encode())
        self.count += self.serial.write('\r\n'.encode())
        if self.count > 128:
            self.flush()
            
    def writeChar(self, command):
        self.count += self.serial.write(command)
        
    def flush(self):
        time.sleep(0.001)
        self.serial.flush()
        self.count = 0
        
    def close(self):
        self.serial.close()
        
class Strip:
    def __init__(self, chip, pin, count):
        self.chip = chip
        self.pin = pin
        self.ledCount = count
        self.name = "neo"
        self.apply()
        self.chip.write(self.name + "H = StripHelper(" + self.name + ")")

class StripRGB(Strip):
    def __init__(self, chip, pin, count):
        super().__init__(chip, pin, count)
    
    def apply(self):
        self.chip.write(self.name + " = neopixel.NeoPixel(machine.Pin(" + str(self.pin) + "), " + str(self.ledCount) +")")
        self.chip.flush()
        
    def setColor(self, index, r, g, b):
        index = index % self.ledCount
        self.chip.write(self.name + "[" + str(index) + "]=(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + ")")
        
    def setAll(self, r, g, b):
        self.chip.write(self.name + "H.setAllRGB(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + ")")
        
    def setRange(self, start, stop, r, g, b):
        self.chip.write(self.name + "H.setRangeRGB(" + str(start) + "," + str(stop) + "," + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + ")")
        
    def sendColor(self, r, g, b):
        self.chip.write(self.name + "H.sendRGB(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + ")")
    
    def flush(self):
        self.chip.write(self.name + ".write()")
        self.chip.flush()
        
class StripRGBW(Strip):
    def __init__(self, chip, pin, count):
        super().__init__(chip, pin, count)
    
    def apply(self):
        self.chip.write(self.name + " = neopixel.NeoPixel(machine.Pin(" + str(self.pin) + "), " + str(self.ledCount) +", bpp=4)")
        self.chip.flush()
        
    def setColor(self, index, r, g, b, w):
        index = index % self.ledCount
        self.chip.write(self.name + "[" + str(index) + "]=(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + "," + str(int(w*0xFF)) + ")")
        
    def setAll(self, r, g, b, w):
        self.chip.write(self.name + "H.setAllRGBW(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + "," + str(int(w*0xFF)) + ")")
        
    def setRange(self, start, stop, r, g, b, w):
        self.chip.write(self.name + "H.setRangeRGBW(" + str(start) + "," + str(stop) + "," + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + "," + str(int(w*0xFF)) + ")")
        
    def sendColor(self, r, g, b, w):
        self.chip.write(self.name + "H.sendRGBW(" + str(int(r*0xFF)) + "," + str(int(g*0xFF)) + "," + str(int(b*0xFF)) + "," + str(int(w*0xFF)) + ")")
    
    def flush(self):
        self.chip.write(self.name + ".write()")
        self.chip.flush()
        
        
chip = Chip("COM3")
strip = StripRGBW(chip, pin=4, count=30)

# reset
strip.setAll(0,0,0,0)
strip.flush()

def neoDemo(strip):
    n = strip.ledCount
    
    # cycle
    for i in range(4 * n):
        strip.setAll(0,0,0,0)
        strip.setColor(i % n, 1.0, 1.0, 1.0, 1.0)
        strip.flush()
        
    # random stream
    for i in range(8 * n):
        strip.sendColor(0.0, 0.0, random.random(), (i % n) / n)
        
        time.sleep(0.001)
        strip.flush()

    # bounce
    for i in range(4 * n):
        strip.setAll(0 ,0 ,0.5 ,0.0)
        
        if (i // n) % 2 == 0:
            strip.setColor(i % n, 0.0, 0.0, 0.0, 0.0)
        else:
            strip.setColor(n - 1 - (i % n), 0.0, 0.0, 0.0, 0.0)
        strip.flush()
    
    # clear
    strip.setAll(0,0,0,0)
    strip.flush()

neoDemo(strip)

time.sleep(3) # wait for demo to finish
