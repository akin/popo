
# Nodemcu ESP8266 toyings around

![](NodeMCU-ESP8266-Pinout.jpg)

[REPL](https://docs.micropython.org/en/latest/esp8266/tutorial/repl.html)

 * [flash.main.py.bat](./flash.main.py.bat) bat file for flashing main.py to esp8266 board.
 * [install_image.bat](./install_image.bat) bat file for micropython to esp8266 board.
 
 
 * [main.py](./main.py) file that contains all sorts of gimmics to run on the client board.
 * [host_control.py](./host_control.py) a python program that uses serial port to call the REPL interface on esp8266 micropython. injects python code to the micropython on board.
 * [trolo.py](./trolo.py) old python test program to test different control strategies.
 
 ## License
@copyright THE BEER-WARE LICENSE (c) 2022
----------------------------------------------------------------------------
"THE BEER-WARE LICENSE" (Revision 42):
<mikael_korpela@hotmail.com> wrote this file.  As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. 

https://en.wikipedia.org/wiki/Beerware
----------------------------------------------------------------------------