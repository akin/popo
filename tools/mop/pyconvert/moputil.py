
import os, sys, shutil
import inspect
import flatbuffers
from pathlib import Path
import PIL
import json

import flatbuffers
from Mop import *

class MopUtil:
    def __init__(self, folder = ""):
        self.folder = folder

    def print(self, value, indent = 0):
        print(' ' * indent + value)
    
    def getDataFormatByteSize(self, dataformat):
        name = self.strDataFormat(dataformat)
        size = 0
        if name.endswith("64"):
            size = 8
        elif name.endswith("48"):
            size = 6
        elif name.endswith("32"):
            size = 4
        elif name.endswith("24"):
            size = 3
        elif name.endswith("16"):
            size = 2
        elif name.endswith("8"):
            size = 1
        else:
            return 0
        
        if "vec2" in name:
            size *= 2
        if "vec3" in name:
            size *= 3
        if "vec4" in name:
            size *= 4
        
        if "mat2" in name:
            size *= 4
        if "mat3" in name:
            size *= 9
        if "mat4" in name:
            size *= 16
        
        return size
    
    def strDataFormat(self, dataformat):
        if dataformat is None:
            return ""
        return dataformat.decode('utf-8')

    def strImageType(self, imageType):
        dformat = ImageType.ImageType()
        for property, value in inspect.getmembers(dformat):
            if not isinstance(value, int):
                continue
            if value == imageType:
                return property
        return ""

    def getFlag(self, type, flag):
        list = []
        if flag != 0:
            for property, value in inspect.getmembers(type):
                if not isinstance(value, int):
                    continue
                if flag & value != 0:
                    list.append(property)
        return list

    def getString(self, value):
        if value is None:
            return ""
        return value.decode('utf-8')

    def getVec2(self, value) -> dict:
        if value == None:
            return {}
        return {
            "type": "vec2",
            "x": value.X(), 
            "y": value.Y()
        }
    
    def getVec3(self, value) -> dict:
        if value == None:
            return {}
        return {
            "type": "vec3",
            "x": value.X(), 
            "y": value.Y(), 
            "z": value.Z()
        }
    
    def getVec4(self, value) -> dict:
        if value == None:
            return {}
        return {
            "type": "vec4",
            "x": value.X(), 
            "y": value.Y(), 
            "z": value.Z(), 
            "w": value.W()
        }

    def getMeshAttribute(self, attribute) -> dict:
        formatSize = self.getDataFormatByteSize(attribute.Format())
        bufferViewSize = attribute.View().Size()
        itemCount = bufferViewSize
        if formatSize != 0:
            itemCount = bufferViewSize / formatSize

        ret = {
            "type": "attribute",
            "semantic": self.getString(attribute.Semantic()),
            "format": self.strDataFormat(attribute.Format()),
            "count": str(int(itemCount)),
            "bytesize": self.getDataFormatByteSize(attribute.Format())
        }

        if attribute.View():
            ret["buffer"] = self.getBufferView(attribute.View())
        return ret
    
    def getMeshLayer(self, layer) -> dict:
        ret = {
            "type": "layer",
        }

        if not layer.AttributeIsNone():
            attrs = []
            for attrIndex in range(0, layer.AttributeLength()):
                attrs.append(self.getMeshAttribute(layer.Attribute(attrIndex)))
            ret["attributes"] = attrs
        
        ret["indices"] = {
            "format": self.strDataFormat(layer.IndicesType()),
            "count": str(layer.IndicesCount()),
            "buffer": self.getBufferView(layer.Indices()),
            "flag": self.getFlag(MeshFlag.MeshFlag(), layer.Flag())
        }
        
        if layer.MaterialPath():
            ret["material"] = self.getString(layer.MaterialPath())
        return ret

    def getBufferView(self, value) -> dict:
        ret = {
            "type": "view",
            "path": self.getString(value.Path()),
            "offset": str(value.Offset()),
            "stride": str(value.Stride()),
            "bytesize": str(value.Size())
        }
        return ret
    
    def getSampler(self, value) -> dict:
        ret = {
            "type": "sampler"
        }
        if value.FilterMin():
            ret["min"] = self.getString(value.FilterMin())
        if value.FilterMag():
            ret["mag"] = self.getString(value.FilterMag())
        if value.ModeU():
            ret["u"] = self.getString(value.ModeU())
        if value.ModeV():
            ret["v"] = self.getString(value.ModeV())
        if value.ModeW():
            ret["w"] = self.getString(value.ModeW())
        return ret

    def getMaterialValue(self, value) -> dict:
        ret = {
            "type": "value",
            "semantic": self.getString(value.Semantic())
        }
        
        if value.Sampler():
            ret["sampler"] = self.getSampler(value.Sampler())
        if value.Buffer():
            ret["buffer"] = self.getBufferView(value.Buffer())
        if value.Format():
            ret["format"] = self.strDataFormat(value.Format())
        ret["size"] = str(value.Size())
        
        if not value.DataIsNone():
            ret["data"] = str(value.DataAsNumpy())
        
        if value.ImagePath():
            ret["image"] = self.getString(value.ImagePath())
        return ret
    
    def getImageView(self, value) -> dict:
        ret = {
            "type": "image-config",
            "path": self.getString(value.Path())
        }
        if value.Type():
            ret["image_type"] = self.strImageType(value.Type())
        if value.Format():
            ret["format"] =  self.strDataFormat(value.Format())
        if value.Flag():
            ret["flag"] =  self.getFlag(PixelFlag.PixelFlag(), value.Flag())
        ret["width"] =  str(value.Width())
        ret["height"] =  str(value.Height())
        ret["depth"] =  str(value.Depth())
        ret["array"] =  str(value.Array())
        ret["mipmap"] =  str(value.Mipmap())
        return ret
        
    def getImage(self, path) -> dict:
        fullPath = os.path.join(self.folder, path)
        buf = open(fullPath, 'rb').read()
        buf = bytearray(buf)
        imageConfiguration = ImageConfiguration.ImageConfiguration.GetRootAsImageConfiguration(buf, 0)

        ret = {
            "type": "image",
            "name": self.getString(imageConfiguration.Name())
        }
        
        views = []
        for index in range(0, imageConfiguration.ViewLength()):
            views.append(self.getImageView(imageConfiguration.View(index)))
        ret["views"] = views
        return ret

    def getMaterial(self, path) -> dict:
        fullPath = os.path.join(self.folder, path)
        buf = open(fullPath, 'rb').read()
        buf = bytearray(buf)
        material = Material.Material.GetRootAsMaterial(buf, 0)

        ret = {
            "type": "material",
            "name": self.getString(material.Name())
        }
        
        if not material.HintIsNone():
            hints = []
            for index in range(0, material.HintLength()):
                hints.append(self.getString(material.Hint(index)))
            ret["hint"] = hints
        
        if not material.ValueIsNone():
            values = []
            for index in range(0, material.ValueLength()):
                values.append(self.getMaterialValue(material.Value(index)))
            ret["value"] = values
        return ret
    
    def getMesh(self, path) -> dict:
        fullPath = os.path.join(self.folder, path)
        buf = open(fullPath, 'rb').read()
        buf = bytearray(buf)
        mesh = Mesh.Mesh.GetRootAsMesh(buf, 0)
        
        ret = {
            "type": "mesh",
            "name": self.getString(mesh.Name()),
            "scale": str(mesh.Scale()),
            "radius": str(mesh.Radius()),
            "pivot": self.getVec3(mesh.Pivot())
        }

        if not mesh.LayerIsNone():
            layers = []
            for layerIndex in range(0, mesh.LayerLength()):
                layers.append(self.getMeshLayer(mesh.Layer(layerIndex)))
            ret["layer"] = layers
        return ret
    
    def getFile(self, path) -> dict:
        if path.lower().endswith(".mesh"):
            return self.getMesh(path)
        if path.lower().endswith(".material"):
            return self.getMaterial(path)
        if path.lower().endswith(".image"):
            return self.getImage(path)
        return None
    
    def printFile(self, path):
        data_set = self.getFile(path)
        json_dump = json.dumps(data_set)
        print(json_dump)

if __name__ == "__main__":
    import argparse as arggs
    
    arguments_parser = arggs.ArgumentParser(description='Debug mop model')
    arguments_parser.add_argument('-i','--in', help='input file', required=True)
    args = vars(arguments_parser.parse_args())

    filepath = args['in']
    p = Path(filepath)

    util = MopUtil(str(p.parent))
    util.printFile(str(p.name))
