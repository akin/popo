
import os, sys, shutil
import PIL
import subprocess

class CompressionUtil:
    def compress(self, directory, target):
        # try 7zip
        executable = "7z"
        if shutil.which(executable) is not None:
            realTarget = os.path.realpath(target)
            command = [executable, "a", "-tzip", realTarget, "*"]

            result = subprocess.run(command, cwd=directory)

            if result.returncode != 0:
                return False
            return True
            
        # try zip
        executable = "zip"
        return False