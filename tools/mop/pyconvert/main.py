
import os
from kivy.app import App
from kivy.uix.label import Label

import gltfutil
import moputil

gltf = gltfutil.GltfUtil()
gltf.load("DamagedHelmet.glb")
if not gltf.saveMop("test.mop"):
    print("Failed to save file")

mop = moputil.MopUtil("tmp/generate")
mop.printFile("gen_0.mesh")