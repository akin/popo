
import os, sys, shutil
import struct
import flatbuffers
from pygltflib import GLTF2
from pygltflib import BufferFormat
from pygltflib.utils import ImageFormat
from pathlib import Path
import PIL

from OpenGL.GL import *

import imageutil
import compressionutil

import flatbuffers
from Mop import *

class GltfUtil:
    def __init__(self):
        self.createPaths()
        self.fileInfo = {}
    
    def getTempPath(self):
        return "tmp"
    
    def getTargetPath(self):
        tmp = self.getTempPath()
        return tmp + "/generate"

    def createPaths(self):
        tmp = self.getTempPath()
        if not os.path.exists(tmp):
            os.makedirs(tmp)

        target = self.getTargetPath()
        if os.path.exists(target):
            shutil.rmtree(target)
        os.makedirs(target)

    def load(self, path):
        self.model = GLTF2().load(path)
    
    def getFilename(self, identifier, type):
        return "gen_" + str(identifier) + "." + type
    
    def convertImageToUri(self, index, image):
        file_name = self.model.export_image_to_file(index, override=True)
        tmp = self.getTempPath()

        # if new file was created, move it to tmp.
        if file_name is not None:
            new_path = tmp + "/" + file_name
            shutil.move(file_name, new_path)
            image.uri = new_path

    def createBuilder(self):
        return flatbuffers.Builder(1024)
    
    def createImages(self):
        for index, image in enumerate(self.model.images):
            # convert embedded images to files.
            self.convertImageToUri(index, image)
            
            # target folder
            path = self.getTargetPath()
            
            # image data handle file
            outputFileName = self.getFilename(index, "image")
            filename = path + "/" + outputFileName

            # Copy image to packaging area
            imagename = Path(image.uri).name
            targetFilename = path + "/" + imagename
            shutil.copy(image.uri, targetFilename)
            targetImage = imageutil.ImageUtil(targetFilename)

            if self.getFileSetting(outputFileName, "srgb"):
                targetImage.srgb = True

            builder = self.createBuilder()

            name_mop = builder.CreateString(imagename)
            path_mop = builder.CreateString(targetFilename)
            format_mop = builder.CreateString(targetImage.getDataFormat())

            images = []
            ImageView.ImageViewStart(builder)
            ImageView.ImageViewAddPath(builder, path_mop)
            ImageView.ImageViewAddType(builder, targetImage.getType())
            ImageView.ImageViewAddFormat(builder, format_mop)
            ImageView.ImageViewAddFlag(builder, targetImage.getFlag())
            ImageView.ImageViewAddWidth(builder, targetImage.getWidth())
            ImageView.ImageViewAddHeight(builder, targetImage.getHeight())
            ImageView.ImageViewAddDepth(builder, targetImage.getDepth())
            ImageView.ImageViewAddArray(builder, targetImage.getArray())
            ImageView.ImageViewAddMipmap(builder, targetImage.getMipMap())
            images.append(ImageView.ImageViewEnd(builder))

            ImageConfiguration.ImageConfigurationStartViewVector(builder, len(images))
            for item in images:
                builder.PrependUOffsetTRelative(item)
            images_mop = builder.EndVector(len(images))

            ImageConfiguration.ImageConfigurationStart(builder)
            ImageConfiguration.ImageConfigurationAddName(builder, name_mop)
            ImageConfiguration.ImageConfigurationAddView(builder, images_mop)
            image_configuration_mop = ImageConfiguration.ImageConfigurationEnd(builder)
            builder.Finish(image_configuration_mop)

            # create "mop image" object for each image
            if not self.writeToFile(filename, builder.Output()):
                print("Failed to write file")

    def addMaterialValueImage(self, builder, semantic, path, sampler):
        semantic_mop = builder.CreateString(semantic)
        path_mop = builder.CreateString(path)

        MaterialValue.MaterialValueStart(builder)
        MaterialValue.MaterialValueAddSemantic(builder, semantic_mop)
        MaterialValue.MaterialValueAddImagePath(builder, path_mop)
        MaterialValue.MaterialValueAddSampler(builder, sampler)
        return MaterialValue.MaterialValueEnd(builder)

    def getFilter(self, value):
        if value == GL_NEAREST:
            return "nearest"
        if value == GL_LINEAR:
            return "linear"
        return None

    def getWrap(self, value):
        if value == GL_REPEAT:
            return "repeat"
        if value == GL_MIRRORED_REPEAT:
            return "mirror_repeat"
        if value == GL_CLAMP_TO_EDGE:
            return "clamp_to_edge"
        if value == GL_CLAMP_TO_BORDER:
            return "clamp_to_border"
        if value == GL_MIRROR_CLAMP_TO_EDGE:
            return "mirror_clamp_to_edge"
        return None

    def addSampler(self, builder, samplerIndex):
        sampler = self.model.samplers[samplerIndex]

        filter_min_mop = None
        filter_mag_mop = None
        mode_u_mop = None
        mode_v_mop = None
        mode_w_mop = None

        if sampler.minFilter:
            filter_min_mop = builder.CreateString(self.getFilter(sampler.minFilter))
        
        if sampler.magFilter:
            filter_mag_mop = builder.CreateString(self.getFilter(sampler.magFilter))
        
        if sampler.wrapS:
            mode_u_mop = builder.CreateString(self.getWrap(sampler.wrapS))
        
        if sampler.wrapT:
            mode_v_mop = builder.CreateString(self.getWrap(sampler.wrapT))
        
        Sampler.SamplerStart(builder)

        if filter_min_mop:
            Sampler.SamplerAddFilterMin(builder, filter_min_mop)
        if filter_mag_mop:
            Sampler.SamplerAddFilterMag(builder, filter_mag_mop)
        if mode_u_mop:
            Sampler.SamplerAddModeU(builder, mode_u_mop)
        if mode_v_mop:
            Sampler.SamplerAddModeV(builder, mode_v_mop)
        if mode_w_mop:
            Sampler.SamplerAddModeW(builder, mode_w_mop)
        
        return Sampler.SamplerEnd(builder)
    
    def addMaterialValueFormatted(self, builder, semantic, format, size, data):
        semantic_mop = builder.CreateString(semantic)
        format_mop = builder.CreateString(format)
        
        MaterialValue.MaterialValueStartDataVector(builder, len(data))
        for item in reversed(data):
            builder.PrependByte(item)
        data_mop = builder.EndVector(len(data))

        MaterialValue.MaterialValueStart(builder)
        MaterialValue.MaterialValueAddSemantic(builder, semantic_mop)
        MaterialValue.MaterialValueAddFormat(builder, format_mop)
        MaterialValue.MaterialValueAddSize(builder, size)
        MaterialValue.MaterialValueAddData(builder, data_mop)
        return MaterialValue.MaterialValueEnd(builder)
    
    def packByteArrayFloat(self, value):
        return bytearray(struct.pack("<f", value))
    
    def packByteArrayVec2(self, value):
        return bytearray(struct.pack("<ff", value[0], value[1]))

    def packByteArrayVec3(self, value):
        return bytearray(struct.pack("<fff", value[0], value[1], value[2]))

    def packByteArrayVec4(self, value):
        return bytearray(struct.pack("<ffff", value[0], value[1], value[2], value[3]))

    def setFileSetting(self, path, key, value):
        if path not in self.fileInfo:
            self.fileInfo[path] = {}
        self.fileInfo[path][key] = value
    
    def getFileSetting(self, path, key):
        if path in self.fileInfo:
            dic = self.fileInfo[path]
            if key in dic:
                return dic[key]
        return None

    def createMaterials(self):
        for index, material in enumerate(self.model.materials):
            builder = self.createBuilder()

            hints = []
            values = []

            if material.pbrMetallicRoughness:
                if material.pbrMetallicRoughness.baseColorTexture:
                    texture = self.model.textures[material.pbrMetallicRoughness.baseColorTexture.index]
                    imageFileName = self.getFilename(texture.source, "image")
                    sampler_mop = self.addSampler(builder, texture.sampler)
                    values.append(self.addMaterialValueImage(builder, "BaseColor", imageFileName, sampler_mop))
                    self.setFileSetting(imageFileName, "srgb", True)
                if material.pbrMetallicRoughness.baseColorFactor:
                    values.append(self.addMaterialValueFormatted(builder, "BaseColorFactor", "vec4_float32", 1, self.packByteArrayVec4(material.pbrMetallicRoughness.baseColorFactor)))

                if material.pbrMetallicRoughness.metallicRoughnessTexture:
                    texture = self.model.textures[material.pbrMetallicRoughness.metallicRoughnessTexture.index]
                    imageFileName = self.getFilename(texture.source, "image")
                    sampler_mop = self.addSampler(builder, texture.sampler)
                    values.append(self.addMaterialValueImage(builder, "MetallicRoughness", imageFileName, sampler_mop))
                    
                if material.pbrMetallicRoughness.metallicFactor:
                    values.append(self.addMaterialValueFormatted(builder, "MetallicFactor", "float32", 1, self.packByteArrayFloat(material.pbrMetallicRoughness.metallicFactor)))
                if material.pbrMetallicRoughness.roughnessFactor:
                    values.append(self.addMaterialValueFormatted(builder, "RoughnessFactor", "float32", 1, self.packByteArrayFloat(material.pbrMetallicRoughness.roughnessFactor)))
            
            if material.alphaCutoff:
                values.append(self.addMaterialValueFormatted(builder, "AlphaCutOff", "float32", 1, self.packByteArrayFloat(material.alphaCutoff)))

            if material.emissiveFactor:
                values.append(self.addMaterialValueFormatted(builder, "EmissiveFactor", "vec3_float32", 1, self.packByteArrayVec3(material.emissiveFactor)))
            
            if material.emissiveTexture:
                texture = self.model.textures[material.emissiveTexture.index]
                imageFileName = self.getFilename(texture.source, "image")
                sampler_mop = self.addSampler(builder, texture.sampler)
                values.append(self.addMaterialValueImage(builder, "Emissive", imageFileName, sampler_mop))

            if material.normalTexture:
                texture = self.model.textures[material.normalTexture.index]
                imageFileName = self.getFilename(texture.source, "image")
                sampler_mop = self.addSampler(builder, texture.sampler)
                values.append(self.addMaterialValueImage(builder, "Normal", imageFileName, sampler_mop))

            if material.occlusionTexture:
                texture = self.model.textures[material.occlusionTexture.index]
                imageFileName = self.getFilename(texture.source, "image")
                sampler_mop = self.addSampler(builder, texture.sampler)
                values.append(self.addMaterialValueImage(builder, "Occlusion", imageFileName, sampler_mop))
            
            if material.alphaMode:
                if material.alphaMode == "OPAQUE":
                    hints.append("opaque")
                if material.alphaMode == "BLEND":
                    hints.append("alpha-blended")
                if material.alphaMode == "MASK":
                    hints.append("alpha-cutoff")
            else:
                hints.append("opaque")

            if material.doubleSided:
                hints.append("double-sided")
            
            # Create hints vector
            hints_arr = []
            for item in hints:
                hints_arr.append(builder.CreateString(item))

            Material.MaterialStartHintVector(builder, len(hints_arr))
            for item in hints_arr:
                builder.PrependUOffsetTRelative(item)
            hints_mop = builder.EndVector(len(hints_arr))

            # Create values vector
            Material.MaterialStartValueVector(builder, len(values))
            for item in values:
                builder.PrependUOffsetTRelative(item)
            values_mop = builder.EndVector(len(values))

            # prepare for material creation
            name_mop = None
            if material.name:
                name_mop = builder.CreateString(material.name)
            
            # create Material
            Material.MaterialStart(builder)
            if name_mop:
                Material.MaterialAddName(builder, name_mop)
            if hints_mop:
                Material.MaterialAddHint(builder, hints_mop)
                
            Material.MaterialAddValue(builder, values_mop)
            mop_material = Material.MaterialEnd(builder)
            builder.Finish(mop_material)
            
            # target folder
            path = self.getTargetPath()
            filename = path + "/" + self.getFilename(index, "material")
            if not self.writeToFile(filename, builder.Output()):
                print("Failed to write file")

    def getPrimitiveType(self, mode):
        primtype = PrimitiveType.PrimitiveType()
        if mode == GL_POINTS:
            return primtype.Point
        if mode == GL_LINES:
            return primtype.Line
        if mode == GL_TRIANGLES:
            return primtype.Triangle
        if mode == GL_TRIANGLE_FAN:
            return primtype.TriangleFan
        if mode == GL_TRIANGLE_STRIP:
            return primtype.TriangleStrip
        if mode == GL_POLYGON:
            return primtype.Polygon
        return primtype.None_

    def getDataType(self, componentType):
        if componentType == GL_UNSIGNED_BYTE:
            return "uint8"
        if componentType == GL_UNSIGNED_SHORT:
            return "uint16"
        if componentType == GL_UNSIGNED_INT:
            return "uint32"
        if componentType == GL_HALF_FLOAT:
            return "float16"
        if componentType == GL_FLOAT:
            return "float32"
        if componentType == GL_DOUBLE:
            return "float64"
        return ""
        
    def getDataTypeByteSize(self, componentType):
        if componentType == GL_UNSIGNED_BYTE:
            return 1
        if componentType == GL_UNSIGNED_SHORT:
            return 2
        if componentType == GL_UNSIGNED_INT:
            return 4
        if componentType == GL_HALF_FLOAT:
            return 2
        if componentType == GL_FLOAT:
            return 4
        if componentType == GL_DOUBLE:
            return 8
        return 0
    
    def getComponentCount(self, ctype):
        if ctype == "SCALAR":
            return 1
        if ctype == "VEC2":
            return 2
        if ctype == "VEC3":
            return 3
        if ctype == "VEC4":
            return 4
        if ctype == "MAT2":
            return 4
        if ctype == "MAT3":
            return 9
        if ctype == "MAT4":
            return 16
        return 0
        
    def convertBuffersToUri(self):
        # based on pygltflib function that converts buffers to whatever
        # this function converts "whatever" format buffers to bin files. and adds it to "uri".
        # if new file is created, use tmp.
        tmp = self.getTempPath()
        
        for i, buffer in enumerate(self.model.buffers):
            current_buffer_format = self.model.identify_uri(buffer.uri)
            if current_buffer_format == BufferFormat.BINFILE:  # already binfile!
                continue

            if current_buffer_format == BufferFormat.DATAURI:
                data = self.model.decode_data_uri(buffer.uri)
            elif current_buffer_format == BufferFormat.BINARYBLOB:
                data = self.model.binary_blob()
            else:
                continue

            self.model.destroy_binary_blob()  # free up any binary blob floating around

            filename = Path(f"{i}").with_suffix(".bin")
            binfile_path = tmp / filename

            with open(binfile_path, "wb") as f:  # save bin file with the gltf file
                f.write(data)
            buffer.uri = str(binfile_path)

    def createBuffers(self):
        # we are going to refer to the buffer, by accessor ID
        # we are going to treat accessors as "unique data", so whatever happens with "buffer view", is of no interest for us at this stage.
        # - we might have duplicate data
        # + simplify everything.
        # ? we are definetly taking away the "stride"
        # but maybe we can have a "packing" round after with the mop, "pack mop" where stride can be brought back and data evaluated.
        for index, accessor in enumerate(self.model.accessors):
            # target folder
            path = self.getTargetPath()
            
            # image data handle file
            filename = path + "/" + self.getFilename(index, "buffer")
            
            # Take buffer & tightly pack it, no offset, stride, shenanigans here.
            elementByteSize = self.getComponentCount(accessor.type) * self.getDataTypeByteSize(accessor.componentType)
            totalByteSize = elementByteSize * accessor.count
            bufferView = self.model.bufferViews[accessor.bufferView]
            buffer = self.model.buffers[bufferView.buffer]

            if buffer.uri == "":
                continue

            dataSource = open(buffer.uri, "rb")
            if dataSource.closed:
                continue
            
            dataTarget = open(filename, "wb")
            if dataTarget.closed:
                continue

            position = accessor.byteOffset + bufferView.byteOffset
            dataSource.seek(position)

            # No stride buffer, just copy.
            if bufferView.byteStride == None or bufferView.byteStride == 0:
                remaining = totalByteSize
                readAmount = 16*1024
                while remaining > 0:
                    if remaining < readAmount:
                        readAmount = remaining
                    blob = dataSource.read(readAmount)
                    dataTarget.write(blob)
                    remaining -= readAmount
            else:
                for index in range(accessor.count):
                    dataSource.seek(position + index * bufferView.byteStride)
                    blob = dataSource.read(elementByteSize)
                    dataTarget.write(blob)
    
    def createBufferView(self, builder, path, offset, stride, size):
        path_mop = builder.CreateString(path)
        BufferView.BufferViewStart(builder)
        BufferView.BufferViewAddPath(builder, path_mop)
        BufferView.BufferViewAddOffset(builder, offset)
        BufferView.BufferViewAddStride(builder, stride)
        BufferView.BufferViewAddSize(builder, size)
        return BufferView.BufferViewEnd(builder)
    
    def getBufferViewForAccessorIndex(self, builder, accessorIndex):
        accessor = self.model.accessors[accessorIndex]
        elementByteSize = self.getComponentCount(accessor.type) * self.getDataTypeByteSize(accessor.componentType)
        totalByteSize = elementByteSize * accessor.count
        accessorFilePath = self.getFilename(accessorIndex, "buffer")
        return self.createBufferView(builder, path=accessorFilePath, offset=0, stride=elementByteSize, size=totalByteSize)

    def createMeshAttribute(self, builder, attribute_semantic, attribute_semantic_index, attribute_format, accessorIndex):
        buffer_view_mop = self.getBufferViewForAccessorIndex(builder, accessorIndex)
        semantic_mop = builder.CreateString(attribute_semantic)
        format_mop = builder.CreateString(attribute_format)
        MeshAttribute.MeshAttributeStart(builder)
        MeshAttribute.MeshAttributeAddSemantic(builder, semantic_mop)
        MeshAttribute.MeshAttributeAddIndex(builder, attribute_semantic_index)
        MeshAttribute.MeshAttributeAddFormat(builder, format_mop)
        MeshAttribute.MeshAttributeAddView(builder, buffer_view_mop)
        return MeshAttribute.MeshAttributeEnd(builder)

    def creatMeshes(self):
        for index, mesh in enumerate(self.model.meshes):
            builder = self.createBuilder()

            layers = []
            for primitive in mesh.primitives:
                attributes = []

                if primitive.attributes.POSITION != None:
                    attributes.append(self.createMeshAttribute(builder, "position", 0, "vec3_float32", primitive.attributes.POSITION))
                if primitive.attributes.NORMAL != None:
                    attributes.append(self.createMeshAttribute(builder, "normal", 0, "vec3_float32", primitive.attributes.NORMAL))
                if primitive.attributes.TANGENT != None:
                    attributes.append(self.createMeshAttribute(builder, "tangent", 0, "vec3_float32", primitive.attributes.TANGENT))
                if primitive.attributes.TEXCOORD_0 != None:
                    attributes.append(self.createMeshAttribute(builder, "texture_coordinate", 0, "vec2_float32", primitive.attributes.TEXCOORD_0))
                if primitive.attributes.TEXCOORD_1 != None:
                    attributes.append(self.createMeshAttribute(builder, "texture_coordinate", 1, "vec2_float32", primitive.attributes.TEXCOORD_1))
                if primitive.attributes.COLOR_0 != None:
                    attributes.append(self.createMeshAttribute(builder, "color", 0, "vec4_uint8", primitive.attributes.COLOR_0))
                if primitive.attributes.JOINTS_0 != None:
                    attributes.append(self.createMeshAttribute(builder, "bone_joint", 0, "uint16", primitive.attributes.JOINTS_0))
                if primitive.attributes.WEIGHTS_0 != None:
                    attributes.append(self.createMeshAttribute(builder, "bone_weight", 0, "float32", primitive.attributes.WEIGHTS_0))

                # Create attributes vector
                MeshLayer.MeshLayerStartAttributeVector(builder, len(attributes))
                for item in attributes:
                    builder.PrependUOffsetTRelative(item)
                attribute_mop = builder.EndVector(len(attributes))

                indices = self.model.accessors[primitive.indices]
                indices_mop = self.getBufferViewForAccessorIndex(builder, primitive.indices)
                material_path_mop = builder.CreateString(self.getFilename(primitive.material, "material"))
                indiceType_mop = builder.CreateString(self.getDataType(indices.componentType))

                MeshLayer.MeshLayerStart(builder)
                MeshLayer.MeshLayerAddAttribute(builder, attribute_mop)
                MeshLayer.MeshLayerAddPrimitiveType(builder, self.getPrimitiveType(primitive.mode))
                MeshLayer.MeshLayerAddIndicesType(builder, indiceType_mop)
                MeshLayer.MeshLayerAddIndicesCount(builder, indices.count)
                MeshLayer.MeshLayerAddIndices(builder, indices_mop)
                MeshLayer.MeshLayerAddMaterialPath(builder, material_path_mop)

                layers.append(MeshLayer.MeshLayerEnd(builder))
            
            name_mop = None
            if mesh.name != None:
                name_mop = builder.CreateString(mesh.name)

            # Create layers vector
            Mesh.MeshStartLayerVector(builder, len(layers))
            for item in layers:
                builder.PrependUOffsetTRelative(item)
            layer_mop = builder.EndVector(len(layers))

            Mesh.MeshStart(builder)
            Mesh.MeshAddName(builder, name_mop)
            Mesh.MeshAddLayer(builder, layer_mop)
            Mesh.MeshAddPivot(builder, Vec3.CreateVec3(builder, 0,0,0))
            Mesh.MeshAddRadius(builder, 0.0)
            Mesh.MeshAddScale(builder, 1.0)
            mesh_mop = Mesh.MeshEnd(builder)
            builder.Finish(mesh_mop)
            
            # target folder
            path = self.getTargetPath()
            filename = path + "/" + self.getFilename(index, "mesh")
            if not self.writeToFile(filename, builder.Output()):
                print("Failed to write file")
            
    def saveMop(self, path):
        # Materials define in gltf2 if what some image is, so materials needs to be parsed before images.
        self.createMaterials()
        self.createImages()
        self.convertBuffersToUri()
        self.createBuffers()
        self.creatMeshes()
        compression = compressionutil.CompressionUtil()
        return compression.compress(self.getTargetPath(), path)

    def writeToFile(self, filename, bytearray) -> bool:
        file = open(filename, "wb")
        if file.closed:
            return False
        file.seek(0)
        count = file.write(bytearray)
        if count != len(bytearray):
            return False
        file.close()
        return True

if __name__ == "__main__":
    import argparse as arggs
    
    arguments_parser = arggs.ArgumentParser(description='gltf to mop converter')
    arguments_parser.add_argument('-i','--in', help='input file', required=True)
    arguments_parser.add_argument('-o','--out', help='output file', required=True)
    args = vars(arguments_parser.parse_args())

    inPath = args['in']
    outPath = args['out']
    
    gltf = gltfutil.GltfUtil()
    gltf.load(inPath)
    if not gltf.saveMop(outPath):
        print("Failed to save file")
