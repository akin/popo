
import os, sys, shutil
import PIL

import flatbuffers
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import generated.Mop as Mop
from generated.Mop.ImageType import *
from generated.Mop.PixelFlag import *

class ImageUtil:
    def __init__(self, path):
        self.image = PIL.Image.open(path)
        self.srgb = False
    
    def getWidth(self):
        return self.image.width

    def getHeight(self):
        return self.image.height

    def getDepth(self):
        return 1

    def getArray(self):
        return 1

    def getMipMap(self):
        return 1

    def getFlag(self):
        flag = 0
        if self.srgb:
            flag |= Mop.PixelFlag.PixelFlag().SRGB
        
        if self.image.mode == 'RGB':
            flag |= Mop.PixelFlag.PixelFlag().Red
            flag |= Mop.PixelFlag.PixelFlag().Green
            flag |= Mop.PixelFlag.PixelFlag().Blue

        if self.image.mode == 'RGBA':
            flag |= Mop.PixelFlag.PixelFlag().Red
            flag |= Mop.PixelFlag.PixelFlag().Green
            flag |= Mop.PixelFlag.PixelFlag().Blue
            flag |= Mop.PixelFlag.PixelFlag().Alpha
        
        return flag

    def getType(self):
        return Mop.ImageType.ImageType().Texture2D
        
    def getDataFormat(self):
        if self.image.mode == 'L' or self.image.mode == 'P':
            return "uint8"
        if self.image.mode == 'RGB':
            return "vec3_uint8"
        if self.image.mode == 'RGBA':
            return "vec4_uint8"
        if self.image.mode == 'I':
            return "uint32"
        if self.image.mode == 'F':
            return "float32"
        return ""
