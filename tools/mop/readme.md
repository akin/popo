
# 3D fileformat based on flatbuffers.
 * editor gui using python https://github.com/kivy/kivy
 * flatbuffers

![](mop_fileformat.png)


## Concepts
binary blobs. The "Views" in mop describe how blobs should be interpreted.

---

Structures are specified in different files. This creates a problem of "multiple files", which should be mitigated by having all the required files in one zip/archive.

each "path:string" can refer to relative file inside the archive, or it could refer to a cloud storage url.

[Donkey.group]
 * Group
 * Node(s)
 * mesh path

[Donkey.mesh]
 * Mesh
 * MeshLayers
 * MeshAttributes
 * MeshPrimitives
 * BufferViews
 * buffer(s) path
 * material(s) path

[Donkey.material]
 * Material
 * MaterialBufferValue(s)
 * MaterialImageValue(s)
 * BufferViews
 * Paths to ImageViews

[Donkey.buffer]
 * binary blob

[Donkey.image]
 * Contains ImageViews
 * image file describes "one image", the application is free to choose which ImageView corresponds to the optimal image.

Scene graphss.
 * Nodes
 * Cameras
 * mesh + materials
 * properties

Todo:
 * Materials.
 * Prefabs.
 * Textures.
 * Animations.
 * Skeletons.
 * Text.

Not started:
 * Scripts.
 * Shaders.
 * Effects.

## License
@copyright THE BEER-WARE LICENSE (c) 2022
----------------------------------------------------------------------------
"THE BEER-WARE LICENSE" (Revision 42):
<mikael_korpela@hotmail.com> wrote this file.  As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. 

https://en.wikipedia.org/wiki/Beerware
----------------------------------------------------------------------------