
clone https://github.com/google/flatbuffers.git
cmake build flatc executable.

flatc --cpp --python ../mop.fbs ../animation.fbs ../node.fbs ../skeleton.fbs ../text.fbs ../mesh.fbs ../material.fbs ../image.fbs ../buffer.fbs ../generic.fbs