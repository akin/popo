// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_MESH_MOP_H_
#define FLATBUFFERS_GENERATED_MESH_MOP_H_

#include "flatbuffers/flatbuffers.h"

#include "buffer_generated.h"
#include "generic_generated.h"
#include "image_generated.h"

namespace Mop {

struct MeshAttribute;
struct MeshAttributeBuilder;

struct MeshLayer;
struct MeshLayerBuilder;

struct Mesh;
struct MeshBuilder;

enum MeshFlag : uint8_t {
  MeshFlag_DoubleSided = 1,
  MeshFlag_WindingCCW = 2,
  MeshFlag_NONE = 0,
  MeshFlag_ANY = 3
};

inline const MeshFlag (&EnumValuesMeshFlag())[2] {
  static const MeshFlag values[] = {
    MeshFlag_DoubleSided,
    MeshFlag_WindingCCW
  };
  return values;
}

inline const char * const *EnumNamesMeshFlag() {
  static const char * const names[3] = {
    "DoubleSided",
    "WindingCCW",
    nullptr
  };
  return names;
}

inline const char *EnumNameMeshFlag(MeshFlag e) {
  if (flatbuffers::IsOutRange(e, MeshFlag_DoubleSided, MeshFlag_WindingCCW)) return "";
  const size_t index = static_cast<size_t>(e) - static_cast<size_t>(MeshFlag_DoubleSided);
  return EnumNamesMeshFlag()[index];
}

struct MeshAttribute FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef MeshAttributeBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_SEMANTIC = 4,
    VT_INDEX = 6,
    VT_FORMAT = 8,
    VT_VIEW = 10
  };
  const flatbuffers::String *semantic() const {
    return GetPointer<const flatbuffers::String *>(VT_SEMANTIC);
  }
  uint8_t index() const {
    return GetField<uint8_t>(VT_INDEX, 0);
  }
  const flatbuffers::String *format() const {
    return GetPointer<const flatbuffers::String *>(VT_FORMAT);
  }
  const Mop::BufferView *view() const {
    return GetPointer<const Mop::BufferView *>(VT_VIEW);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_SEMANTIC) &&
           verifier.VerifyString(semantic()) &&
           VerifyField<uint8_t>(verifier, VT_INDEX) &&
           VerifyOffset(verifier, VT_FORMAT) &&
           verifier.VerifyString(format()) &&
           VerifyOffset(verifier, VT_VIEW) &&
           verifier.VerifyTable(view()) &&
           verifier.EndTable();
  }
};

struct MeshAttributeBuilder {
  typedef MeshAttribute Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_semantic(flatbuffers::Offset<flatbuffers::String> semantic) {
    fbb_.AddOffset(MeshAttribute::VT_SEMANTIC, semantic);
  }
  void add_index(uint8_t index) {
    fbb_.AddElement<uint8_t>(MeshAttribute::VT_INDEX, index, 0);
  }
  void add_format(flatbuffers::Offset<flatbuffers::String> format) {
    fbb_.AddOffset(MeshAttribute::VT_FORMAT, format);
  }
  void add_view(flatbuffers::Offset<Mop::BufferView> view) {
    fbb_.AddOffset(MeshAttribute::VT_VIEW, view);
  }
  explicit MeshAttributeBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<MeshAttribute> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<MeshAttribute>(end);
    return o;
  }
};

inline flatbuffers::Offset<MeshAttribute> CreateMeshAttribute(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> semantic = 0,
    uint8_t index = 0,
    flatbuffers::Offset<flatbuffers::String> format = 0,
    flatbuffers::Offset<Mop::BufferView> view = 0) {
  MeshAttributeBuilder builder_(_fbb);
  builder_.add_view(view);
  builder_.add_format(format);
  builder_.add_semantic(semantic);
  builder_.add_index(index);
  return builder_.Finish();
}

inline flatbuffers::Offset<MeshAttribute> CreateMeshAttributeDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *semantic = nullptr,
    uint8_t index = 0,
    const char *format = nullptr,
    flatbuffers::Offset<Mop::BufferView> view = 0) {
  auto semantic__ = semantic ? _fbb.CreateString(semantic) : 0;
  auto format__ = format ? _fbb.CreateString(format) : 0;
  return Mop::CreateMeshAttribute(
      _fbb,
      semantic__,
      index,
      format__,
      view);
}

struct MeshLayer FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef MeshLayerBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_ATTRIBUTE = 4,
    VT_PRIMITIVE_TYPE = 6,
    VT_FLAG = 8,
    VT_INDICES_TYPE = 10,
    VT_INDICES_COUNT = 12,
    VT_INDICES = 14,
    VT_MATERIAL_PATH = 16
  };
  const flatbuffers::Vector<flatbuffers::Offset<Mop::MeshAttribute>> *attribute() const {
    return GetPointer<const flatbuffers::Vector<flatbuffers::Offset<Mop::MeshAttribute>> *>(VT_ATTRIBUTE);
  }
  Mop::PrimitiveType primitive_type() const {
    return static_cast<Mop::PrimitiveType>(GetField<uint8_t>(VT_PRIMITIVE_TYPE, 0));
  }
  Mop::MeshFlag flag() const {
    return static_cast<Mop::MeshFlag>(GetField<uint8_t>(VT_FLAG, 0));
  }
  const flatbuffers::String *indices_type() const {
    return GetPointer<const flatbuffers::String *>(VT_INDICES_TYPE);
  }
  uint32_t indices_count() const {
    return GetField<uint32_t>(VT_INDICES_COUNT, 0);
  }
  const Mop::BufferView *indices() const {
    return GetPointer<const Mop::BufferView *>(VT_INDICES);
  }
  const flatbuffers::String *material_path() const {
    return GetPointer<const flatbuffers::String *>(VT_MATERIAL_PATH);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_ATTRIBUTE) &&
           verifier.VerifyVector(attribute()) &&
           verifier.VerifyVectorOfTables(attribute()) &&
           VerifyField<uint8_t>(verifier, VT_PRIMITIVE_TYPE) &&
           VerifyField<uint8_t>(verifier, VT_FLAG) &&
           VerifyOffset(verifier, VT_INDICES_TYPE) &&
           verifier.VerifyString(indices_type()) &&
           VerifyField<uint32_t>(verifier, VT_INDICES_COUNT) &&
           VerifyOffset(verifier, VT_INDICES) &&
           verifier.VerifyTable(indices()) &&
           VerifyOffset(verifier, VT_MATERIAL_PATH) &&
           verifier.VerifyString(material_path()) &&
           verifier.EndTable();
  }
};

struct MeshLayerBuilder {
  typedef MeshLayer Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_attribute(flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Mop::MeshAttribute>>> attribute) {
    fbb_.AddOffset(MeshLayer::VT_ATTRIBUTE, attribute);
  }
  void add_primitive_type(Mop::PrimitiveType primitive_type) {
    fbb_.AddElement<uint8_t>(MeshLayer::VT_PRIMITIVE_TYPE, static_cast<uint8_t>(primitive_type), 0);
  }
  void add_flag(Mop::MeshFlag flag) {
    fbb_.AddElement<uint8_t>(MeshLayer::VT_FLAG, static_cast<uint8_t>(flag), 0);
  }
  void add_indices_type(flatbuffers::Offset<flatbuffers::String> indices_type) {
    fbb_.AddOffset(MeshLayer::VT_INDICES_TYPE, indices_type);
  }
  void add_indices_count(uint32_t indices_count) {
    fbb_.AddElement<uint32_t>(MeshLayer::VT_INDICES_COUNT, indices_count, 0);
  }
  void add_indices(flatbuffers::Offset<Mop::BufferView> indices) {
    fbb_.AddOffset(MeshLayer::VT_INDICES, indices);
  }
  void add_material_path(flatbuffers::Offset<flatbuffers::String> material_path) {
    fbb_.AddOffset(MeshLayer::VT_MATERIAL_PATH, material_path);
  }
  explicit MeshLayerBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<MeshLayer> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<MeshLayer>(end);
    return o;
  }
};

inline flatbuffers::Offset<MeshLayer> CreateMeshLayer(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Mop::MeshAttribute>>> attribute = 0,
    Mop::PrimitiveType primitive_type = Mop::PrimitiveType_None,
    Mop::MeshFlag flag = static_cast<Mop::MeshFlag>(0),
    flatbuffers::Offset<flatbuffers::String> indices_type = 0,
    uint32_t indices_count = 0,
    flatbuffers::Offset<Mop::BufferView> indices = 0,
    flatbuffers::Offset<flatbuffers::String> material_path = 0) {
  MeshLayerBuilder builder_(_fbb);
  builder_.add_material_path(material_path);
  builder_.add_indices(indices);
  builder_.add_indices_count(indices_count);
  builder_.add_indices_type(indices_type);
  builder_.add_attribute(attribute);
  builder_.add_flag(flag);
  builder_.add_primitive_type(primitive_type);
  return builder_.Finish();
}

inline flatbuffers::Offset<MeshLayer> CreateMeshLayerDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const std::vector<flatbuffers::Offset<Mop::MeshAttribute>> *attribute = nullptr,
    Mop::PrimitiveType primitive_type = Mop::PrimitiveType_None,
    Mop::MeshFlag flag = static_cast<Mop::MeshFlag>(0),
    const char *indices_type = nullptr,
    uint32_t indices_count = 0,
    flatbuffers::Offset<Mop::BufferView> indices = 0,
    const char *material_path = nullptr) {
  auto attribute__ = attribute ? _fbb.CreateVector<flatbuffers::Offset<Mop::MeshAttribute>>(*attribute) : 0;
  auto indices_type__ = indices_type ? _fbb.CreateString(indices_type) : 0;
  auto material_path__ = material_path ? _fbb.CreateString(material_path) : 0;
  return Mop::CreateMeshLayer(
      _fbb,
      attribute__,
      primitive_type,
      flag,
      indices_type__,
      indices_count,
      indices,
      material_path__);
}

struct Mesh FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef MeshBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_NAME = 4,
    VT_LAYER = 6,
    VT_PIVOT = 8,
    VT_RADIUS = 10,
    VT_SCALE = 12
  };
  const flatbuffers::String *name() const {
    return GetPointer<const flatbuffers::String *>(VT_NAME);
  }
  const flatbuffers::Vector<flatbuffers::Offset<Mop::MeshLayer>> *layer() const {
    return GetPointer<const flatbuffers::Vector<flatbuffers::Offset<Mop::MeshLayer>> *>(VT_LAYER);
  }
  const Mop::Vec3 *pivot() const {
    return GetStruct<const Mop::Vec3 *>(VT_PIVOT);
  }
  float radius() const {
    return GetField<float>(VT_RADIUS, 0.0f);
  }
  float scale() const {
    return GetField<float>(VT_SCALE, 1.0f);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_NAME) &&
           verifier.VerifyString(name()) &&
           VerifyOffset(verifier, VT_LAYER) &&
           verifier.VerifyVector(layer()) &&
           verifier.VerifyVectorOfTables(layer()) &&
           VerifyField<Mop::Vec3>(verifier, VT_PIVOT) &&
           VerifyField<float>(verifier, VT_RADIUS) &&
           VerifyField<float>(verifier, VT_SCALE) &&
           verifier.EndTable();
  }
};

struct MeshBuilder {
  typedef Mesh Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_name(flatbuffers::Offset<flatbuffers::String> name) {
    fbb_.AddOffset(Mesh::VT_NAME, name);
  }
  void add_layer(flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Mop::MeshLayer>>> layer) {
    fbb_.AddOffset(Mesh::VT_LAYER, layer);
  }
  void add_pivot(const Mop::Vec3 *pivot) {
    fbb_.AddStruct(Mesh::VT_PIVOT, pivot);
  }
  void add_radius(float radius) {
    fbb_.AddElement<float>(Mesh::VT_RADIUS, radius, 0.0f);
  }
  void add_scale(float scale) {
    fbb_.AddElement<float>(Mesh::VT_SCALE, scale, 1.0f);
  }
  explicit MeshBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<Mesh> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<Mesh>(end);
    return o;
  }
};

inline flatbuffers::Offset<Mesh> CreateMesh(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> name = 0,
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Mop::MeshLayer>>> layer = 0,
    const Mop::Vec3 *pivot = 0,
    float radius = 0.0f,
    float scale = 1.0f) {
  MeshBuilder builder_(_fbb);
  builder_.add_scale(scale);
  builder_.add_radius(radius);
  builder_.add_pivot(pivot);
  builder_.add_layer(layer);
  builder_.add_name(name);
  return builder_.Finish();
}

inline flatbuffers::Offset<Mesh> CreateMeshDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *name = nullptr,
    const std::vector<flatbuffers::Offset<Mop::MeshLayer>> *layer = nullptr,
    const Mop::Vec3 *pivot = 0,
    float radius = 0.0f,
    float scale = 1.0f) {
  auto name__ = name ? _fbb.CreateString(name) : 0;
  auto layer__ = layer ? _fbb.CreateVector<flatbuffers::Offset<Mop::MeshLayer>>(*layer) : 0;
  return Mop::CreateMesh(
      _fbb,
      name__,
      layer__,
      pivot,
      radius,
      scale);
}

inline const Mop::Mesh *GetMesh(const void *buf) {
  return flatbuffers::GetRoot<Mop::Mesh>(buf);
}

inline const Mop::Mesh *GetSizePrefixedMesh(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<Mop::Mesh>(buf);
}

inline bool VerifyMeshBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<Mop::Mesh>(nullptr);
}

inline bool VerifySizePrefixedMeshBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<Mop::Mesh>(nullptr);
}

inline void FinishMeshBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Mop::Mesh> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedMeshBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Mop::Mesh> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Mop

#endif  // FLATBUFFERS_GENERATED_MESH_MOP_H_
